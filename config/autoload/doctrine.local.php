<?php
    return array(
        'doctrine' => array(
            'connection' => array(
                'orm_default' => array(
                    'driverClass' =>'Doctrine\DBAL\Driver\PDOMySql\Driver',
                    'params' => array(
                        'host'     => 'localhost',
                        'port'     => '3306',
                        'user'     => 'clubman',
                        'password' => 'S5UtJpCrM2Xd9JZB',
                        'dbname'   => 'clubman',
                    )
                )
            ),
            'driver' => array(
                'cache' => array(
                    'class' => 'Doctrine\Common\Cache\ApcCache'
                ),
                'configuration' => array(
                    'orm_default' => array(
                        'metadata_cache' => 'apc',
                        'query_cache'    => 'apc',
                        'result_cache'   => 'apc'
                    )
                ),
            )
        )
    );

