@Echo off
:START
set /p todo=What to do [DB/GITUPDATE/GITCOMMIT]?: 
echo %todo%
GoTo %todo%
pause
GoTo START
:DB
call vendor\bin\doctrine-module orm:schema-tool:update --force
GoTo START

:GITUPDATE
git pull
GoTo START

:GITCOMMIT
set /p svnmessage=Give CommitMessage?: 
git add .
git commit -m "%svnmessage%"
GoTo START
