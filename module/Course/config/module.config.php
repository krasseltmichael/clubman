<?php
use Zend\Router\Http\Literal;
use Zend\Router\Http\Segment;
use Zend\Router\Http\Regex;
use Zend\ServiceManager\Factory\InvokableFactory;
use Application\Route\StaticRoute;
use Doctrine\ORM\Mapping\Driver\AnnotationDriver;

return array(
    'router' => array(
        'routes' => array(
            'course' => array(
                'type'    => Segment::class,
                'options' => array(
                    'route'    => '/course[/][:action][/:course_id]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'calendar_id'     => '[a-zA-Z0-9;:@&%=+\/\$_.-]*',
                    ),
                    'defaults' => array(
                        'controller' => CourseController::class,
                        'action'     => 'index',
                        ),
                ),
            ),
            'plan' => array(
                'type'    => Segment::class,
                'options' => array(
                    'route'    => '/plan[/][:action][/:plan_id]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'plan_id'     => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => PlanController::class,
                        'action'     => 'index',
                    ),
                ),
            ), 
            'time' => array(
                'type'    => Segment::class,
                'options' => array(
                    'route'    => '/time[/][:action][/:time_id]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'time_id'     => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => TimeController::class,
                        'action'     => 'index',
                    ),
                ),
            ),
        ),
    ),
    'controllers' => array(
        'factories' => array(
            CourseController::class => \Course\Service\Factory\CourseFactory::class,
            PlanController::class => \Course\Service\Factory\PlanFactory::class,
            TimeController::class => \Course\Service\Factory\TimeFactory::class,
        ),
    ),

     'doctrine' => array(
        'driver' => array(
            'application_entities' => array(
                'class' =>'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                //'cache' => 'filesystem',
                'paths' => array(__DIR__ . '/../src/Course/Entity')
            ),

            'orm_default' => array(
                'drivers' => array(                    
                    'Course\Entity'  => 'application_entities',                    
                )
            ),
        ),
    ),
    'view_manager' => array(
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        //'not_found_template'       => 'error/404',
        //'exception_template'       => 'error/index',
        'template_map' => array(
            //'layout/layout'           => __DIR__ . '/../view/layout/layout.phtml',
            'course/course/index' => __DIR__ . '/../view/course/course/index.phtml',
            //'error/404'               => __DIR__ . '/../view/error/404.phtml',
            //'error/index'             => __DIR__ . '/../view/error/index.phtml',
        ),
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
    ),

);
