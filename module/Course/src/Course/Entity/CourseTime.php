<?php

namespace Course\Entity;
use Doctrine\ORM\Mapping as ORM;
use Zend\InputFilter\Factory as InputFactory;     
use Zend\InputFilter\InputFilter;                 
use Zend\InputFilter\InputFilterAwareInterface;  
use Zend\InputFilter\InputFilterInterface;      

/** @ORM\Entity 
 *  @ORM\Table(name="coursetime");
*/
class CourseTime implements InputFilterAwareInterface{
    /**
     * @var integer
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    protected $time_id; 
    
    /**
     * @var time
     * @ORM\Column(type="time", nullable=false) 
     */
    protected $begin;   
    
    /**
     * @var time
     * @ORM\Column(type="time", nullable=false) 
     */
    protected $end;   
    
    /**
     * @var boolean
     * @ORM\Column(type="boolean")
     */
    protected $time_active;
    
    /**
     * @var Collection
     * @ORM\OneToMany(targetEntity="CourseDay", mappedBy="time_id", cascade={"all"})
     */
    protected $courses; 
    
    protected $inputFilter;
    
    //getters and setters
    public function getTime_id() {
        return $this->time_id;
    } 
        
    public function getBegin() {
        return $this->begin;
    }
    
    public function getEnd() {
        return $this->end;
    }
    
    public function getTime_Active() {
        return $this->time_active;
    }
    
    public function setTime_id($time_id) {
        $this->time_id = $time_id;
    }

    public function setBegin($begin) {
        $this->begin = $begin;
    }
   
    public function setEnd($end) {
        $this->end = $end;
    }
    
    public function setTime_Active($time_active) {
        $this->time_active = $time_active;
    }
    
    public function getdeletable(){
        $today= new \DateTime('today');
        foreach($this->courses as $course){
            $timediff=$course->getCourse_Date()->getTimeStamp()-$today->getTimeStamp();//Zeit des Starts abziehen
            $timediffdays=round($timediff/87000); //Umrechnen in Tage
            if($timediffdays>0) return false;
        }
        return true;
    }
    
    //Functions
     /** void __construct(array $options) constructor, if array is given, it initializes the data */
    public function __construct(array $options = null) {
        if (is_array($options)) {
            $this->setOptions($options);
        }
    }

    /** getArrayCopy()
     * This method is needed for Hydrator, when binding forms 
     * @return void 
     */
    public function getArrayCopy()
    {
        return get_object_vars($this);
    }
      /** 
     * ___set(string $name, type $value)
     * Checks if set-method exists and returns it
     * @retrun vo_id
     */
    public function __set($name, $value) {
        $method = 'set' . $name;
        if (!method_exists($this, $method)) {
            throw new \Exception('Invalid Method: ' . $method);
        }
        return $this->$method($value);
    }
    
    /** __get(string $name)
     * Checks if get-method exists and returns it 
     * @return vo_id
     */
    public function __get($name) {
        $method = 'get' . $name;
        if (!method_exists($this, $method)) {
            throw new \Exception('Invalid Method: ' . $method);
        }
        return $this->$method;
    }
    /** 
     * exchangeArray(array $data)
     * This method is needed for data exchange and hydrator (when binding forms) 
     * @return vo_id 
     */
    public function exchangeArray($data)
    {
        foreach ($data as $key=>$value){
            $method = 'set' . ucfirst($key);
            if(method_exists($this, $method)){
                $this->$method($value);
            }
        }
    }

    /**
     * setInputFilter(InputFilterInterface $inputFiler)
     * Right now no use, but needs to be defined for InputfilterAwareInterface 
     * @return Exception
     */
    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Filter ".$inputFilter." Not used");
    }

    /** getInputFilter()
     *  Defines filter for form inputs (e. g.  length), needed for form val_idation 
     *  @return InputFilter
     */
    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory     = new InputFactory();
            
            //Filter mitgl_nr
            //$this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }
}
