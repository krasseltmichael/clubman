<?php

namespace Course\Entity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\EntityRepository;
use Zend\InputFilter\Factory as InputFactory;     
use Zend\InputFilter\InputFilter;                 
use Zend\InputFilter\InputFilterAwareInterface;  
use Zend\InputFilter\InputFilterInterface;       

/** @ORM\Entity; 
 *  @ORM\Table(name="course");
*/

class Course extends EntityRepository implements InputFilterAwareInterface{
    /**
     * @var integer
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    protected $course_id; 
    
    /**
     * @var string
     * @ORM\Column(type="string", nullable=true) 
     */
    protected $course_name;
    
    /**
     * @var string
     * @ORM\Column(type="string", nullable=false) 
     */
    protected $course_quantity;
    
    /**
     * @var string
     * @ORM\Column(type="string", nullable=false) 
     */
    protected $course_health;
    
    /**
     * @var string
     * @ORM\Column(type="integer", nullable=false) 
     */
    protected $course_duration;
    
    /**
     * @var string
     * @ORM\Column(type="string", nullable=false) 
     */
    protected $course_14day;
    
    /**
     * @var boolean
     * @ORM\Column(type="boolean")
     */
    protected $course_active;
    
   protected $inputFilter;
    
    //getters and setters
    public function getCourse_id() {
        return $this->course_id;    }    
   
    public function getCourse_Name() {
        return $this->course_name;
    }
    
    public function getCourse_Quantity() {
        return $this->course_quantity;
    }
    
    public function getCourse_Health() {
        return $this->course_health;
    }
    
    public function getCourse_Active() {
        return $this->course_active;
    }
    
    public function getCourse_Duration() {
        return $this->course_duration;
    }
    
    public function getCourse_14Day() {
        return $this->course_14day;
    }
    
    public function setCourse_id($course_id) {
        $this->course_id = $course_id;
    }
            
    public function setCourse_Name($course_name) {
        $this->course_name = $course_name;
    }
    
    public function setCourse_Quantity($course_quantity) {
        $this->course_quantity = $course_quantity;
    }
    
    public function setCourse_Health($course_health) {
        $this->course_health = $course_health;
    }
    
    public function setCourse_Active($course_active) {
        $this->course_active = $course_active;
    }
    
    public function setCourse_duration($course_duration) {
        $this->course_duration = $course_duration;
    }
    
    public function setCourse_14Day($course_14day) {
        $this->course_14day = $course_14day;
    }
    
    /** void __construct(array $options) constructor, if array is given, it initializes the data */
    public function __construct(array $options = null) {
        if (is_array($options)) {
            $this->setOptions($options);
        }
    }

    /** getArrayCopy()
     * This method is needed for Hydrator, when binding forms 
     * @return void 
     */
    public function getArrayCopy()
    {
        return get_object_vars($this);
    }
      /** 
     * ___set(string $name, type $value)
     * Checks if set-method exists and returns it
     * @retrun vo_id
     */
    public function __set($name, $value) {
        $method = 'set' . $name;
        if (!method_exists($this, $method)) {
            throw new \Exception('Invalid Method: ' . $method);
        }
        return $this->$method($value);
    }
    
    /** __get(string $name)
     * Checks if get-method exists and returns it 
     * @return vo_id
     */
    public function __get($name) {
        $method = 'get' . $name;
        if (!method_exists($this, $method)) {
            throw new \Exception('Invalid Method: ' . $method);
        }
        return $this->$method;
    }
    /** 
     * exchangeArray(array $data)
     * This method is needed for data exchange and hydrator (when binding forms) 
     * @return vo_id 
     */
    public function exchangeArray($data)
    {
        foreach ($data as $key=>$value){
            $method = 'set' . ucfirst($key);
            if(method_exists($this, $method)){
                $this->$method($value);
            }
        }
    }

    /**
     * setInputFilter(InputFilterInterface $inputFiler)
     * Right now no use, but needs to be defined for InputfilterAwareInterface 
     * @return Exception
     */
    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Filter ".$inputFilter." Not used");
    }

    /** getInputFilter()
     *  Defines filter for form inputs (e. g.  Duration), needed for form val_idation 
     *  @return InputFilter
     */
    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory     = new InputFactory();
            
            //Filter course_id
            $inputFilter->add($factory->createInput(array(
                'name'     => 'course_id',
                'required' => true,
                'filters'  => array(
                    array('name' => 'Int'),
                ),
            )));
            
            //Filter name
            $inputFilter->add($factory->createInput(array(
                'name'     => 'course_name',
                'required' => true,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'val_idators' => array(
                    array(
                        'name'    => 'StringDuration',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min'      => 1,
                            'max'      => 100,
                        ),
                    ),
                ),
            )));
            
            //Filter quantity
            $inputFilter->add($factory->createInput(array(
                'name'     => 'course_quantity',
                'required' => true,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'val_idators' => array(
                    array(
                        'name'    => 'StringDuration',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min'      => 1,
                            'max'      => 100,
                        ),
                    ),
                ),
            )));
            
            //Filter health
            $inputFilter->add($factory->createInput(array(
                'name'     => 'course_health',
                'required' => true,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'val_idators' => array(
                    array(
                        'name'    => 'StringDuration',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min'      => 1,
                            'max'      => 100,
                        ),
                    ),
                ),
            )));
/*
            //Filter _personType
            $inputFilter->add($factory->createInput(array(
                'name'     => '_personType',
                'required' => true,
                'filters'  => array(
                    array('name' => 'Int'),
                ),
            )));*/

            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    }
