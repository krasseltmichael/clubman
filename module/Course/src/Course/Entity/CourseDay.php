<?php

namespace Course\Entity;
use Doctrine\ORM\Mapping as ORM;
use Zend\InputFilter\Factory as InputFactory;     
use Zend\InputFilter\InputFilter;                 
use Zend\InputFilter\InputFilterAwareInterface;  
use Zend\InputFilter\InputFilterInterface;       

/** @ORM\Entity 
 *  @ORM\Table(name="courseday");
*/
class CourseDay implements InputFilterAwareInterface{
    /**
     * @var integer
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    protected $day_id; 
    
     /**
     * @var date
     * @ORM\Column(type="date", nullable=false) 
     */
    protected $course_date;
    
     /**
     * @var integer
     * @ORM\ManyToOne(targetEntity="Course", inversedBy="CourseDay", cascade={"persist"})
     * @ORM\JoinColumn(name="course_id", referencedColumnName="course_id")
     */
    protected $course_id;
    
     /**
     * @var integer
     * @ORM\ManyToOne(targetEntity="CourseTime", inversedBy="CourseDay", cascade={"persist"})
     * @ORM\JoinColumn(name="time_id", referencedColumnName="time_id")
     */
    protected $time_id;
    
    /**
     * @var boolean
     * @ORM\Column(type="boolean")
     */
    protected $day_active;
    
    protected $inputFilter;
    
    //getters and setters
    public function getDay_id() {
        return $this->day_id;    } 
        
    public function getCourse_id() {
        return $this->course_id;
    } 
    
    public function getTime_id() {
        return $this->time_id;
    }
   
    public function getCourse_Date() {
        return $this->course_date;
    } 
    
    public function getDay_active() {
        return $this->day_active;
    }
    
    public function setDay_id($day_id) {
        $this->day_id = $day_id;
    }            
    public function setCourse_id($course_id) {
        $this->course_id = $course_id;
    }
    
    public function setTime_id($time_id) {
        $this->time_id = $time_id;
    }
    
    public function setCourse_Date($course_date) {
        $this->course_date = $course_date;
    } 
    
    public function setDay_active($day_active) {
        $this->day_active = $day_active;
    }

    //Functions
     /** void __construct(array $options) constructor, if array is given, it initializes the data */
    public function __construct(array $options = null) {
        if (is_array($options)) {
            $this->setOptions($options);
        }
    }

    /** getArrayCopy()
     * This method is needed for Hydrator, when binding forms 
     * @return void 
     */
    public function getArrayCopy()
    {
        return get_object_vars($this);
    }
      /** 
     * ___set(string $name, type $value)
     * Checks if set-method exists and returns it
     * @retrun vo_id
     */
    public function __set($name, $value) {
        $method = 'set' . $name;
        if (!method_exists($this, $method)) {
            throw new \Exception('Invalid Method: ' . $method);
        }
        return $this->$method($value);
    }
    
    /** __get(string $name)
     * Checks if get-method exists and returns it 
     * @return vo_id
     */
    public function __get($name) {
        $method = 'get' . $name;
        if (!method_exists($this, $method)) {
            throw new \Exception('Invalid Method: ' . $method);
        }
        return $this->$method;
    }
    /** 
     * exchangeArray(array $data)
     * This method is needed for data exchange and hydrator (when binding forms) 
     * @return vo_id 
     */
    public function exchangeArray($data)
    {
        foreach ($data as $key=>$value){
            $method = 'set' . ucfirst($key);
            if(method_exists($this, $method)){
                $this->$method($value);
            }
        }
    }

    /**
     * setInputFilter(InputFilterInterface $inputFiler)
     * Right now no use, but needs to be defined for InputfilterAwareInterface 
     * @return Exception
     */
    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Filter ".$inputFilter." Not used");
    }

    /** getInputFilter()
     *  Defines filter for form inputs (e. g.  length), needed for form val_idation 
     *  @return InputFilter
     */
    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory     = new InputFactory();
            
            //Filter mitgl_nr
            //$this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }
}