<?php

namespace Course\Form;

use Zend\Form\Form;

/**
 * Defines a Form for person entity
 *
 * @author Nadja
 */
class CourseTimeForm extends Form {
    
    /** __construct()
     * Constructor that defines elements of form 
     * @return void 
    */
    function __construct() {
        parent::__construct('course');
        $this->setAttribute('method', 'post');
              
        $this->add(array(
            'name' => 'begin',
            'attributes' => array(
                'type'  => 'time',
            ),
            'options' => array(
                'label' => 'Startzeit',
            ),
        )); 

        $this->add(array(
            'name' => 'end',
            'attributes' => array(
                'type'  => 'time',
            ),
            'options' => array(
                'label' => 'Endzeit',
            ),
        ));
               
        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'time_active',
            'value' => '1',
            'options' => array(
                'use_hidden_element' => true,
                'checked_value' => '1',
                'unchecked_value' => '0',
                'label' => 'Aktiv',
                'value_options' => array(
                    '0' =>'nein',
                    '1' =>'ja',
                ),
            ),
            'attributes' => array(
                'value' => '1' //set checked to '1'
            )
        ));
               
        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type'  => 'submit',
                'value' => 'Absenden',
                '_id' => 'submitbutton',
            ),
        ));
    }
}
