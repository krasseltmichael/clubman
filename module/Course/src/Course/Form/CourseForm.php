<?php

namespace Course\Form;
use Zend\Form\Form;

/**
 * Defines a Form for person entity
 *
 * @author Nadja
 */
class CourseForm extends Form {
    
    /** __construct()
     * Constructor that defines elements of form 
     * @return void 
    */
    function __construct() {
        parent::__construct('course');
        $this->setAttribute('method', 'post');
        $this->add(array(
            'name' => 'course_id',
            'attributes' => array(
                'type'  => 'hidden',
            ),
        ));
        
        $this->add(array(
            'type' => 'Text',
            'name' => 'course_name',
            'options' => array(
                'label' => 'Kursbezeichnung',
                ),
            'attributes' => array(
                'value' => ''
            )
        ));
        
        $this->add(array(
            'name' => 'course_quantity',
            'attributes' => array(
                'type'  => 'text',
            ),
            'options' => array(
                'label' => 'Teilnehmerzahl ',
            ),
        )); 
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'course_health',
            'options' => array(
                'use_hidden_element' => true,
                'checked_value' => '1',
                'unchecked_value' => '0',
                'label' => 'Krankenkassen gef.',
                'value_options' => array(
                    '0' =>'nein',
                    '1' =>'ja',
                ),
            ),
            'attributes' => array(
                'value' => '0',
            )
        ));
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'course_active',
            'options' => array(
                'use_hidden_element' => true,
                'checked_value' => '1',
                'unchecked_value' => '0',
                'label' => 'Kurs aktiv',
                'value_options' => array(   
                    '0' =>'nein',
                    '1' =>'ja',
                ),
            ),
        ));
                    
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'course_duration',
            'options' => array(
                'use_hidden_element' => true,
                'checked_value' => '1',
                'unchecked_value' => '0',
                'label' => 'Dauer',
                'value_options' => array(
                    '30' =>'30 min',
                    '45' =>'45 min',
                    '60' =>'60 min',
                ),
            ),
            'attributes' => array(
                'value' => '0',
            )
        ));        
                
        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'course_14day',
            'options' => array(
                'use_hidden_element' => true,
                'checked_value' => '1',
                'unchecked_value' => '0',
                'label' => '14-tägig',
                'value_options' => array(
                    '0' =>'nein',
                    '1' =>'ja',
                ),
            ),
            'attributes' => array(
                'value' => '0',
            )
        ));
        
        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type'  => 'submit',
                'value' => 'Absenden',
                '_id' => 'submitbutton',
            ),
        ));
    }
}
