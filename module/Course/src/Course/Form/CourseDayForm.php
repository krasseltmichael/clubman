<?php

namespace Course\Form;

use Zend\Form\Form;

/**
 * Defines a Form for person entity
 *
 * @author Nadja
 */
class CourseDayForm extends Form {
    
    /** __construct()
     * Constructor that defines elements of form 
     * @return void 
    */
    function __construct() {
        parent::__construct('course');
        $this->setAttribute('method', 'post');
        
        $this->add(array(
            'type' => 'Text',
            'name' => 'course_name',
            'options' => array(
                'label' => 'Kursbezeichnung',
            ),
            'attributes' => array(
                'value' => ''
            )
        ));
        
        $this->add(array(
            'name' => 'course_startdate',
            'attributes' => array(
                'type'  => 'date',
            ),
            'options' => array(
                'label' => 'Start',
            ),
        )); 
        
        $this->add(array(
            'name' => 'course_enddate',
            'attributes' => array(
                'type'  => 'date',
            ),
            'options' => array(
                'label' => 'Ende',
            ),
        ));  

        $this->add(array(
            'name' => 'course_id',
            'type' => 'Zend\Form\Element\Select',
            'options' =>array(
                'label' => 'Kurs',
            ),
        ));
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'time_id',
            'options' => array(
                'label' => 'Beginn',
                'value_options' => array(
                    '0' => '',
                    '1' => '09:00 Uhr',
                    '2' => '10:00 Uhr',
                    '3' => '11:00 Uhr',
                    '4' => '12:00 Uhr',
                    '5' => '13:00 Uhr',
                    '6' => '14:00 Uhr',
                    '7' => '15:00 Uhr',
                    '8' => '16:00 Uhr',
                    '9' => '17:00 Uhr',
                    '10' => '18:00 Uhr',
                    '11' => '19:00 Uhr',
                    '11' => '20:00 Uhr',
                ),
            ),
            'attributes' => array(
                'value' => '0' //set selected to '1'
            )
        ));
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'course_length',
            'options' => array(
                'use_hidden_element' => true,
                'checked_value' => '1',
                'unchecked_value' => '0',
                'label' => 'Dauer',
                'value_options' => array(
                    '0' =>'30 min',
                    '1' =>'45 min',
                    '2' =>'60 min',
                ),
            ),
            'attributes' => array(
                'value' => '0',
            )
        ));        
                
        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'course_14day',
            'options' => array(
                'use_hidden_element' => true,
                'checked_value' => '1',
                'unchecked_value' => '0',
                'label' => '14-tägig',
                'value_options' => array(
                    '0' =>'nein',
                    '1' =>'ja',
                ),
            ),
            'attributes' => array(
                'value' => '0',
            )
        ));
        
        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type'  => 'submit',
                'value' => 'Absenden',
                '_id' => 'submitbutton',
            ),
        ));
    }
}
