<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Course\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Course\Entity\Course;
use Course\Entity\CourseDay;
use Course\Entity\CourseTime;
use Course\Form\CourseTimeForm;
use Doctrine\ORM\EntityManager;

class TimeController extends AbstractActionController {
        /**
     * Constructor is used for injecting dependencies into the controller.
     */
    public function __construct(EntityManager $entityManager) 
    {
        $this->entityManager = $entityManager;
    }  

    /**
     * Entity manager.
     * @var Doctrine\ORM\EntityManager 
     */
    public $entityManager;
    
    /** indexAcion()
     * executes action for IndexRoute, lists all times from database
     * @return ViewModel
     */
    public function indexAction()
    {
        //extract all times from Database
        $queryBuilder = $this->entityManager->getRepository('Course\Entity\CourseTime')->createQueryBuilder('ct');
        $queryBuilder->orderBy('ct.begin', 'ASC');
        $times=$queryBuilder->getQuery()->getresult();
        return new ViewModel(array('times' => $times));
    }            
    
    /** addAction()
     * Action for adding a time, checks if inputs are val_id and returns form 
     * @return array 
     */
     public function addAction() {
        $form = new CourseTimeForm();
        
        //if request is post the data will be saved else an empty form is returned
        if($this->request->isPost()){
            $time = new CourseTime();            
            //Validate Form
            //$form->setInputFilter($time->getInputFilter());            
            $form->setData($this->getRequest()->getPost());
            
            if($form->isValid()){
                $data=$this->getRequest()->getPost();
                $data["begin"]= new \DateTime($data["begin"]);
                $data["end"]= new \DateTime($data["end"]);
                $data["time_active"]=1;
                
                $time->exchangeArray($data);
                //save time in database
                $this->entityManager->persist($time);
                $this->entityManager->flush();
                //redirect to startpage
                return $this->redirect()->toRoute('plan');
            }
        }
       
        return array('form' => $form);
    }
    
    /** editAction()
     * Action for editing a time, checks if inputs are val_id and returns form
     * @return array
     */
    /*public function editAction(){
        //extract time_id from URL
        $time_id = (int) $this->params()->fromRoute('time_id', 0);
        
        //find time in databse
        $time = $this->entityManager->find('Course\Entity\CourseTime', $time_id);
        
        $form  = new CourseForm();
        
        //fill out form with time data
        $form->bind($time);
        
        //if time_id not found in database redirect to add plan
        if (!$time_id) {
            return $this->redirect()->toRoute('plan', array(
                'action' => 'edit'
            ));
        }
        
        //if request is post the data will be saved else an empty form is returned
        if($this->request->isPost()){            
            $form->setInputFilter($time->getInputFilter());
            //$form->setData($this->getRequest()->getPost());
            
            //if ($form->isValid()) {            
                $data=$this->getRequest()->getPost();
                print_r($data);
                $time->exchangeArray($data);
                //save time in database
                $this->entityManager->persist($time);
                $this->entityManager->flush();
            //}
            
            //redirect to start page
            return $this->redirect()->toRoute('plan');
        }
        return array('time_id' => $time_id, 'form' => $form);
    }
    */
    
    /** deleteAction()
     * Action for deleting a time 
     * @return ViewModel;
     */
    public function deleteAction() {
        
        //extract time_id from URL
        $time_id = (int) $this->params()->fromRoute('time_id',0);
        
        //find time in database
        $time = $this->entityManager->find('Course\Entity\CourseTime', $time_id);
        
        if($this->request->isPost()){
            
            //delete course in database
            $this->entityManager->remove($time);
            $this->entityManager->flush();
            
            //redirect to start page
            return $this->redirect()->toRoute('plan');
        }
        
        return new Viewmodel(array('time' => $time));
    }
}
