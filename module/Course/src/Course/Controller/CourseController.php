<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Course\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Course\Entity\Course;
use Course\Entity\Day;
use Course\Entity\Time;
use Course\Form\CourseForm;
use Course\Form\CourseDayForm;
use Course\Form\CourseTimeForm;
use Doctrine\ORM\EntityManager;
class CourseController extends AbstractActionController{
    /**
     * Constructor is used for injecting dependencies into the controller.
     */
    public function __construct(EntityManager $entityManager) 
    {
        $this->entityManager = $entityManager;
    }  

    /**
     * Entity manager.
     * @var Doctrine\ORM\EntityManager 
     */
    public $entityManager;
    
    /** indexAcion()
     * executes action for IndexRoute, lists all courses from database
     * @return ViewModel
     */
    public function indexAction()
    {
        //extract all courses from Database
        $queryBuilder = $this->entityManager->getRepository('Course\Entity\Course')->createQueryBuilder('c');
        $queryBuilder->orderBy('c.course_name', 'ASC');
        $courses=$queryBuilder->getQuery()->getresult();
        return new ViewModel(array('courses' => $courses));
    }            
    
    /** addAction()
     * Action for adding a course, checks if inputs are val_id and returns form 
     * @return array 
     */
     public function addAction() {
        $form = new CourseForm();
        
        //if request is post the data will be saved else an empty form is returned
        if($this->request->isPost()){
            $course = new Course();            
            //Validate Form
            //$form->setInputFilter($course->getInputFilter());            
            $form->setData($this->getRequest()->getPost());
            
            if($form->isValid()){
                $data=$this->getRequest()->getPost();
                $data["begin"]=new \DateTime($data["begin"]);
                //$data["end"]=new \DateTime($data["end"]);
                $data["active"]=1;
                $course->exchangeArray($data);
                //save course in database
                $this->entityManager->persist($course);
                $this->entityManager->flush();
                //redirect to startpage
                return $this->redirect()->toRoute('course');
            }
        }
       
        return array('form' => $form);
    }
    
    /** editAction()
     * Action for editing a course, checks if inputs are val_id and returns form
     * @return array
     */
    public function editAction(){
        //extract course_id from URL
        $course_id = (int) $this->params()->fromRoute('course_id', 0);
        
        //find course in databse
        $course = $this->entityManager->find('Course\Entity\Course', $course_id);
        
        $form  = new CourseForm();
        
        //fill out form with course data
        $form->bind($course);
        
        //if course_id not found in database redirect to add course
        if (!$course_id) {
            return $this->redirect()->toRoute('course', array(
                'action' => 'edit'
            ));
        }
        
        //if request is post the data will be saved else an empty form is returned
        if($this->request->isPost()){            
            $form->setInputFilter($course->getInputFilter());
            //$form->setData($this->getRequest()->getPost());
            
            //if ($form->isValid()) {            
                $data=$this->getRequest()->getPost();
                print_r($data);
                $course->exchangeArray($data);
                //save course in database
                $this->entityManager->persist($course);
                $this->entityManager->flush();
            //}
            
            //redirect to start page
            return $this->redirect()->toRoute('course');
        }
        return array('course_id' => $course_id, 'form' => $form);
    }
    
    /** deleteAction()
     * Action for deleting a course 
     * @return ViewModel;
     */
    public function deleteAction() {        
        //extract course_id from URL
        $course_id = (int) $this->params()->fromRoute('course_id',0);
        
        //find course in database
        $course = $this->entityManager->find('Course\Entity\Course', $course_id);
        
        if($this->request->isPost()){
            //delete course in database
            $data['course_active']=0;
            $course->exchangearray($data);
            $this->entityManager->persist($course);
            $this->entityManager->flush();
            
            //redirect to start page
            return $this->redirect()->toRoute('course');
        }
        
        return new Viewmodel(array('course' => $course));
    }
}
