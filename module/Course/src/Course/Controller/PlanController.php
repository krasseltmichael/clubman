<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Course\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Zend_Date;
use Course\Entity\Course;
use Course\Entity\CourseDay;
use Course\Entity\CourseTime;
use Course\Form\CourseDayForm;
use Doctrine\ORM\EntityManager;

class PlanController extends AbstractActionController {
    /**
     * Constructor is used for injecting dependencies into the controller.
     */
    public function __construct(EntityManager $entityManager) 
    {
        $this->entityManager = $entityManager;
    }  

    /**
     * Entity manager.
     * @var Doctrine\ORM\EntityManager 
     */
    public $entityManager;
    

    /** indexAcion()
     * executes action for IndexRoute, lists all courses from database
     * @return ViewModel
     */
    public function indexAction()
    {
        //date and time functions
        $emConfig = $this->entityManager->getConfiguration();
        $emConfig->addCustomDatetimeFunction('YEAR', 'DoctrineExtensions\Query\Mysql\Year');
        $emConfig->addCustomDatetimeFunction('MONTH', 'DoctrineExtensions\Query\Mysql\Month');
        $emConfig->addCustomDatetimeFunction('DAY', 'DoctrineExtensions\Query\Mysql\Day');

        //Letzten Montag bestimmen
        $monday = new \DateTime('monday this week');
        $sunday = new \DateTime('sunday this week');
        //extract all course from Database
        $queryBuilder = $this->entityManager->getRepository('Course\Entity\CourseDay')->createQueryBuilder('cd');
        $queryBuilder->AddSelect("DAY(cd.course_date), MONTH(cd.course_date), YEAR(cd.course_date)");
        $queryBuilder->LeftJoin('Course\Entity\Course', 'c', \Doctrine\ORM\Query\Expr\Join::WITH, 'cd.course_id=c.course_id');
        $queryBuilder->LeftJoin('Course\Entity\CourseTime', 'ct', \Doctrine\ORM\Query\Expr\Join::WITH, 'cd.time_id=ct.time_id');
        $queryBuilder->where('DAY(cd.course_date)>=:actday AND MONTH(cd.course_date)>=:actmonth AND YEAR(cd.course_date)>=:actyear'
                          . ' AND DAY(cd.course_date)<=:thatday AND MONTH(cd.course_date)<=:thatmonth AND YEAR(cd.course_date)<=:thatyear'
                          . ' AND cd.day_active=1')
                ->setParameters(
                                array(
                                        'actday' => $monday->format("d"), 
                                        'actmonth' => $monday->format("m"),
                                        'actyear' => $monday->format("Y"),
                                        'thatday' => $sunday->format("d"),
                                        'thatmonth' => $sunday->format("m"),
                                        'thatyear' => $sunday->format("Y")
                                    )
                                );
                              
        $queryBuilder->orderBy('cd.course_date', 'ASC');
        $queryBuilder->addorderBy('ct.begin', 'ASC');
        $queryBuilder->groupBy('ct.begin');
        //echo $queryBuilder->getQuery()->getDQL();
        $plans=$queryBuilder->getQuery()->getresult();
        //Uhrzeiten abfragen
        $queryBuilder = $this->entityManager->getRepository('Course\Entity\CourseTime')->createQueryBuilder('ct');
        $queryBuilder->orderBy('ct.begin', 'ASC');
        $times=$queryBuilder->getQuery()->getresult();
        
        
        return new ViewModel(array('plans' => $plans, 'times' => $times));
    }                   
    
    /** addAction()
     * Action for adding a course, checks if inputs are val_id and returns form 
     * @return array 
     */
    public function addAction() {
        $form = new CourseDayForm();
        
        //Kurse für select Abrufen
        $queryBuilder = $this->entityManager->getRepository('Course\Entity\Course')->createQuerybuilder('c');
        $queryBuilder->Where("c.course_active = 1");
        $queryBuilder->orderBy('c.course_name', 'ASC');
        $courses = $queryBuilder->getQuery()->getresult();
        $selectcourses=array();
        foreach($courses as $course){
          $selectcourses[$course->getCourse_id()]=$course->getCourse_Name();
        }
        //Ins Formular einbauen
        $form->get('course_id')->setOptions(array('value_options' => $selectcourses));
        
        //Zeiten für select Abrufen
        $queryBuilder = $this->entityManager->getRepository('Course\Entity\CourseTime')->createQuerybuilder('ct');
        $queryBuilder->orderBy('ct.begin', 'ASC');
        $times = $queryBuilder->getQuery()->getresult();
        $selecttimes=array();
        foreach($times as $time){
            $selecttimes[$time->getTime_id()]=$time->getBegin()->format("H:i");
        }
        //Ins Formular einbauen
        $form->get('time_id')->setOptions(array('value_options' => $selecttimes));
        
        //if request is post the data will be saved else an empty form is returned
        if($this->request->isPost()){
            //Validate Form
            //$form->setInputFilter($course->getInputFilter());            
            //$form->setData($this->getRequest()->getPost());
            //if($form->isValid()){
                $data=$this->getRequest()->getPost();
                unset($data["submit"]);
                
                //find course in databse
                $course = $this->entityManager->find('Course\Entity\Course', $data["course_id"]);
                //find time in databse
                $time = $this->entityManager->find('Course\Entity\CourseTime', $data["time_id"]);
        
                $start = new \DateTime($data["course_startdate"]);
                $end = new \DateTime($data["course_enddate"]);
                //$timediff=$end->getTimeStamp()-$start->getTimeStamp();//Zeit des Starts abziehen
                //$timediffdays=round($timediff/87000); //Umrechnen in Tage
                
                if($course->getCourse_14Day()){
                    $interval = \DateInterval::createFromDateString('+14 day');
                }else{
                    $interval = \DateInterval::createFromDateString('+7 day');
                }
                $period = new \DatePeriod($start, $interval, $end);
                foreach ($period as $date){
                    $CourseDay = new CourseDay();
                    $CourseDay->exchangeArray(array(
                            "course_date"=>$date,
                            "day_active"=>1,
                            "course_id"=>$course,
                            "time_id"=>$time,
                    ));
                    //save course in database
                    $this->entityManager->persist($CourseDay);
                    $this->entityManager->flush();
                    unset($CourseDay);
                }
                //redirect to startpage
                return $this->redirect()->toRoute('plan');
        }
       
        return array('form' => $form);
    }
    
    /** editAction()
     * Action for editing a course, checks if inputs are val_id and returns form
     * @return array
     */
    public function editAction(){
        //extract course_id from URL
        $course_id = (int) $this->params()->fromRoute('course_id', 0);
        
        //find course in databse
        $course = $this->entityManager->find('Course\Entity\Course', $course_id);
        
        $form  = new CourseForm();
        
        //fill out form with course data
        $form->bind($course);
        
        //if course_id not found in database redirect to add course
        if (!$course_id) {
            return $this->redirect()->toRoute('course', array(
                'action' => 'edit'
            ));
        }
        
        //if request is post the data will be saved else an empty form is returned
        if($this->request->isPost()){            
            $form->setInputFilter($course->getInputFilter());
            //$form->setData($this->getRequest()->getPost());
            
            if ($form->isValid()) {            
                $data=$this->getRequest()->getPost();
                $course->exchangeArray($data);
                //save course in database
                $this->entityManager->persist($course);
                $this->entityManager->flush();
            }
            
            //redirect to start page
            return $this->redirect()->toRoute('plan');
        }
        return array('course_id' => $course_id, 'form' => $form);
    }
    
    /** deleteAction()
     * Action for deleting a time 
     * @return ViewModel;
     */
    public function deleteAction() {        
        //extract day_id from URL
        $day_id = (int) $this->params()->fromRoute('day_id',0);
        
        //find time in database
        $day = $this->entityManager->find('Course\Entity\CourseDay', $day_id);
        
        if($this->request->isPost()){
            
            //delete course in database
            $this->entityManager->remove($day);
            $this->entityManager->flush();
            
            //redirect to start page
            return $this->redirect()->toRoute('plan');
        }
        
        return new Viewmodel(array('day' => $day));
    }
}
