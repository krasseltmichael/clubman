<?php

namespace Cashbook\Entity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\EntityRepository;
use Zend\InputFilter\Factory as InputFactory;     
use Zend\InputFilter\InputFilter;                 
use Zend\InputFilter\InputFilterAwareInterface;  
use Zend\InputFilter\InputFilterInterface;       
use Doctrine\Common\Collections\ArrayCollection;

class CashbookRepository extends EntityRepository{
    /**
     * function create balance
     */
    public function calculateBalance_all($datum=null,$mode=null){                
        //Generating Query
        $querybuilder = $this->_em->createQuerybuilder('c')
                ->select("c")
                ->from("Cashbook\Entity\Cashbook", "c");
            //Switch between FullSumm, Plus/Minus Mode- preselecting Entrys
            if(is_null($mode)){ $querybuilder->andwhere("(c.cashbook_status = '+' OR c.cashbook_status = '-')");
            }else if($mode=="+"){ $querybuilder->andwhere("c.cashbook_status = '+'");
            }else if($mode=="-"){ $querybuilder->andwhere("c.cashbook_status = '-'");
        }
        //Retrieving all Matching Entrys 
        $entrys = $querybuilder->getQuery()->getresult();
        
        //Iterate Through all Entry Summing them up 
        $summe=0;
        foreach ($entrys as $cashbookentry){
            if($cashbookentry->getCashbook_Status()=='+'){
                $summe+=$cashbookentry->getCashbook_value();
            }
            if($cashbookentry->getCashbook_Status()=='-'){
                $summe-=$cashbookentry->getCashbook_value();
            }
        }
        return $summe;
    }
    
    /**
     * function create balance of day
    */
    public function calculateBalance_day($datum=null,$mode=null){
        is_null($datum)?$day = date("Y-m-d%"):"";     
                
        $querybuilder = $this->_em->createQuerybuilder('c')
                ->select("c")
                ->from("Cashbook\Entity\Cashbook", "c")
                ->where("c.cashbook_time LIKE '$day'");
          if(is_null($mode)){ $querybuilder->andwhere("(c.cashbook_status = '+' OR c.cashbook_status = '-')");
        }else if($mode=="+"){ $querybuilder->andwhere("c.cashbook_status = '+'");
        }else if($mode=="-"){ $querybuilder->andwhere("c.cashbook_status = '-'");
        }
        $entrys = $querybuilder->getQuery()->getresult();
        
        $summe=0;
        foreach ($entrys as $cashbookentry){
            if($cashbookentry->getCashbook_Status()=='+'){
                $summe+=$cashbookentry->getCashbook_value();
            }
            if($cashbookentry->getCashbook_Status()=='-'){
                $summe-=$cashbookentry->getCashbook_value();
            }
        }
        return $summe;
    }
        
    /**
     * function create balance of month
    */
    public function calculateBalance_month($month=null,$mode=null){
        is_null($month)?$month = date("Y-m-%"):"";     
                
        $querybuilder = $this->_em->createQuerybuilder('c')
                ->select("c")
                ->from("Cashbook\Entity\Cashbook", "c")
                ->where("c.cashbook_time LIKE '$month'");
          if(is_null($mode)){ $querybuilder->andwhere("(c.cashbook_status = '+' OR c.cashbook_status = '-')");
        }else if($mode=="+"){ $querybuilder->andwhere("c.cashbook_status = '+'");
        }else if($mode=="-"){ $querybuilder->andwhere("c.cashbook_status = '-'");
        }
        
        $entrys = $querybuilder->getQuery()->getresult();
        
        $summe=0;
        foreach ($entrys as $cashbookentry){
            if($cashbookentry->getCashbook_Status()=='+'){
                $summe+=$cashbookentry->getCashbook_value();
            }
            if($cashbookentry->getCashbook_Status()=='-'){
                $summe-=$cashbookentry->getCashbook_value();
            }
        }
        return $summe;
    }
    
    /**
     * Funtion Checks Database for existing Cashbookentrys
     * @return int Entrynumber
     */
    public function getNextCashbookEntryNumber(){
        $querybuilder = $this->_em->createQuerybuilder('c')
                ->select("c")
                ->from("Cashbook\Entity\Cashbook", "c")
                ->addOrderBy('c.cashbook_nr', 'DESC')
                ->where("(c.cashbook_status = '+' OR c.cashbook_status = '-')");
                //->setMaxResults(1);
        $entrys = $querybuilder->getQuery()->getresult();
        $cashbookentry=$entrys[0];
        if(is_object($cashbookentry))
            return ($cashbookentry->getCashbook_Nr()+1);
        else
            return 1;
    }
        
    /**
     * function create bon
    */
    public function getbon($getbon=null){
        if(is_null($getbon))return;     
                
        $querybuilder = $this->_em->createQuerybuilder('c')
                ->select("c")
                ->from("Cashbook\Entity\Cashbook", "c")
                ->where("c.cashbook_nr = ".$getbon);
        $entrys = $querybuilder->getQuery()->getresult();
        
        return $entrys  ;
    }
    
    /**
     * function create bon
    */
    public function findarticle($getarticle=null){
        if(is_null($getarticle))return;     
                
        $querybuilder = $this->_em->createQuerybuilder('p')
                ->select("p.name")
                ->from("Cashbook\Entity\Position", "p")
                ->where("p.cashbook = ".$getarticle);
        $entrys = $querybuilder->getQuery()->getresult();
        
        return $articles  ;
    }
}