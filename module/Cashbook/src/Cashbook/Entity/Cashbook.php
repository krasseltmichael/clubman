<?php

namespace Cashbook\Entity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\EntityRepository;
use Zend\InputFilter\Factory as InputFactory;     
use Zend\InputFilter\InputFilter;                 
use Zend\InputFilter\InputFilterAwareInterface;  
use Zend\InputFilter\InputFilterInterface;       
use Doctrine\Common\Collections\ArrayCollection;
use Cashbook\Entity\Position;

/** @ORM\Entity; 
 *  @ORM\Table(name="cashbook");
 *  @ORM\Entity(repositoryClass="Cashbook\Entity\CashbookRepository")
 */
class Cashbook extends EntityRepository implements InputFilterAwareInterface{
    /**
     * @var integer
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    protected $cashbook_id; 
           
    /**
     * @var integer
     * @ORM\Column(type="integer", nullable=true) 
     */
    protected $cashbook_nr;
    
    /**
     * @var date
     * @ORM\Column(type="datetime", nullable=true) 
     */
    protected $cashbook_time;
    
    /**
     * @ORM\OneToMany(targetEntity="position", mappedBy="cashbook", cascade={"all"})
     */
    public $positions;
    
    /**
     * @var string
     * @ORM\Column(type="string", nullable=false) 
     */
    protected $cashbook_status;
        
    /**
     * @var string
     * @ORM\Column(type="string", nullable=true) 
     */
    protected $cashbook_value;
    
    /**
     * @var string
     * @ORM\Column(type="string", nullable=true) 
     */
    protected $cashbook_tax;
           
    protected $inputFilter;
   
   /** 
     * exchangePriceformat($data)
     * This method is needed for data exchange numberformat bei _price 
     * @return string 
     */
    public function convertMoney($data)
    {
        $this->cashbook_value=floatval(str_replace(',', '.', str_replace('.', '', $data["cashbook_value"])));
    }
    
    //getters and setters
    public function getCashbook_id() {
        return $this->cashbook_id;    }    
   
    public function getCashbook_Nr() {
        return $this->cashbook_nr;
    }
    
    public function getCashbook_Time() {
        return $this->cashbook_time;
    }
    
    public function getPositions() {
        return $this->positions;
    }
       
    public function getCashbook_Status() {
        return $this->cashbook_status;
    }
    
    public function getCashbook_Value() {
        return $this->cashbook_value;
    }
    
    public function getCashbook_Tax() {
        return $this->cashbook_tax;
    }    
    
    public function setCashbook_Value($cashbook_value) {
        $this->cashbook_value = $cashbook_value;
    }
    
    public function setCashbook_Tax($cashbook_tax) {
        $this->cashbook_tax = $cashbook_tax;
    }
        
    public function setCashbook_id($cashbook_id) {
        $this->cashbook_id = $cashbook_id;
    }
    
    public function setCashbook_Nr($cashbook_nr) {
        $this->cashbook_nr = $cashbook_nr;
    }
    
    public function setCashbook_Time(\DateTime $cashbook_time) {
        $this->cashbook_time = $cashbook_time;
    }
        
    public function setPositions($position) {
        $this->positions = $position;
    }
    
    public function setCashbook_Status($cashbook_status) {
        $this->cashbook_status = $cashbook_status;
    }
    
    public function addPosition($position){
        $this->positions[]=$position;
    }
    
    public function printValue(){
        return $this->getCashbook_Status().number_format((float) $this->getCashbook_Value(),2,",",".")." €";
    }
    public function printPrice(){
        return number_format((float) $this->getCashbook_Value(),2,",",".")." €";
    }
            
    public function printTax(){
        return ($this->getCashbook_tax()*100)." %";
    }
    
    public function printTaxbon(){
        return (number_format((float) $this->getCashbook_Tax() * $this->getCashbook_Value(),2,",","."))." €";
    }
    
    /** void __construct(array $options) constructor, if array is given, it initializes the data */
    public function __construct(array $options = null) {
        if (is_array($options)) {
            $this->setOptions($options);
        }
        $this->positions = new ArrayCollection();
    }

    /** getArrayCopy()
     * This method is needed for Hydrator, when binding forms 
     * @return void 
     */
    public function getArrayCopy(){
        return get_object_vars($this);
    }
    
    /** 
     * ___set(string $name, type $value)
     * Checks if set-method exists and returns it
     * @retrun vo_id
     */
    public function __set($name, $value) {
        $method = 'set' . $name;
        if (!method_exists($this, $method)) {
            throw new \Exception('Invalid Method: ' . $method);
        }
        return $this->$method($value);
    }
    
    /** __get(string $name)
     * Checks if get-method exists and returns it 
     * @return vo_id
     */
    public function __get($name) {
        $method = 'get' . $name;
        if (!method_exists($this, $method)) {
            throw new \Exception('Invalid Method: ' . $method);
        }
        return $this->$method;
    }
    /** 
     * exchangeArray(array $data)
     * This method is needed for data exchange and hydrator (when binding forms) 
     * @return vo_id 
     */
    public function exchangeArray($data){
        foreach ($data as $key=>$value){
            $method = 'set' . ucfirst($key);
            if(method_exists($this, $method)){
                $this->$method($value);
            }
        }
    }

    /**
     * setInputFilter(InputFilterInterface $inputFiler)
     * Right now no use, but needs to be defined for InputfilterAwareInterface 
     * @return Exception
     */
    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Filter ".$inputFilter." Not used");
    }

    /** getInputFilter()
     *  Defines filter for form inputs (e. g. Usteuer), needed for form val_idation 
     *  @return InputFilter
     */
    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory     = new InputFactory();
            
            //Filter cashbook_id
            $inputFilter->add($factory->createInput(array(
                'name'     => 'cashbook_id',
                'required' => true,
                'filters'  => array(
                    array('name' => 'Int'),
                ),
            )));

            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    }
