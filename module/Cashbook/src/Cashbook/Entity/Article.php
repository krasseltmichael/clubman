<?php

namespace Cashbook\Entity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\EntityRepository;
use Zend\InputFilter\Factory as InputFactory;     
use Zend\InputFilter\InputFilter;                 
use Zend\InputFilter\InputFilterAwareInterface;  
use Zend\InputFilter\InputFilterInterface;       

/** @ORM\Entity; 
 *  @ORM\Table(name="article");
*/

class Article implements InputFilterAwareInterface{
    /**
     * @var integer
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    protected $article_id; 
    
    /**
     * @var string
     * @ORM\Column(type="string", nullable=false) 
     */
    protected $article_active;
           
    /**
     * @var string
     * @ORM\Column(type="string", nullable=true) 
     */
    protected $article_name;
    
    /**
     * @var string
     * @ORM\Column(type="string", nullable=true) 
     */
    protected $article_group;
    
    /**
     * @var string
     * @ORM\Column(type="string", nullable=false) 
     */
    protected $article_tax;
    
    /**
     * @var decimal
     * @ORM\Column(type="decimal", precision=6, scale=2) 
     */
    protected $article_price;
    
    /**
     * @var float
     * @ORM\Column(type="float", nullable=false) 
     */
    protected $article_amount;
            
   protected $inputFilter;
    
   /** 
     * exchangePriceformat($data)
     * This method is needed for data exchange numberformat bei _price 
     * @return string 
     */
    public function convertPrice($data)
    {
        $this->article_price = floatval(str_replace(',', '.', str_replace('.', ',', $data["article_price"])));
    }
    
    /** 
     * exchangePriceformat($data)
     * This method is needed for data exchange numberformat bei _price 
     * @return string 
     */
    public function reconvertPrice($data)
    {
        $this->article_price = floatval(str_replace('.', ',', str_replace(',', '.', $data["article_price"])));
    }
    
    //getters and setters
    public function getArticle_id() {
        return $this->article_id;            
    }

    public function getArticle_Active() {
        return $this->article_active;            
    }    
   
    public function getArticle_Name() {
        return $this->article_name;
    }
    
    public function getArticle_Group() {
        return $this->article_group;
    }
    
    public function getArticle_Tax() {
        return $this->article_tax;
    }
    
    public function getArticle_Price() {
        return $this->article_price;
    }
    
    public function getArticle_Amount() {
        return $this->article_amount;
    }
    
    public function setArticle_id($article_id) {
        $this->article_id = $article_id;
    }
    
    public function setArticle_Active($article_active) {
        $this->article_active = $article_active;
    }
    
    public function setArticle_Name($article_name) {
        $this->article_name = $article_name;
    }
    
    public function setArticle_Group($article_group) {
        $this->article_group = $article_group;
    }
        
    public function setArticle_Tax($article_tax) {
        $this->article_tax = $article_tax;
    }
    
    public function setArticle_Price($article_price) {
        $this->article_price = $article_price;
    }
    
     public function setArticle_Amount($article_amount) {
        $this->article_amount = $article_amount;
    }
    
    public function printPrice(){
        return $this->article_price = number_format((float) $this->getArticle_Price(),2,",",".")." €";
    }
    
    public function printTax(){
        $taxformat = substr_replace( $this->getArticle_Tax(),'', -4, 2)." %";
        return preg_replace("/^0+/",  "", $taxformat);
    }
    
    /** void __construct(array $options) constructor, if array is given, it initializes the data */
    public function __construct(array $options = null) {
        if (is_array($options)) {
            $this->setOptions($options);
        }
    }

    /** getArrayCopy()
     * This method is needed for Hydrator, when binding forms 
     * @return void 
     */
    public function getArrayCopy()
    {
        return get_object_vars($this);
    }
      /** 
     * ___set(string $name, type $value)
     * Checks if set-method exists and returns it
     * @retrun vo_id
     */
    public function __set($name, $value) {
        $method = 'set' . $name;
        if (!method_exists($this, $method)) {
            throw new \Exception('Invalid Method: ' . $method);
        }
        return $this->$method($value);
    }
    
    /** __get(string $name)
     * Checks if get-method exists and returns it 
     * @return vo_id
     */
    public function __get($name) {
        $method = 'get' . $name;
        if (!method_exists($this, $method)) {
            throw new \Exception('Invalid Method: ' . $method);
        }
        return $this->$method;
    }
    /** 
     * exchangeArray(array $data)
     * This method is needed for data exchange and hydrator (when binding forms) 
     * @return vo_id 
     */
    public function exchangeArray($data)
    {
        foreach ($data as $key=>$value){
            $method = 'set' . ucfirst($key);
            if(method_exists($this, $method)){
                $this->$method($value);
            }
        }
    }

    /**
     * setInputFilter(InputFilterInterface $inputFiler)
     * Right now no use, but needs to be defined for InputfilterAwareInterface 
     * @return Exception
     */
    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Filter ".$inputFilter." Not used");
    }

    /** getInputFilter()
     *  Defines filter for form inputs (e. g.  Usteuer), needed for form val_idation 
     *  @return InputFilter
     */
    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory     = new InputFactory();
            
            //Filter article_id
            $inputFilter->add($factory->createInput(array(
                'name'     => 'article_id',
                'required' => true,
                'filters'  => array(
                    array('name' => 'Int'),
                ),
            )));

            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    }
