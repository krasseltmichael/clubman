<?php

namespace Cashbook\Entity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\EntityRepository;

/** @ORM\Entity; 
 *  @ORM\Table(name="position");
*/

class Position{
    /**
     * @var integer
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    protected $id; 
    
    /**
     * @var string
     * @ORM\Column(type="string", nullable=true) 
     */
    protected $name;
    
    /**
     * @var string
     * @ORM\Column(type="string", nullable=false) 
     */
    protected $tax;
    
    /**
     * @var decimal
     * @ORM\Column(type="decimal", precision=6, scale=2) 
     */
    protected $price;
    
    /**
     * @var float
     * @ORM\Column(type="float", nullable=false) 
     */
    protected $amount;
    
    /**
     * @ORM\ManyToOne(targetEntity="Cashbook", inversedBy="positions")
     * @ORM\JoinColumn(name="cashbook", referencedColumnName="cashbook_id", nullable=true)
     */
    private $cashbook;            
    
    protected $inputFilter;
    
    /** 
     * exchangePriceformat($data)
     * This method is needed for data exchange numberformat bei _price 
     * @return string 
     */
    public function convertPrice($data)
    {
        $this->price = floatval($data["price"]);
    }
    
    public function printPrice(){
        return number_format((float) $this->price,2,",",".")." €";
    }

      
    //getters and setters
    function getId() {
        return $this->id;
    }

    function getName() {
        return $this->name;
    }

    function getTax() {
        return $this->tax;
    }

    function getPrice() {
        return $this->price;
    }

    function getAmount() {
        return $this->amount;
    }

    function getCashbook() {
        return $this->cashbook;
    }

    function setCashbook($cashbook) {
        $this->cashbook = $cashbook;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setName($name) {
        $this->name = $name;
    }

    function setTax($tax) {
        $this->tax = $tax;
    }

    function setPrice(decimal $price) {
        $this->price = $price;
    }

    function setAmount($amount) {
        $this->amount = $amount;
    }

    /** void __construct(array $options) constructor, if array is given, it initializes the data */
    public function __construct(array $options = null) {
        if (is_array($options)) {
            $this->setOptions($options);
        }
    }

    /** getArrayCopy()
     * This method is needed for Hydrator, when binding forms 
     * @return void 
     */
    public function getArrayCopy()
    {
        return get_object_vars($this);
    }
    /** 
     * ___set(string $name, type $value)
     * Checks if set-method exists and returns it
     * @retrun vo_id
     */
    public function __set($name, $value) {
        $method = 'set' . $name;
        if (!method_exists($this, $method)) {
            throw new \Exception('Invalid Method: ' . $method);
        }
        return $this->$method($value);
    }
    
    /** __get(string $name)
     * Checks if get-method exists and returns it 
     * @return vo_id
     */
    public function __get($name) {
        $method = 'get' . $name;
        if (!method_exists($this, $method)) {
            throw new \Exception('Invalid Method: ' . $method);
        }
        return $this->$method;
    }
    
    /** 
     * exchangeArray(array $data)
     * This method is needed for data exchange and hydrator (when binding forms) 
     * @return vo_id 
     */
    public function exchangeArray($data)
    {
        foreach ($data as $key=>$value){
            $method = 'set' . ucfirst($key);
            if(method_exists($this, $method)){
                $this->$method($value);
            }
        }
    }

}
