<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Cashbook\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Doctrine\ORM\EntityManager;
use Cashbook\Entity\Article;
use Cashbook\Form\ArticleForm;

class ArticleController extends AbstractActionController 
{
    
    /**
     * Constructor is used for injecting dependencies into the controller.
     */
    public function __construct(EntityManager $entityManager) 
    {
        $this->entityManager = $entityManager;
    }  

    /**
     * Entity manager.
     * @var Doctrine\ORM\EntityManager 
     */
    public $entityManager;
        
    /** indexAcion()
     * executes action for IndexRoute, lists all articles from database
     * @return ViewModel
     */
    public function indexAction()
    {
        $tableHead = array(
            'a.article_name'=>array('Artikel','asc'),
            'a.article_group'=>array('Gruppe','noOrder'),
            'a.article_price'=>array('Preis','noOrder'),
            'a.article_tax'=>array('Steuer','noOrder'),
            'a.article_amount'=>array('Menge','noOrder'),
        );
        
        //$sortByString = '';
        $queryBuilder = $this->entityManager->getRepository('Cashbook\Entity\Article')->createQuerybuilder('a');
                           
        if($this->request->isPost()){
            $post = $this->getRequest()->getPost();
            //$sortByString = $post->sortByString;
            //$searchField = $post->searchField;
            //$searchText = $post->searchText;            
            
        }else{
            $articles = $queryBuilder->where('a.article_active = 1');
            $articles = $queryBuilder->orderBy('a.article_group, a.article_name', 'asc');
            $articles = $queryBuilder->getQuery()->getresult();
        }
        $search["action"]=array("article", 'index');
        //$search["sortByString"]=isset($sortByString)?$sortByString:"";
        //$search["searchText"]=isset($searchText)?$searchText:"";
        $search["tableHead"]=$tableHead;

        
        return new ViewModel(array('articles' => $articles, 'search'=> $search, 'post' => $this->getRequest()->getPost()));
        
    }
    
    /** addAction()
     * Action for adding a article, checks if inputs are val_id and returns form 
     * @return array 
     */
     public function addAction() {
        $form = new ArticleForm();
        
        //if request is post the data will be saved else an empty form is returned
        if($this->request->isPost()){
            $article = new Article();            
            //Validate Form
            //$form->setInputFilter($article->getInputFilter());            
            $form->setData($this->getRequest()->getPost());
            
            if($form->isValid()){
                $data=$this->getRequest()->getPost();
                $data["article_active"]=1;
                $article->reconvertPrice($data);
                
                // send article to database
                $article->exchangeArray($data);    
                
                //save article in database
                $this->entityManager->persist($article);
                $this->entityManager->flush();
                //redirect to startpage
                return $this->redirect()->toRoute('article');
            }
        }       
        return array('form' => $form);
    }
    
    /** editAction()
     * Action for editing a article, checks if inputs are val_id and returns form
     * @return array
     */
    public function editAction(){
        //extract article_id from URL
        $article_id = (int) $this->params()->fromRoute('article_id', 0);
        
        //find article in databse
        $article = $this->entityManager->find('Cashbook\Entity\Article', $article_id);
        
        $form  = new ArticleForm();        
        //fill out form with article data
        $form->bind($article);
        if($article->getArticle_Name()!='')
            $form->get('article_name')->setValue($article->getArticle_Name());        
        
        //if article_id not found in database redirect to add article
        if (!$article_id) {
            return $this->redirect()->toRoute('article', array(
                'action' => 'edit'
            ));
        }
        
        //if request is post the data will be saved else an empty form is returned
        if($this->request->isPost()){            
            //$form->setInputFilter($article->getInputFilter());
            $form->setData($this->getRequest()->getPost());
                $data=$this->getRequest()->getPost();
                $article->convertPrice($data);
                $article->exchangeArray($data);
                //save article in database
                $this->entityManager->persist($article);
                $this->entityManager->flush();
            
            //redirect to start page
            return $this->redirect()->toRoute('article');
        }
        return array('article_id' => $article_id, 'form' => $form);
    }
    
    /** deleteAction()
     * Action for deleting a article 
     * @return ViewModel;
     */
    public function deleteAction() {        
        //extract article_id from URL
        $article_id = (int) $this->params()->fromRoute('article_id',0);
        
        //find article in database
        $article = $this->entityManager->find('Cashbook\Entity\Article', $article_id);
        
        if($this->request->isPost()){
            //delete article in database
            $data['article_active']=0;
            $article->exchangearray($data);
            $this->entityManager->persist($article);
            $this->entityManager->flush();
            
            //redirect to start page
            return $this->redirect()->toRoute('article');
        }
        
        return new Viewmodel(array('article' => $article));
    }
    
    public function searchAction() {
        
        //extract article_id from URL
        $article_id = (int) $this->params()->fromRoute('article_id',0);
        
        //find article in database
        $article = $this->entityManager()->find('Cashbook\Entity\Article', $article_id);
        
        if($this->request->isPost()){
            //delete article in database
            //$data['active']=0;
            //$article->exchangearray($data);
            //var_dump($article);
            //$this->entityManager->persist($article);
            //$this->entityManager->flush();
            
            //redirect to start page
            return $this->redirect()->toRoute('article');
        }
        
        return new Viewmodel(array('article' => $article));
    }
}
