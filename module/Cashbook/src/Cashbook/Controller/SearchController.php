<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Cashbook\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Cashbook\Entity\Cashbook;
use Cashbook\Form\CashbookForm;
use Cashbook\Form\SearchForm;
use Cashbook\Form\SearchHiddenForm;
use Zend\Zend_Date;

class SearchController extends AbstractActionController
{
    /** @var Doctrine\ORM\Entitymanager needed for managing Objects from Database with doctrine */
    protected $entityManager;
    
    /** indexAcion()
     * executes action for IndexRoute, lists cashbook from database
     * @return ViewModel
     */
    public function indexAction()
    {
        $form=new \Cashbook\Form\SearchForm();   
        return new ViewModel(array('form' => $form));        
    }    
    /* searchfor()
     * general function to search for cashbooks
     * @return Cashbook array
     */
    private function searchfor($post,$queryBuilder){
        if(!empty($post['submit'])){
                unset($post['submit']);
                foreach($post as $key=>$search){
                    
                    if($key=='contract' && $search<=1)continue;
                    if($key=='runtime' && $search<=1)continue;
                    if($key=='gender' && $search<0)continue;
                    if($key=='gender' && $search==0){ $queryBuilder->andwhere("c.gender = '0'"); }
                    if($key=='active' && $search==0){ $queryBuilder->andwhere("c.active = '0'"); }
                    if($key=='zahlweise' && $search<=1)continue;
                    if(!empty($search)){
                        $queryBuilder->andwhere('c.'.$key.' LIKE :'.$key)->setParameter($key,"%$search%");
                    }
                }
                }
            $queryBuilder->orderBy('c.mitgl_nr', 'asc');
            return $queryBuilder->getQuery()->getresult();
    }
                    
    public function searchAction() {
        $tableHead = array(
            'c.mitgl_nr'=>array('Nr','asc'),
            'c.name'=>array('Name','noOrder'),
            'c.forename'=>array('Vorname','noOrder'),
            'c.born'=>array('geb. am','noOrder'),
            'c.city'=>array('Ort','noOrder'),
            'c.entry'=>array('Beitritt','noOrder'),
            'c.contract'=>array('Vertrag','noOrder'),
        );
        
        $sortByString = '';
           
        $queryBuilder = $this->entityManager->getRepository('Cashbook\Entity\Cashbook')->createQuerybuilder('c');
        if($this->request->isPost()){
            $post = $this->getRequest()->getPost();
            $sortByString = $post->sortByString;
            $searchField = $post->searchField;
            $searchText = $post->searchText;
            $tableHead = $this->getServiceLocator()->get('ManageTables\Controller\ManageTables')->getTableHead($sortByString, $tableHead);
            $sortByArray = $this->getServiceLocator()->get('ManageTables\Controller\ManageTables')->getSortArray($sortByString);

            $cashbooks=$this->searchfor($post,$queryBuilder);
            //For Export
            $form=new \Cashbook\Form\SearchHiddenForm();   
            $form->bind($post);
        
        }

        return new ViewModel(array('tableHead'=>$tableHead, 'cashbooks' => $cashbooks, 'form'=>$form, 'post' => $this->getRequest()->getPost()));
        
    }
    
    public function exportAction() {
        //$this->layout('layout/blankhtml');
        $queryBuilder = $this->entityManager->getRepository('Cashbook\Entity\Cashbook')->createQuerybuilder('c');
        $queryBuilder->orderBy('c.mitgl_nr', 'asc');
        $cashbooks = $queryBuilder->getQuery()->getresult();

        if($this->request->isPost()){
            $post = $this->getRequest()->getPost();
            $cashbooks=$this->searchfor($post,$queryBuilder);
        }
        //return new ViewModel(array('cashbooks' => $cashbooks));
        
        require_once("html2pdf/html2pdf.class.php");
        $content = '<style type="text/css">
<!--
table
{
    padding: 0;
    font-size: 10pt;
    text-align: left;
    vertical-align: top;
}
td
{
    width: 30mm;
    height: 6mm;
    padding: 5mm;
    border: solid 0.5mm white;
}

-->
</style>
<page>
<table>';
$x=1;
foreach($cashbooks as $cashbook){
    if(!(($x%4)-1)) $content.="<tr>";
        $content.='<td>';
            $content.= $cashbook->getForename()." ".$cashbook->getName()."<br>";
            $content.= $cashbook->getStreet()."<br>";
            $content.= $cashbook->getZipCode()." ".$cashbook->getCity();
        $content.='</td>';
    if(!(($x-4)%4)) $content.="</tr>";
    $x++;
}
if((count($cashbooks)%4)) $content.="</tr>";
$content .= '</table>
</page>';
        $html2pdf = new \HTML2PDF('I' ,'A4','de',true,'UTF-8',array(0, 0, 0, 0));
        $html2pdf->WriteHTML($content);
        $html2pdf->Output('Etiketten.pdf');
    }
}