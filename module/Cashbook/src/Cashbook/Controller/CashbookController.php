<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Cashbook\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Doctrine\ORM\EntityManager;
use Cashbook\Entity\Cashbook;
use Cashbook\Entity\Search;
use Cashbook\Form\CashbookForm;
use Cashbook\Form\CostsForm;
use Cashbook\Form\SearchForm;
use Cashbook\Entity\Position;

use ZendPdf\Color;
use ZendPdf\Font;
use ZendPdf\Page;
use ZendPdf\PdfDocument;

class CashbookController extends AbstractActionController 
{
    
    /**
     * Constructor is used for injecting dependencies into the controller.
     */
    public function __construct(EntityManager $entityManager) 
    {
        $this->entityManager = $entityManager;
    }  

    /**
     * Entity manager.
     * @var Doctrine\ORM\EntityManager 
     */
    public $entityManager;
        
    /** indexAcion()
     * executes action for IndexRoute, lists all cashbooks from database
     * @return ViewModel
     */
    public function indexAction(){
        $tableHead = array(
            'c.cashbook_nr'=>array('Belegnr.','asc'),
            'c.cashbook_time'=>array('Datum','noOrder'),
            'c.article_id'=>array('Vorgang','noOrder'),
            'p.name'=>array('Artikel','noOrder'),
            'c.cashbook_money'=>array('Betrag','noOrder'),
            'c.cashbook_tax'=>array('Steuersatz','noOrder'),
        );
        
        $sortByString = '';
        $queryBuilder = $this->entityManager->getRepository('Cashbook\Entity\Cashbook')->createQuerybuilder('c');
        if($this->request->isPost()){
            $post = $this->getRequest()->getPost();
            $sortByString = $post->sortByString;
            $searchField = $post->searchField;
            $searchText = $post->searchText;            
        }else{
            $queryBuilder->orderBy('c.cashbook_id', 'ASC');
            $cashbooks = $queryBuilder->getQuery()->getresult();
        }
        $search["action"]=array("cashbook", 'index');
        $search["sortByString"]=isset($sortByString)?$sortByString:"";
        $search["searchText"]=isset($searchText)?$searchText:"";
        $search["tableHead"]=$tableHead;
        //This Creates Sum
        $balances["all"] = $this->entityManager->getRepository('Cashbook\Entity\Cashbook')->calculateBalance_all(null,null);
        $balances["minus"] = $this->entityManager->getRepository('Cashbook\Entity\Cashbook')->calculateBalance_all(null,"-");
        $balances["plus"] = $this->entityManager->getRepository('Cashbook\Entity\Cashbook')->calculateBalance_all(null,"+");                
        return new ViewModel(array('cashbooks' => $cashbooks, 'search' => $search, 'balances'=> $balances));
    }
    
    /** dayAcion()
     * executes action for IndexRoute, lists all for day cashbooks from database
     * @return ViewModel
     */
    public function dayAction(){
        $tableHead = array(
            'c.cashbook_nr'=>array('Belegnr.','asc'),
            'c.cashbook_time'=>array('Datum','noOrder'),
            'c.article_id'=>array('Vorgang','noOrder'),
            'c.cashbook_name'=>array('Artikel','noOrder'),
            'c.cashbook_money'=>array('Betrag','noOrder'),
            'c.cashbook_tax'=>array('Steuersatz','noOrder'),
        );
        
        $queryBuilder = $this->entityManager->getRepository('Cashbook\Entity\Cashbook')
                        ->createQueryBuilder("c")
                        ->where("c.cashbook_time LIKE '".date("Y-m-d")."%'")
                        ->orderBy('c.cashbook_nr', 'ASC');
        $cashbooks = $queryBuilder->getQuery()->getresult();
        $search["tableHead"]=$tableHead;
        
        //This Creates a Daily Sum
        $balances["day_all"] = $this->entityManager->getRepository('Cashbook\Entity\Cashbook')->calculateBalance_day(null,null);
        $balances["day_minus"] = $this->entityManager->getRepository('Cashbook\Entity\Cashbook')->calculateBalance_day(null,"-");
        $balances["day_plus"] = $this->entityManager->getRepository('Cashbook\Entity\Cashbook')->calculateBalance_day(null,"+");   
        return new ViewModel(array('cashbooks' => $cashbooks, 'search' => $search, 'balances'=> $balances));
    }
    
    /** monthAcion()
     * executes action for IndexRoute, lists all for month cashbooks from database
     * @return ViewModel
     */
    public function monthAction(){
        $tableHead = array(
            'c.cashbook_nr'=>array('Belegnr.','asc'),
            'c.cashbook_time'=>array('Datum','noOrder'),
            'c.article_id'=>array('Vorgang','noOrder'),
            'c.cashbook_name'=>array('Artikel','noOrder'),
            'c.cashbook_money'=>array('Betrag','noOrder'),
            'c.cashbook_tax'=>array('Steuersatz','noOrder'),
        );
        
        $queryBuilder = $this->entityManager->getRepository('Cashbook\Entity\Cashbook')
                        ->createQueryBuilder("c")
                        ->where("c.cashbook_time LIKE '".date("Y-m-%")."%'")
                        ->orderBy('c.cashbook_nr', 'ASC');
        $cashbooks = $queryBuilder->getQuery()->getresult();
        $search["tableHead"]=$tableHead;
        
        //This Creates a Month Sum
        $balances["month_all"] = $this->entityManager->getRepository('Cashbook\Entity\Cashbook')->calculateBalance_month(null,null);
        $balances["month_minus"] = $this->entityManager->getRepository('Cashbook\Entity\Cashbook')->calculateBalance_month(null,"-");
        $balances["month_plus"] = $this->entityManager->getRepository('Cashbook\Entity\Cashbook')->calculateBalance_month(null,"+");
        return new ViewModel(array('cashbooks' => $cashbooks, 'search' => $search, 'balances'=> $balances,));
    }
    
    /** addAction()
     * Action for adding a cashbook, checks if inputs are val_id and returns form 
     * @return array 
     */
     public function addAction() {
        //$this->layout('layout/blankhtml');
        $form = new CashbookForm();
        
        //if request is post the data will be saved else an empty form is returned
        if($this->request->isPost()){
            $data=$this->getRequest()->getPost();
            $data['cashbook_nr']=$this->entityManager->getRepository('Cashbook\Entity\Cashbook')->getNextCashbookEntryNumber();
            $data['cashbook_time']=new \DateTime();
            
            //Daten Rauskopieren / Zend Meckert sonst
            $postarticles=$data["articles"];
            unset($data["articles"]);
            //Wiederhole bis keine Artikel mehr in Hauptliste
            while(count($postarticles)>0) {
                //Nehme ersten Eintrag als Beispielsteuer
                $tax = $postarticles[0]["tax"];
            
                //Finde alle Artikel mit selber Steuer
                foreach ($postarticles as $id=>$articledata){
                    if($articledata["tax"]==$tax){
                        //Kopiere passenden Articel in neue Gruppe
                        $taxgroup[$tax][]=$articledata;
                        //Entferne Verschobenen Artikel aus Gesamtliste
                        unset($postarticles[$id]);
                    }
                }
                //Array index neu vergeben
                $postarticles = array_values($postarticles);
            }
            //Für alle Steuerklassen Eigenen Kassenbucheintrag
            foreach ($taxgroup as $key=>$taxarticles){
                $summe=0;
                $cashbook = new Cashbook();            
                $cashbook->setCashbook_Status("+");
                $cashbook->setCashbook_tax($key);
                $this->entityManager->persist($cashbook);
                $this->entityManager->flush();
            
                foreach ($taxarticles as $articledata){
                    $summe+=$articledata["price"]*$articledata["amount"];
                    $position = new Position();
                    $position->convertPrice($articledata);
                    unset($articledata["price"]);
                    $articledata["cashbook"]=$cashbook;
                    $position->exchangeArray($articledata);
                    //$this->entityManager->persist($position);
                    //$this->entityManager->flush();
                    $cashbook->addPosition($position);
                }
                $data['cashbook_value']=$summe;
                $cashbook->exchangeArray($data);
                //save cashbook in database
                $this->entityManager->persist($cashbook);
                $this->entityManager->flush();
            }
            
            $json = \Zend\Json\Json::encode(array("cashbook_nr"=>$cashbook->getCashbook_Nr()));
            echo $json;
            //echo \Zend\Json\Json::prettyPrint($json, array("indent" => " "));
            $view = new ViewModel();
            $view->setTerminal(true);
            $view->setTemplate('cashbook/cashbook/addsafe');
            return $view;
            //return $this->redirect()->toRoute('cashbook');
        }       
        return array('form' => $form);
    }
    
    /** addCostsAction()
     * Action for adding a Coast to cashbook, checks if inputs are val_id and returns form 
     * @return array 
     */
     public function addCostsAction() {
        $form = new CostsForm();
        
        //if request is post the data will be saved else an empty form is returned
        if($this->request->isPost()){
            //Validate Form
            //$form->setInputFilter($cashbook->getInputFilter());            
            $form->setData($this->getRequest()->getPost());
            $data=$this->getRequest()->getPost();
            if(isset($data["submit"])){
                $data['cashbook_nr']=$this->entityManager->getRepository('Cashbook\Entity\Cashbook')->getNextCashbookEntryNumber();
                $data['cashbook_time']=new \DateTime();

                //Daten Rauskopieren / Zend Meckert sonst
                $postarticles=$data["articles"];
                unset($data["articles"]);
                //Konvertierungen der EIngaben
                foreach ($postarticles as $id=>$articledata){
                     $postarticles[$id]["tax"]=(String)(intval($articledata["tax"])/100);
                     $postarticles[$id]["price"]= floatval(str_replace(array(",","€"),array(".",""), $articledata["price"]));
                     $postarticles[$id]["amount"]= intval($articledata["amount"]);
                }

                //Wiederhole bis keine Artikel mehr in Hauptliste
                while(count($postarticles)>0) {
                    //Nehme ersten Eintrag als Beispielsteuer
                    $tax = $postarticles[0]["tax"];

                    //Finde alle Artikel mit selber Steuer
                    foreach ($postarticles as $id=>$articledata){   
                        if($articledata["tax"]==$tax){
                            //Kopiere passenden Articel in neue Gruppe
                            $taxgroup[$tax][]=$articledata;
                            //Entferne Verschobenen Artikel aus Gesamtliste
                            unset($postarticles[$id]);
                        }
                    }
                    //Array index neu vergeben
                    $postarticles = array_values($postarticles);
                }

                //Für alle Steuerklassen Eigenen Kassenbucheintrag
                foreach ($taxgroup as $key=>$taxarticles){
                    $summe=0;
                    $cashbook = new Cashbook();            
                    $cashbook->setCashbook_Status("-");
                    $cashbook->setCashbook_tax($key);
                    $this->entityManager->persist($cashbook);
                    $this->entityManager->flush();

                    foreach ($taxarticles as $articledata){
                        $summe+=$articledata["price"]*$articledata["amount"];
                        $position = new Position();
                        $position->convertPrice($articledata);
                        unset($articledata["price"]);
                        $articledata["cashbook"]=$cashbook;
                        $position->exchangeArray($articledata);
                        //$this->entityManager->persist($position);
                        //$this->entityManager->flush();
                        $cashbook->addPosition($position);
                    }
                    $data['cashbook_value']=$summe;
                    $cashbook->exchangeArray($data);
                    
                    //save cashbook in database
                    $this->entityManager->persist($cashbook);
                    $this->entityManager->flush();
                }
                //redirect to startpage
                return $this->redirect()->toRoute('cashbook');
                
            }
        }       
        return array('form' => $form, 'post' =>$this->getRequest()->getPost());
    }
    
    /** editAction()
     * Action for editing a cashbook, checks if inputs are val_id and returns form
     * @return array
     */
    public function editAction(){
        //extract cashbook_id from URL
        $cashbook_id = (int) $this->params()->fromRoute('cashbook_id', 0);
        
        //find cashbook in databse
        $cashbook = $this->entityManager->find('Cashbook\Entity\Cashbook', $cashbook_id);
        
        $form  = new CashbookForm();
        
        //fill out form with cashbook data
        $form->bind($cashbook);
        if($cashbook->getCashbook_Time()!='')
            $form->get('cashbook_time')->setValue($cashbook->getCashbook_Time()->format('Y-m-d'));        
        
        //if cashbook_id not found in database redirect to add cashbook
        if (!$cashbook_id) {
            return $this->redirect()->toRoute('cashbook', array(
                'action' => 'edit'
            ));
        }
        
        //if request is post the data will be saved else an empty form is returned
        if($this->request->isPost()){            
            //$form->setInputFilter($cashbook->getInputFilter());
            $form->setData($this->getRequest()->getPost());
            //if ($form->isValid()) {            
                $data=$this->getRequest()->getPost();
                $data["cashbook_time"]=new \Date($data["cashbook_time"]);
        
                $cashbook->exchangeArray($data);
                //save cashbook in database
                $this->entityManager->persist($cashbook);
                $this->entityManager->flush();
            //}
            
            //redirect to start page
            return $this->redirect()->toRoute('cashbook');
        }
        return array('cashbook_id' => $cashbook_id, 'form' => $form);
    }
    
    public function printAction(){
        $tableHead = array(
            'c.cashbook_nr'=>array('Nummer','noOrder'),
            'c.cashbook_time'=>array('Datum','noOrder'),
            'c.cashbook_name'=>array('Artikel','noOrder'),
            'c.cashbook_amount'=>array('Menge','noOrder'),
            'c.cashbook_money'=>array('Betrag','noOrder'),
            'c.cashbook_tax'=>array('Steuersatz','noOrder'),
        );
        //extract cashbook_nr from URL
        $cashbook_nr = (int) $this->params()->fromRoute('cashbook_id', 0);
        
        $queryBuilder = $this->entityManager->getRepository('Cashbook\Entity\Cashbook')
                        ->createQueryBuilder("c")
                        ->where("c.cashbook_nr LIKE :cashbook_nr")
                        ->setParameter("cashbook_nr", $cashbook_nr)
                        ->orderBy('c.cashbook_nr', 'DESC');
                        
        $cashbooks = $queryBuilder->getQuery()->getresult();
        $search["tableHead"]=$tableHead;
        
        //This Creates a bon
        //$balances["bon_all"] = $this->entityManager->getRepository('Cashbook\Entity\Cashbook')->calculateBalance_bon(null,null); 
        return new ViewModel(array('cashbooks' => $cashbooks, 'search' => $search));
    }    
    
    public function pdfAction(){
        // Load Zend_Pdf class  
        
    }
}