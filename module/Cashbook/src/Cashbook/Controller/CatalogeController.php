<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Cashbook\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Doctrine\ORM\EntityManager;
use Cashbook\Entity\Cataloge;
use Cashbook\Form\CatalogeForm;
use Cashbook\Form\SearchForm;

class CatalogeController extends AbstractActionController 
{
    
    /**
    * Entity manager.
    * @var Doctrine\ORM\EntityManager
    */
    private $entityManager;
  
    // Constructor method is used to inject dependencies to the controller.
    public function __construct(EntityManager $entityManager) 
    {
      $this->entityManager = $entityManager;
      $this->layout('layout/blankhtml');
    }  

    /** indexAcion()
     * executes action for IndexRoute, lists all cashbooks from database
     * @return ViewModel
     */
    public function indexAction()
    {
        $tableHead = array(
            'c.cashbook_nr'=>array('Belegnr.','asc'),
            'c.cashbook_time'=>array('Datum','noOrder'),
            'c.cashbook_text'=>array('Vorgang','noOrder'),
            'c.cashbook_money'=>array('Einnahme','noOrder'), 
            'c.cashbook_status'=>array('Ausgabe','noOrder'),
            'c.cashbook_tax'=>array('Steuersatz','noOrder'),
        );
        $search["tableHead"]=$tableHead;
        $queryBuilder = $this->entityManager->getRepository('Cashbook\Entity\Cashbook')->createQuerybuilder('c');
        $cashbooks = $queryBuilder->orderBy('c.cashbook_nr', 'asc');
        $cashbooks = $queryBuilder->getQuery()->getresult();
        
        return new ViewModel(array('cashbooks' => $cashbooks, 'search'=> $search, 'post' => $this->getRequest()->getPost()));        
    }    
    
    /** getCategoryAcion()
     * executes action for getByCategorieRoute, lists all articles for Categorie from database
     * @return ViewModel
     */
    public function getByCategoryAction(){
        $group = $this->params()->fromRoute('cataloge_group', -1);
        if($group>=0){
            $queryBuilder = $this->entityManager->getRepository('Cashbook\Entity\Article')->createQuerybuilder('a');
            $queryBuilder->where('a.article_active = 1')
                        ->ANDwhere('a.article_group=:group')
                        ->setParameter('group', $group)
                        ->orderBy('a.article_id', 'asc');
            $selected_articles = $queryBuilder->getQuery()->getresult();
            foreach($selected_articles as $article){
                $articles[]=$article->getArrayCopy();
            }
            $view = new ViewModel(array('json' => $articles));
        }else{
            $view = new ViewModel();
        }
        return $view->setTerminal(true);        
    }    
}
