<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Cashbook\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Doctrine\ORM\EntityManager;
use Cashbook\Entity\Cashbook;
use Cashbook\Form\CostsForm;

class CostsController extends AbstractActionController 
{    
    /**
     * Constructor is used for injecting dependencies into the controller.
     */
    public function __construct(EntityManager $entityManager) 
    {
        $this->entityManager = $entityManager;
    }  

    /**
     * Entity manager.
     * @var Doctrine\ORM\EntityManager 
     */
    public $entityManager;
        
    /** indexAcion()
     * executes action for IndexRoute, lists all cashbooks from database
     * @return ViewModel
     */
    public function indexAction()
    {
        $tableHead = array(
            'c.cashbook_text'=>array('Artikel','noOrder'),
            'c.cashbook_tax'=>array('Steuersatz','noOrder'),
        );
        
        $sortByString = '';
        $queryBuilder = $this->entityManager->getRepository('Cashbook\Entity\Cashbook')->createQuerybuilder('c');
                           
        if($this->request->isPost()){
            $post = $this->getRequest()->getPost();
            $sortByString = $post->sortByString;
            $searchField = $post->searchField;
            $searchText = $post->searchText;
            
        //foreach($cashbooks as $cashbook):
            for ($i=0; $i<count($cashbook->getCashbook_Money()); $i++){
                    $sumaus = ($cashbook->getCashbook_Money()[$i]['$cashbook->getCashbook_Money()']);
                //echo number_format($sumaus,2, ",", ".");
                echo $sumaus;
            }
            
        if ($cashbook->getCashbook_Status()=='-'){
        echo number_format((float) $cashbook->getCashbook_Money(), (int)$decimals = 2, (string)$dec_point = ",", (string)$thousands_sep = ".")." €";
        }
                        
        }else{
            $cashbooks = $queryBuilder->orderBy('c.cashbook_nr', 'asc');
            $cashbooks = $queryBuilder->getQuery()->getresult();
        }
        $search["action"]=array("cashbook", 'index');
        //$search["sortByString"]=isset($sortByString)?$sortByString:"";
        //$search["searchText"]=isset($searchText)?$searchText:"";
        $search["tableHead"]=$tableHead;
        
        return new ViewModel(array('cashbooks' => $cashbooks, 'search'=> $search, 'post' => $this->getRequest()->getPost()));
        
    }
    
    /** addAction()
     * Action for adding a cashbook, checks if inputs are val_id and returns form 
     * @return array 
     */
     public function addAction() {
        //$form = new CostsForm();
        
        //if request is post the data will be saved else an empty form is returned
        if($this->request->isPost()){
            $cashbook = new Costs();            
            //Validate Form
            //$form->setInputFilter($cashbook->getInputFilter());            
            $form->setData($this->getRequest()->getPost());
            
            if($form->isValid()){
                $data=$this->getRequest()->getPost();
                $cashbook->exchangeArray($data);
                //save cashbook in database
                $this->entityManager->persist($cashbook);
                $this->entityManager->flush();
                //redirect to startpage
                return $this->redirect()->toRoute('cashbook');
            }
        }       
        return;
    }
    
    /** editAction()
     * Action for editing a cashbook, checks if inputs are val_id and returns form
     * @return array
     */
    public function editAction(){
        //extract cashbook_id from URL
        $cashbook_id = (int) $this->params()->fromRoute('cashbook_id', 0);
        
        //find cashbook in databse
        $cashbook = $this->entityManager->find('Cashbook\Entity\Costs', $cashbook_id);
        
        $form  = new CostsForm();
        
        //fill out form with cashbook data
        $form->bind($cashbook);
        if($cashbook->getCashbook_Time()!='')
            $form->get('cashbook_time')->setValue($cashbook->getCashbook_Time()->format('Y-m-d'));        
        
        //if cashbook_id not found in database redirect to add cashbook
        if (!$cashbook_id) {
            return $this->redirect()->toRoute('cashbook', array(
                'action' => 'edit'
            ));
        }
        
        //if request is post the data will be saved else an empty form is returned
        if($this->request->isPost()){            
            //$form->setInputFilter($cashbook->getInputFilter());
            $form->setData($this->getRequest()->getPost());
            //if ($form->isValid()) {            
                $data=$this->getRequest()->getPost();
                $data["cashbook_time"]=new \Date($data["cashbook_time"]);
        
                $cashbook->exchangeArray($data);
                //save cashbook in database
                $this->entityManager->persist($cashbook);
                $this->entityManager->flush();
            //}
            
            //redirect to start page
            return $this->redirect()->toRoute('cashbook');
        }
        return array('cashbook_id' => $cashbook_id, 'form' => $form);
    }
    
    /** deleteAction()
     * Action for deleting a cashbook 
     * @return ViewModel;
     */
    public function deleteAction() {        
        //extract cashbook_id from URL
        $cashbook_id = (int) $this->params()->fromRoute('cashbook_id',0);
        
        //find cashbook in database
        $cashbook = $this->entityManager()->find('Cashbook\Entity\Costs', $cashbook_id);
        
        if($this->request->isPost()){
            //delete cashbook in database
            $data['active']=0;
            $cashbook->exchangearray($data);
            $this->entityManager->persist($cashbook);
            $this->entityManager->flush();
            
            //redirect to start page
            return $this->redirect()->toRoute('cashbook');
        }
        
        return new Viewmodel(array('cashbook' => $cashbook));
    }
    
    public function searchAction() {
        
        //extract cashbook_id from URL
        $cashbook_id = (int) $this->params()->fromRoute('cashbook_id',0);
        
        //find cashbook in database
        $cashbook = $this->entityManager()->find('Cashbook\Entity\Costs', $cashbook_id);
        
        if($this->request->isPost()){
            //delete cashbook in database
            //$data['active']=0;
            //$cashbook->exchangearray($data);
            //var_dump($cashbook);
            //$this->entityManager->persist($cashbook);
            //$this->entityManager->flush();
            
            //redirect to start page
            return $this->redirect()->toRoute('cashbook');
        }
        
        return new Viewmodel(array('cashbook' => $cashbook));
    }
}
