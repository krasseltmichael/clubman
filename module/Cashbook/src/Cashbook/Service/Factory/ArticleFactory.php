<?php
namespace Cashbook\Service\Factory;

use Zend\ServiceManager\Factory\FactoryInterface;
use Interop\Container\ContainerInterface;
use Doctrine\ORM\EntityManager;
use Cashbook\Controller\ArticleController;

/**
 * This is the factory for Articlecontroller. Its purpose is to instantiate the
 * service.
 */
class ArticleFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = NULL)
    {
        $entityManager = $container->get('doctrine.entitymanager.orm_default');
        return new ArticleController($entityManager);
    }
}