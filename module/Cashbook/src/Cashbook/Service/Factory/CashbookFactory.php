<?php 
/*Die datei brauchen wir für jeden controller, gibt noch ne bessere lösung aber die funktioniert bei mir noch nicht*/
namespace Cashbook\Service\Factory;

use Zend\ServiceManager\Factory\FactoryInterface;
use Interop\Container\ContainerInterface;
use Doctrine\ORM\EntityManager;
use Cashbook\Controller\CashbookController;

/**
 * This is the factory for Cashbookcontroller. Its purpose is to instantiate the
 * service.
 */
class CashbookFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = NULL)
    {
        $entityManager = $container->get('doctrine.entitymanager.orm_default');
        return new CashbookController($entityManager);
    }
}