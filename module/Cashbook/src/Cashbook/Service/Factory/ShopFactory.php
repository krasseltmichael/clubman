<?php
namespace Cashbook\Service\Factory;

use Zend\ServiceManager\Factory\FactoryInterface;
use Interop\Container\ContainerInterface;
use Doctrine\ORM\EntityManager;
use Cashbook\Controller\ShopController;

/**
 * This is the factory for Shopcontroller. Its purpose is to instantiate the
 * service.
 */
class ShopFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = NULL)
    {
        $entityManager = $container->get('doctrine.entitymanager.orm_default');
        return new ShopController($entityManager);
    }
}