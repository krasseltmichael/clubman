<?php

namespace Cashbook\Form;

use Zend\Form\Form;

class CostsForm extends Form {
    
    /** __construct()
     * Constructor that defines elements of form 
     * @return void 
    */
    function __construct() {
        parent::__construct('cashbook');
        $this->setAttribute('method', 'post');
        
//        $this->add(array(
//            'name' => 'cashbook_text',
//            'attributes' => array(
//                'type'  => 'text',
//            ),
//            'options' => array(
//                'label' => 'Buchungstext  ',
//            ),
//        ));
                
//        $this->add(array(
//            'name' => 'cashbook_money',
//            'attributes' => array(
//                'type'  => 'decimal',
//            ),
//            'options' => array(
//                'label' => ' Betrag  ',
//            ),
//            
//        ));
//                     
        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type'  => 'submit',
                'value' => 'Absenden',
                '_id' => 'submitbutton',
            ),
        ));

        $this->add(array(
            'name' => 'addrow',
            'attributes' => array(
                'type'  => 'submit',
                'value' => 'Posten hinzufügen',
                //'_id' => 'submitbutton',
            ),
        ));

    }
}
