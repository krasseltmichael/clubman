<?php

namespace Cashbook\Form;

use Zend\Form\Form;

class ArticleForm extends Form {
    
    /** __construct()
     * Constructor that defines elements of form 
     * @return void 
    */
    function __construct() {
        parent::__construct('article');
        $this->setAttribute('method', 'post');
        $this->add(array(
            'name' => 'article_id',
            'attributes' => array(
                'type'  => 'hidden',
            ),
        ));
        
        $this->add(array(
            'name' => 'article_name',
            'attributes' => array(
                'type'  => 'text',
            ),
            'options' => array(
                'label' => 'Artikel',
            ),
        ));
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'article_group',
            'options' => array(
                'label' => 'Artikelgruppe',
                'value_options' => array(
                    '0' => ' Getränke',
                    '1' => ' Riegel',
                    '2' => ' Kleidung',
                    '3' => ' Accessoires',
                    '4' => ' Sonstige',
                ),
            ),
            'attributes' => array(
                'value' => '0' //set checked to '0'
            )
        ));
                       
        $this->add(array(
            'name' => 'article_price',
            'attributes' => array(
                'type'  => 'type="decimal", precision=6, scale=2',
            ),
            'options' => array(
                'label' => 'Preis',
            ),
            
        ));$this->add(array(
            'name' => 'article_amount',
            'attributes' => array(
                'type'  => 'text',
            ),
            'options' => array(
                'label' => 'Menge',
            ),
        ));
        
         $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'article_tax',
            'options' => array(
                'label' => 'Umsatzsteuer',
                'value_options' => array(
                    '0.07' => '  7%',
                    '0.19' => ' 19%',
                ),
            ),
            'attributes' => array(
                'value' => '0' //set checked to '0'
            )
        ));
                     
        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type'  => 'submit',
                'value' => 'Absenden',
                '_id' => 'submitbutton',
            ),
        ));
    }
}
