<?php

namespace Cashbook\Form;

use Zend\Form\Form;

class CashbookForm extends Form {
    
    /** __construct()
     * Constructor that defines elements of form 
     * @return void 
    */
    function __construct() {
        parent::__construct('cashbook');
        $this->setAttribute('method', 'post');
        $this->add(array(
            'name' => 'cashbook_id',
            'attributes' => array(
                'type'  => 'hidden',
            ),
        ));
        
        $this->add(array(
            'name' => 'cashbook_nr',
            'attributes' => array(
                'type'  => 'text',
            ),
            'options' => array(
                'label' => 'Belegnummer  ',
            ),
        ));
        
        $this->add(array(
            'name' => 'cashbook_text',
            'attributes' => array(
                'type'  => 'text',
            ),
            'options' => array(
                'label' => 'Buchungstext  ',
            ),
        ));
                
        $this->add(array(
            'name' => 'cashbook_money',
            'attributes' => array(
                'type'  => 'decimal',
            ),
            'options' => array(
                'label' => ' Betrag  ',
            ),
            
        ));$this->add(array(
            'name' => 'cashbook_time',
            'attributes' => array(
                'type'  => 'date',
            ),
            'options' => array(
                'label' => ' Datum  ',
            ),
        ));
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Radio',
            'name' => 'cashbook_tax',
            'options' => array(
                //'label' => 'Umsatzsteuer',
                'value_options' => array(
                    '0' => '  7%',
                    '1' => ' 19%',
                ),
            ),
            'attributes' => array(
                'value' => '0' //set checked to '0'
            )
        ));
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Radio',
            'name' => 'cashbook_status',
            'options' => array(
                //'label' => 'Umsatzsteuer',
                'value_options' => array(
                    '-' => '  Ausgabe  ',
                    '+' => ' Einnahme  ',
                ),
            ),
            'attributes' => array(
                'value' => '+' //set checked to '0'
            )
        ));
                     
        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type'  => 'submit',
                'value' => 'Absenden',
                '_id' => 'submitbutton',
            ),
        ));
    }
}
