<?php

namespace Cashbook\Controller;
use Zend\Router\Http\Literal;
use Zend\Router\Http\Segment;
use Zend\Router\Http\Regex;
use Zend\ServiceManager\Factory\InvokableFactory;
use Zend\Mvc\Controller\LazyControllerAbstractFactory;
use Application\Route\StaticRoute;
use Doctrine\ORM\Mapping\Driver\AnnotationDriver;

return array(
    'router' => array(
        'routes' => array(
            'cashbook' => array(
                'type'    => Segment::class,
                'options' => array(
                    'route'    => '/cashbook[/][:action][/:cashbook_id]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'cashbook_id'     => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => CashbookController::class,
                            'action'     => 'index',
                        ),
                ),
            ),
            'shop' => array(
                'type'    => Segment::class,
                'options' => array(
                    'route'    => '/shop[/][:action][/:shop_id]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'shop_id'     => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => ShopController::class,
                            'action'     => 'index',
                        ),
                ),
            ),
            'article' => array(
                'type'    => Segment::class,
                'options' => array(
                    'route'    => '/article[/][:action][/:article_id]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'article_id'     => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => ArticleController::class,
                            'action'     => 'index',
                        ),
                ),
            ),
            'costs' => array(
                'type'    => Segment::class,
                'options' => array(
                    'route'    => '/costs[/][:action][/:costs_id]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'costs_id'     => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => CostsController::class,
                            'action'     => 'index',
                        ),
                ),
            ),
            'cataloge' => array(
                'type'    => Segment::class,
                'options' => array(
                    'route'    => '/cataloge[/][:action][/:cataloge_group]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'cataloge_id'     => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => CatalogeController::class,
                            'action'     => 'index',
                        ),
                ),
            ),
        ),
    ),
    'controllers' => array(
        'factories' => array(
            CashbookController::class => \Cashbook\Service\Factory\CashbookFactory::class,
            ShopController::class => \Cashbook\Service\Factory\ShopFactory::class,
            ArticleController::class => \Cashbook\Service\Factory\ArticleFactory::class,
            CostsController::class => \Cashbook\Service\Factory\CostsFactory::class,
            CatalogeController::class => LazyControllerAbstractFactory::class,
        ),
    ),
    'doctrine' => array(
        'driver' => array(
            'application_entities' => array(
                'class' =>'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                //'cache' => 'filesystem',
                'paths' => array(__DIR__ . '/../src/Cashbook/Entity')
            ),

            'orm_default' => array(
                'drivers' => array(                    
                    'Cashbook\Entity'  => 'application_entities',                    
                )
            ),
        ),
    ),
    'view_manager' => array(
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        //'not_found_template'       => 'error/404',
        //'exception_template'       => 'error/index',
        'template_map' => array(
            //'layout/layout'           => __DIR__ . '/../view/layout/layout.phtml',
            'cashbook/cashbook/index' => __DIR__ . '/../view/cashbook/cashbook/index.phtml',
            //'error/404'               => __DIR__ . '/../view/error/404.phtml',
            //'error/index'             => __DIR__ . '/../view/error/index.phtml',
        ),
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
    ),

);
