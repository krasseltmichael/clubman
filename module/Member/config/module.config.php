<?php

use Zend\Router\Http\Literal;
use Zend\Router\Http\Segment;
use Zend\Router\Http\Regex;
use Zend\ServiceManager\Factory\InvokableFactory;
use Application\Route\StaticRoute;
use Doctrine\ORM\Mapping\Driver\AnnotationDriver;

return array(
    'router' => array(
        'routes' => array(
            'member' => array(
                'type'    => Segment::class,
                'options' => array(
                    'route'    => '/member[/][:action][/:member_id]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        //person_id can be part of Url
                        'member_id'     => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => MemberController::class,
                        'action'     => 'index',
                        ),
                ),
            ),
            'visit' => array(
                'type'    => Segment::class,
                'options' => array(
                    'route'    => '/visit[/][:action][/:visit_id]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        //person_id can be part of Url
                        'visit_id'     => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => VisitController::class,
                        'action'     => 'index',
                    ),
                ),
            ),            
            'search' => array(
                'type'    => Segment::class,
                'options' => array(
                    'route'    => '/search[/][:action][/:search_id]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        //person_id can be part of Url
                        'search_id'     => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => SearchController::class,
                        'action'     => 'index',
                    ),
                ),
            ),
            'key' => array(
                'type'    => Segment::class,
                'options' => array(
                    'route'    => '/key[/][:action][/:key_id]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        //person_id can be part of Url
                        'key_id'     => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => KeyController::class,
                        'action'     => 'index',
                    ),
                ),
            ),            
        ),
     ),   
    'controllers' => array(
        'factories' => array(
            MemberController::class => \Member\Service\Factory\MemberFactory::class,
            SearchController::class => \Member\Service\Factory\SearchFactory::class,
            VisitController::class =>  \Member\Service\Factory\VisitFactory::class,
            KeyController::class =>  \Member\Service\Factory\KeyFactory::class,
        ),
    ),
    'doctrine' => array(
        'driver' => array(
            'application_entities' => array(
                'class' =>'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                //'cache' => 'filesystem',
                'paths' => array(__DIR__ . '/../src/Member/Entity')
            ),

            'orm_default' => array(
                'drivers' => array(                    
                    'Member\Entity'  => 'application_entities',                    
                )
            ),
        ),
    ),
    'view_manager' => array(
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        //'not_found_template'       => 'error/404',
        //'exception_template'       => 'error/index',
        'template_map' => array(
            //'layout/layout'           => __DIR__ . '/../view/layout/layout.phtml',
            'member/member/index' => __DIR__ . '/../view/member/member/index.phtml',
            //'error/404'               => __DIR__ . '/../view/error/404.phtml',
            //'error/index'             => __DIR__ . '/../view/error/index.phtml',
        ),
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
    ),

);
