<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Member\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Doctrine\ORM\EntityManager;
use Member\Entity\Member;
use Member\Entity\Visit;
use Member\Entity\LockerKey;
use Member\Form\VisitForm;
use Zend\Zend_Date;

class VisitController extends AbstractActionController
{
    /**
     * Constructor is used for injecting dependencies into the controller.
     */
    public function __construct(EntityManager $entityManager) 
    {
        $this->entityManager = $entityManager;
    }  

    /**
     * Entity manager.
     * @var Doctrine\ORM\EntityManager 
     */
    public $entityManager;
    
    /** indexAcion()
     * executes action for IndexRoute, lists all visits from database
     * @return ViewModel
     */
    public function indexAction()
    {        
        //extract all visits from Database
        $queryBuilder = $this->entityManager->getRepository('Member\Entity\Visit')->createQueryBuilder('v');
        $queryBuilder->where('v.checkout IS NULL');
        $queryBuilder->orderBy('v.visit_id', 'ASC');
        $visits=$queryBuilder->getQuery()->getresult();
        return new ViewModel(array('visits' => $visits));
    }            
       
    
    /** addAction()
     * Action for adding a visit, checks if inputs are val_id and returns form 
     * @return array 
     */
   
    public function addAction() {
        //if request is post 
        if($this->request->isPost()){
            $visit = new Visit();
            $data=$this->getRequest()->getPost();
            $data["checkin"]=new \DateTime();
            $visit->exchangeArray($data);
            
            $queryBuilder = $this->entityManager->getRepository('Member\Entity\LockerKey')->createQueryBuilder('k');
            $queryBuilder->where('k.key_nr LIKE :param AND k.key_type = 1')->setParameter('param', $data['key_nr']);     /** Frage nach männlichen Schlüsseln */
            $queryBuilder->orderBy('k.key_nr', 'ASC');
            $keys=$queryBuilder->getQuery()->getresult();
        
            $keys = $this->entityManager->getRepository('Member\Entity\LockerKey')->findAll();  
            return new ViewModel(array('keys' => $keys));
        
            $queryBuilder = $this->entityManager->getRepository('Member\Entity\Member')->createQueryBuilder('m');
            $queryBuilder->where('m.mitgl_nr = :param AND m.active = 1')->setParameter('param', $data['mitgl_nr']);
            $members=$queryBuilder->getQuery()->getresult();
            
            if(count($members)==0) {
                return array('error' => 'Mitgliedsnummer nicht vergeben');
            
            }else{    
                    $setMember_id = $visit->setMember_id($members[0]);   

                    //save visit in database
                    $this->entityManager->persist($visit);
                    //$this->entityManager->persist($key);
                    $this->entityManager->flush();                        
                }                       
        }
        //redirect to startpage
        return $this->redirect()->toRoute('visit');
    }
    
    /** editAction()
     * Action for editing a visit, checks if inputs are val_id and returns form
     * @return array
     */
   
    public function editAction(){
        //extract visit_id from URL
        $visit_id = (int) $this->params()->fromRoute('visit_id', 0);
        
        //find visit in databse
        $visit = $this->entityManager->find('Member\Entity\Visit', $visit_id);
        
        $form  = new VisitForm();
        
        //fill out form with visit data
        $form->bind($visit);
        $form->get('checkin')->setValue($visit->getBorn()->format('d.m.Y'));
        $form->get('checkout')->setValue($visit->getEntry()->format('d.m.Y'));        
        
        
        //if visit_id not found in database redirect to add visit
        if (!$visit_id) {
            return $this->redirect()->toRoute('visit', array(
                'action' => 'edit'
            ));
        }
        
        //if request is post the data will be saved else an empty form is returned
        if($this->request->isPost()){            
            //$form->setInputFilter($visit->getInputFilter());
            $form->setData($this->getRequest()->getPost());
            //if ($form->isValid()) {            
                $data=$this->getRequest()->getPost();
                $date=new \DateTime();
                $date->createFromFormat('d.m.Y', $data["born"]);
                $data["born"]=$date;
                $date->createFromFormat('d.m.Y', $data["entry"]);
                $data["entry"]=$date;
                
                $visit->exchangeArray($data);
                //save visit in database
                $this->entityManager->persist($visit);
                $this->entityManager->flush();
            //}
            
            //redirect to start page
            return $this->redirect()->toRoute('visit');
        }
        return array('visit_id' => $visit_id, 'form' => $form);
    }
    
    /** deleteAction()
     * Action for deleting a visit 
     * @return ViewModel;
     */
    public function checkoutAction() {
        
        //extract visit_id from URL
        $visit_id = (int) $this->params()->fromRoute('visit_id',0);
        
        //find visit in database
        $visit = $this->entityManager->find('Member\Entity\Visit', $visit_id);
        
        $visit->setCheckout(new \DateTime());
        $this->entityManager->persist($visit);
        $this->entityManager->flush();
            
        //redirect to start page
        return $this->redirect()->toRoute('visit');
    }         
}
