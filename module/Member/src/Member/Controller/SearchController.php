<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Member\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Member\Entity\Member;
use Member\Form\MemberForm;
use Member\Form\SearchForm;
use Member\Form\SearchHiddenForm;
use Zend\Zend_Date;
use Doctrine\ORM\EntityManager;
class SearchController extends AbstractActionController
{
    /**
     * Constructor is used for injecting dependencies into the controller.
     */
    public function __construct(EntityManager $entityManager) 
    {
        $this->entityManager = $entityManager;
    }  

    /**
     * Entity manager.
     * @var Doctrine\ORM\EntityManager 
     */
    public $entityManager;
    
    /** indexAcion()
     * executes action for IndexRoute, lists member from database
     * @return ViewModel
     */
    public function indexAction()
    {
        $form=new \Member\Form\SearchForm();   
        return new ViewModel(array('form' => $form));        
    }    
    /* searchfor()
     * general function to search for members
     * @return Member array
     */
    private function searchfor($post,$queryBuilder){
        if(!empty($post['submit'])){
                unset($post['submit']);
                foreach($post as $key=>$search){
                    
                    if($key=='contract' && $search<=1)continue;
                    if($key=='runtime' && $search<=1)continue;
                    if($key=='gender' && $search<0)continue;
                    if($key=='gender' && $search==0){ $queryBuilder->andwhere("m.gender = '0'"); }
                    if($key=='active' && $search==0){ $queryBuilder->andwhere("m.active = '0'"); }
                    if($key=='zahlweise' && $search<=1)continue;
                    if(!empty($search)){
                        $queryBuilder->andwhere('m.'.$key.' LIKE :'.$key)->setParameter($key,"%$search%");
                    }
                }
                }
            $queryBuilder->orderBy('m.mitgl_nr', 'asc');
            return $queryBuilder->getQuery()->getresult();
    }
                    
    public function searchAction() {
        $tableHead = array(
            'm.mitgl_nr'=>array('Nr','asc'),
            'm.name'=>array('Name','noOrder'),
            'm.forename'=>array('Vorname','noOrder'),
            'm.born'=>array('geb. am','noOrder'),
            'm.city'=>array('Ort','noOrder'),
            'm.entry'=>array('Beitritt','noOrder'),
            'm.contract'=>array('Vertrag','noOrder'),
        );
        
        $sortByString = '';
           
        $queryBuilder = $this->entityManager->getRepository('Member\Entity\Member')->createQuerybuilder('m');
        if($this->request->isPost()){
            $post = $this->getRequest()->getPost();
            $sortByString = $post->sortByString;
            $searchField = $post->searchField;
            $searchText = $post->searchText;
            //$tableHead = $this->getServiceLocator()->get('ManageTables\Controller\ManageTables')->getTableHead($sortByString, $tableHead);
            //$sortByArray = $this->getServiceLocator()->get('ManageTables\Controller\ManageTables')->getSortArray($sortByString);

            $members=$this->searchfor($post,$queryBuilder);
            //For Export
            $form=new \Member\Form\SearchHiddenForm();   
            $form->bind($post);
        
        }

        return new ViewModel(array('tableHead'=>$tableHead, 'members' => $members, 'form'=>$form, 'post' => $this->getRequest()->getPost()));
        
    }
    
    public function exportAction() {
        //$this->layout('layout/blankhtml');
        $queryBuilder = $this->entityManager->getRepository('Member\Entity\Member')->createQuerybuilder('m');
        $queryBuilder->orderBy('m.mitgl_nr', 'asc');
        $members = $queryBuilder->getQuery()->getresult();

        if($this->request->isPost()){
            $post = $this->getRequest()->getPost();
            $members=$this->searchfor($post,$queryBuilder);
        }
        //return new ViewModel(array('members' => $members));
        
        require_once("html2pdf/html2pdf.class.php");
        $content = '<style type="text/css">
<!--
table
{
    padding: 0;
    font-size: 10pt;
    text-align: left;
    vertical-align: top;
}
td
{
    width: 30mm;
    height: 6mm;
    padding: 5mm;
    border: solid 0.5mm white;
}

-->
</style>
<page>
<table>';
$x=1;
foreach($members as $member){
    if(!(($x%4)-1)) $content.="<tr>";
        $content.='<td>';
            $content.= $member->getForename()." ".$member->getName()."<br>";
            $content.= $member->getStreet()."<br>";
            $content.= $member->getZipCode()." ".$member->getCity();
        $content.='</td>';
    if(!(($x-4)%4)) $content.="</tr>";
    $x++;
}
if((count($members)%4)) $content.="</tr>";
$content .= '</table>
</page>';
        $html2pdf = new \HTML2PDF('I' ,'A4','de',true,'UTF-8',array(0, 0, 0, 0));
        $html2pdf->WriteHTML($content);
        $html2pdf->Output('Etiketten.pdf');
    }
}