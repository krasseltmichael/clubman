<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Member\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Doctrine\ORM\EntityManager;
use Member\Form\KeyForm;
use Member\Entity\Member;
use Member\Entity\Visit;
use Member\Entity\Search;
use Member\Entity\LockerKey;

class KeyController extends AbstractActionController  {
    /**
     * Constructor is used for injecting dependencies into the controller.
     */
    public function __construct(EntityManager $entityManager) 
    {
        $this->entityManager = $entityManager;
    }  

    /**
     * Entity manager.
     * @var Doctrine\ORM\EntityManager 
     */
    public $entityManager;
    
    /** indexAcion()
     * executes action for IndexRoute, lists all keys from database
     * @return ViewModel
     */
    public function indexAction()
    {
        $tableHead = array(
            'k.key_nr'=>array('Nr.','asc'),
            'k.key_type'=>array('Typ','noOrder'),
        );
        
        $sortByString = '';
        $queryBuilder = $this->entityManager->getRepository('Member\Entity\LockerKey')
                                            ->createQuerybuilder('k')
                                            ->Where("k.key_active = 1");
            
        if($this->request->isPost()){
            $post = $this->getRequest()->getPost();
            $sortByString = $post->sortByString;
            $searchField = $post->searchField;
            $searchText = $post->searchText;
            
            if($post->sortByString != ''){
                $tableHead = $this->entityManager->get('ManageTables\Controller\ManageTables')->getTableHead($sortByString, $tableHead);
                $sortByArray = $this->entityManager->get('ManageTables\Controller\ManageTables')->getSortArray($sortByString);
                if(empty($searchField)){
                    $keys = $queryBuilder->getQuery()->getresult();
                }else{
                    $queryBuilder->where($searchField.' LIKE :param')->setParameter('param', '%'.$searchText.'%');
                    $counter = 0;
                    foreach($sortByArray as $key => $value){
                        if(!$counter){
                            $queryBuilder->orderBy($key,$value);
                        }
                        $queryBuilder->addOrderBy($key,$value);
                        $counter++;
                    }
                }
            }else{
                $queryBuilder->where($searchField.' LIKE :param')->setParameter('param', '%'.$searchText.'%');
            }
            $keys = $queryBuilder->getQuery()->getresult();
        }else{
            $keys = $queryBuilder->orderBy('k.key_nr', 'asc');
            $keys = $queryBuilder->getQuery()->getresult();
        }
        $search["action"]=array("key", 'index');
        $search["sortByString"]=isset($sortByString)?$sortByString:"";
        $search["searchText"]=isset($searchText)?$searchText:"";
        $search["tableHead"]=$tableHead;

        
        return new ViewModel(array('keys' => $keys, 'search'=> $search, 'post' => $this->getRequest()->getPost()));
        
    }
    
    /** addAction()
     * Action for adding a key, checks if inputs are val_id and returns form 
     * @return array 
     */
    public function addAction() {
        $form = new KeyForm();
        
        //if request is post the data will be saved else an empty form is returned
        if($this->request->isPost()){
            $key = new LockerKey();            
            //Validate Form
            //$form->setInputFilter($key->getInputFilter());            
            $form->setData($this->getRequest()->getPost());
            
            if($form->isValid()){
                $data=$this->getRequest()->getPost();
                $data["key_active"]=1;
                $key->exchangeArray($data);
                //save key in database
                $this->entityManager->persist($key);
                $this->entityManager->flush();
                //redirect to startpage
                return $this->redirect()->toRoute('key');
            }
        }
        return array('form' => $form);
    }
    
    /** editAction()
     * Action for editing a key, checks if inputs are val_id and returns form
     * @return array
     */
    public function editAction(){
        //extract key_id from URL
        $key_id = (int) $this->params()->fromRoute('key_id', 0);
        
        //find key in databse
        $key = $this->entityManager->find('Member\Entity\LockerKey', $key_id);
        
        $form  = new KeyForm();
        
        //fill out form with key data
        $form->bind($key);
        
        //if key_id not found in database redirect to add key
        if (!$key_id) {
            return $this->redirect()->toRoute('key', array(
                'action' => 'add'
            ));
        }
        
        //if request is post the data will be saved else an empty form is returned
        if($this->request->isPost()){            
            //$form->setInputFilter($key->getInputFilter());
            $form->setData($this->getRequest()->getPost());
            //if ($form->isValid()) {            
                $data=$this->getRequest()->getPost();
                
                $key->exchangeArray($data);
                //save key in database
                $this->entityManager->persist($key);
                $this->entityManager->flush();
            //}
            
            //redirect to start page
            return $this->redirect()->toRoute('key');
        }
        return array('key_id' => $key_id, 'form' => $form);
    }
    
    /** deleteAction()
     * Action for deleting a key 
     * @return ViewModel;
     */
    public function deleteAction() {        
        //extract key_id from URL
        $key_id = (int) $this->params()->fromRoute('key_id',0);
        
        //find key in database
        $key = $this->entityManager->find('Member\Entity\LockerKey', $key_id);
        
        if($this->request->isPost()){
            //delete key in database
            $data['key_active']=0;
            $key->exchangearray($data);
            $this->entityManager->persist($key);
            $this->entityManager->flush();
            
            //redirect to start page
            return $this->redirect()->toRoute('key');
        }
        
        return new Viewmodel(array('key' => $key));
    }
    
    public function searchAction() {
        
        //extract key_id from URL
        $key_id = (int) $this->params()->fromRoute('key_id',0);
        
        //find key in database
        $key = $this->entityManager()->find('Member\Entity\LockerKey', $key_id);
        
        if($this->request->isPost()){
            //delete key in database
            //$data['active']=0;
            //$key->exchangearray($data);
            //var_dump($key);
            //$this->enitityManager->persist($key);
            //$this->entityManager->flush();
            
            //redirect to start page
            return $this->redirect()->toRoute('keys');
        }
        
        return new Viewmodel(array('key' => $key));
    }
}
