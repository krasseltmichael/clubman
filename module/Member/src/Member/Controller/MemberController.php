<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Member\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Doctrine\ORM\EntityManager;
use Member\Entity\Member;
use Member\Entity\Visit;
use Member\Entity\Search;
use Member\Entity\LockerKey;
use Member\Form\MemberForm;
use Member\Form\VisitForm;
use Member\Form\SearchForm;

class MemberController extends AbstractActionController 
{
    
    /**
     * Constructor is used for injecting dependencies into the controller.
     */
    public function __construct(EntityManager $entityManager) 
    {
        $this->entityManager = $entityManager;
    }  

    /**
     * Entity manager.
     * @var Doctrine\ORM\EntityManager 
     */
    public $entityManager;
       
    /** indexAcion()
     * executes action for IndexRoute, lists all members from database
     * @return ViewModel
     */
    public function indexAction()
    {
        $tableHead = array(
            'm.mitgl_nr'=>array('Nr.','asc'),
            'm.forename'=>array('Vorname','noOrder'),
            'm.name'=>array('Name','noOrder'),
            'm.city'=>array('Ort','noOrder'),
        );
        
        $sortByString = '';
        $queryBuilder = $this->entityManager->getRepository('Member\Entity\Member')->createQuerybuilder('m');
            
        if($this->request->isPost()){
            $post = $this->getRequest()->getPost();
            $sortByString = $post->sortByString;
            $searchField = $post->searchField;
            $searchText = $post->searchText;
            
            if($post->sortByString != ''){
                $tableHead = $this->getServiceLocator()->get('ManageTables\Controller\ManageTables')->getTableHead($sortByString, $tableHead);
                $sortByArray = $this->getServiceLocator()->get('ManageTables\Controller\ManageTables')->getSortArray($sortByString);
                if(empty($searchField)){
                    $members = $queryBuilder->getQuery()->getresult();
                }else{
                    $queryBuilder->where($searchField.' LIKE :param AND m.active = 1')->setParameter('param', '%'.$searchText.'%');
                    $counter = 0;
                    foreach($sortByArray as $key => $value){
                        if(!$counter){
                            $queryBuilder->orderBy($key,$value);
                        }
                        $queryBuilder->addOrderBy($key,$value);
                        $counter++;
                    }
                }
            }else{
                $queryBuilder->where($searchField.' LIKE :param AND m.active = 1')->setParameter('param', '%'.$searchText.'%');
            }
            $members = $queryBuilder->getQuery()->getresult();
        }else{
            $members = $queryBuilder->where('m.active = 1');
            $members = $queryBuilder->orderBy('m.mitgl_nr', 'asc');
            $members = $queryBuilder->getQuery()->getresult();
        }
        $search["action"]=array("member", 'index');
        $search["sortByString"]=isset($sortByString)?$sortByString:"";
        $search["searchText"]=isset($searchText)?$searchText:"";
        $search["tableHead"]=$tableHead;

        
        return new ViewModel(array('members' => $members, 'search'=> $search, 'post' => $this->getRequest()->getPost()));
        
    }
    
    /** addAction()
     * Action for adding a member, checks if inputs are val_id and returns form 
     * @return array 
     */
    public function addAction() {
        $form = new MemberForm();
        
        //if request is post the data will be saved else an empty form is returned
        if($this->request->isPost()){
            $member = new Member();            
            //Validate Form
            //$form->setInputFilter($member->getInputFilter());            
            $form->setData($this->getRequest()->getPost());
            
            //if($form->isValid()){
                $data=$this->getRequest()->getPost();
                $data["born"]=new \DateTime($data["born"]);
                $data["entry"]=new \DateTime($data["entry"]);
                $data["active"]=1;
                //echo "<pre>";
                //var_dump($member);
                //echo "</pre>";
                $member->exchangeArray($data);
                //save member in database
                $this->entityManager->persist($member);
                $this->entityManager->flush();
                //redirect to startpage
                return $this->redirect()->toRoute('member');
            //}
        }
        $queryBuilder = $this->entityManager->getRepository('Member\Entity\Member')->createQuerybuilder('m');
        $queryBuilder->orderBy('m.mitgl_nr','DESC');
        $queryBuilder->setFirstResult(0)->setMaxResults(1);
        $latestmember = $queryBuilder->getQuery()->getresult();
        $MitglNr=$latestmember[0]->getMitgl_nr()+1;
        return array('form' => $form, 'MitglNr' => $MitglNr);
    }
    
    /** editAction()
     * Action for editing a member, checks if inputs are val_id and returns form
     * @return array
     */
    public function editAction(){
        //extract member_id from URL
        $member_id = (int) $this->params()->fromRoute('member_id', 0);
        
        //find member in databse
        $member = $this->entityManager->find('Member\Entity\Member', $member_id);
        
        $form  = new MemberForm();
        
        //fill out form with member data
        $form->bind($member);
        if($member->getBorn()!='')
            $form->get('born')->setValue($member->getBorn()->format('Y-m-d'));
        if($member->getEntry()!='')
            $form->get('entry')->setValue($member->getEntry()->format('Y-m-d'));
        
        
        //if member_id not found in database redirect to add member
        if (!$member_id) {
            return $this->redirect()->toRoute('member', array(
                'action' => 'add'
            ));
        }
        
        //if request is post the data will be saved else an empty form is returned
        if($this->request->isPost()){
            //$form->setInputFilter($member->getInputFilter());
            $form->setData($this->getRequest()->getPost());
            //if ($form->isValid()) {
                $data=$this->getRequest()->getPost();
                $data["born"]=new \DateTime($data["born"]);
                $data["entry"]=new \DateTime($data["entry"]);
        
                $member->exchangeArray($data);
                //save member in database
                $this->entityManager->persist($member);
                $this->entityManager->flush();
            //}
            //redirect to start page
            return $this->redirect()->toRoute('member');
        }
        return array('member_id' => $member_id, 'form' => $form);
    }
    
    /** deleteAction()
     * Action for deleting a member 
     * @return ViewModel;
     */
    public function deleteAction() {        
        //extract member_id from URL
        $member_id = (int) $this->params()->fromRoute('member_id',0);
        
        //find member in database
        $member = $this->entityManager->find('Member\Entity\Member', $member_id);
        
        if($this->request->isPost()){
            //delete member in database
            $data['active']=0;
            $member->exchangearray($data);
            $this->entityManager->persist($member);
            $this->entityManager->flush();
            
            //redirect to start page
            return $this->redirect()->toRoute('member');
        }
        
        return new Viewmodel(array('member' => $member));
    }
    
    public function searchAction() {
        
        //extract member_id from URL
        $member_id = (int) $this->params()->fromRoute('member_id',0);
        
        //find member in database
        $member = $this->entityManager()->find('Member\Entity\Member', $member_id);
        
        if($this->request->isPost()){
            //delete member in database
            //$data['active']=0;
            //$member->exchangearray($data);
            //var_dump($member);
            //$this->enitityManager->persist($member);
            //$this->entityManager->flush();
            
            //redirect to start page
            return $this->redirect()->toRoute('member');
        }
        
        return new Viewmodel(array('member' => $member));
    }
}
