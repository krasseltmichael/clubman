<?php

namespace Member\Entity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\EntityRepository;
use Zend\InputFilter\Factory as InputFactory;     
use Zend\InputFilter\InputFilter;                 
use Zend\InputFilter\InputFilterAwareInterface;  
use Zend\InputFilter\InputFilterInterface;       
use Doctrine\Common\Collections\ArrayCollection;

/** @ORM\Entity;
 *  @ORM\Table(name="contract");
 *  */

class Contract extends EntityRepository{
    /**
     * @var integer
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    protected $contract_id; 
    
    /**
     * @var integer
     * @ORM\ManyToOne(targetEntity="Member", inversedBy="Contract", cascade={"persist"})
     * @ORM\JoinColumn(name="member", referencedColumnName="member_id")
     */
    protected $member_id;     

    /**
     * @var integer
     * @ORM\ManyToOne(targetEntity="ContractType", inversedBy="Contract", cascade={"persist"})
     * @ORM\JoinColumn(name="contract", referencedColumnName="contracttype_id")
     */
    protected $contracttype_id;     

    /**
     * @var integer
     * @ ORM\ManyToOne(targetEntity="Payment", inversedBy="Contract", cascade={"persist"})
     * @ ORM\JoinColumn(name="payment", referencedColumnName="payment_id")
     */
    protected $payment_id;     
    
    /**
     * @var integer
     * @ORM\Column(type="integer")
     */
    protected $visits;     

    /**
     * @var integer
     * @ORM\Column(type="integer")
     */
    protected $amount;     

    /**
     * @var integer
     * @ORM\Column(type="integer")
     */
    protected $payed;     
    
    /**
     * @var String
     * @ORM\Column(type="string")
     */
    protected $description;     

    
}