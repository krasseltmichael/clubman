<?php
//Old Member Entity (No Conversion needed)
namespace Member\Entity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\EntityRepository;
use Zend\InputFilter\Factory as InputFactory;     
use Zend\InputFilter\InputFilter;                 
use Zend\InputFilter\InputFilterAwareInterface;  
use Zend\InputFilter\InputFilterInterface;       
use Doctrine\Common\Collections\ArrayCollection;

/** @ORM\Entity;
 *  @ORM\Table(name="member");
 *  @ORM\Entity(repositoryClass="Member\Entity\MemberRepository")
 *  */

class Member extends EntityRepository implements InputFilterAwareInterface{
    /**
     * @var integer
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    protected $member_id; 
    
    /** 
     * @var integer
     * @ORM\Column(type="integer") 
     */
    protected $mitgl_nr;   
    
    /**
     * @var string
     * @ORM\Column(type="string", nullable=true) 
     */
    protected $name;  
    
    /**
     * @var string
     * @ORM\Column(type="string", nullable=true) 
     */
    protected $forename;
    
    /**
     * @var string
     * @ORM\Column(type="string", nullable=true) 
     */
    protected $gender;
    
    /**
     * @var datetime
     * @ORM\Column(type="datetime", nullable=true) 
     */
    protected $born;
    
    /**
     * @var string
     * @ORM\Column(type="string", nullable=false) 
     */
    protected $street;
    
    /**
     * @var string
     * @ORM\Column(type="string", nullable=true) 
     */
    protected $zipCode;
    
    /** 
     * @var string
     * @ORM\Column(type="string", nullable=false) 
     */
    protected $city;
    
    /** 
     * @var string
     * @ORM\Column(type="string", nullable=false) 
     */
    protected $telephone;
    
    /** 
     * @var string
     * @ORM\Column(type="string", nullable=false) 
     */
    protected $mobilNr;
    
    /** 
     * @var string
     * @ORM\Column(type="string", nullable=false) 
     */
    protected $faxNr;
    
    /**
     * @var string
     * @ORM\Column(type="string", nullable=false) 
     */
    protected $email;
    
    /**
     * @var string
     * @ORM\Column(type="string", nullable=false) 
     */
    protected $company;
    
    /**
     * @var string
     * @ORM\Column(type="string", nullable=false) 
     */
    protected $profession;
    
    /**
     * @var string
     * @ORM\Column(type="string", nullable=false) 
     */
    protected $contract; 
    
    /**
     * @var string
     * @ORM\Column(type="string", nullable=false) 
     */
    protected $runtime;
    
    /**
     * @var datetime
     * @ORM\Column(type="datetime", nullable=false) 
     */
    protected $entry;
    
    /**
     * @var string
     * @ORM\Column(type="string", nullable=false) 
     */
    protected $active;
    
    /**
     * @var string
     * @ORM\Column(type="string", nullable=false) 
     */
    protected $sauna_week;
    
    /**
     * @var string
     * @ORM\Column(type="string", nullable=true) 
     */
    protected $solarabo;
    
    /**
     * @var string
     * @ORM\Column(type="string", nullable=false) 
     */
    protected $start;
    
    /**
     * @var string
     * @ORM\Column(type="string", nullable=false) 
     */
    protected $kontoInh;
    
    /**
     * @var string
     * @ORM\Column(type="string", nullable=false) 
     */
    protected $kontoNr;
    
    /**
     * @var string
     * @ORM\Column(type="string", nullable=false) 
     */
    protected $iban;   
    
     /**
     * @var string
     * @ORM\Column(type="string", nullable=false) 
     */
    protected $zahlweise;
    
     /**
     * @var string
     * @ORM\Column(type="string", nullable=false) 
     */
    protected $remark;
    
     /**
     * @var string
     * @ORM\Column(type="string", nullable=false) 
     */
    protected $origin;
    
    /**
     * @var Collection
     * @ORM\OneToMany(targetEntity="Visit", mappedBy="member_id", cascade={"all"})
     */
    protected $visits; 
       
    /** 
     * @var string
     * @ORM\Column(type="string", nullable=false) 
     */
    protected $rfid;

    protected $inputFilter;
    
    //getters and setters
    public function getMember_id() {
        return $this->member_id;
    }
    
    public function getMitgl_nr() {
        return $this->mitgl_nr;
    }

    public function getName() {
        return $this->name;
    }

    public function getForename() {
        return $this->forename;
    }
    
    public function printFullName(){
        return $this->forename." ".$this->name;
    }

    public function getGender() {
        return $this->gender;
    }
    
    public function getBorn() {
        return $this->born;
    }

    public function getStreet() {
        return $this->street;
    }

    public function getZipCode() {
        return $this->zipCode;
    }

    public function getCity() {
        return $this->city;
    }

    public function getTelephone() {
        return $this->telephone;
    }

    public function getMobilNr() {
        return $this->mobilNr;
    }

    public function getFaxNr() {
        return $this->faxNr;
    }

    public function getEmail() {
        return $this->email;
    }
        
    public function getContract() {
        return $this->contract;
    }
    public function printContract(){
        switch ($this->contract){
            case 1: $contract= ' ';break;
            case 2: $contract= 'Sparabo';break;
            case 3: $contract= 'Tageskarte';break;
            case 4: $contract= 'Zehnerkarte';break;
            case 5: $contract= 'Gold';break;
            case 6: $contract= 'Platin';break;
            case 7: $contract= 'Möbius';break;
            case 8: $contract= 'Richter';break;
            case 9: $contract= 'P+S';break;
            case 10: $contract= 'Reha';break;
            case 11: $contract= 'Partner';break;
            case 12: $contract= 'Schüler';break;
            case 13: $contract= 'Go/Akt.';break;
            case 14: $contract= 'Pla/Akt.';break;
        }
        return $contract;
    }
    
    public function getRuntime() {
        return $this->runtime;
    }
    public function printRuntime(){
        switch ($this->runtime){
            case 1: $runtime= ' ';break;
            case 2: $runtime= '6';break;
            case 3: $runtime= '12';break;
            case 4: $runtime= '12e';break;
            case 5: $runtime= '18';break;
            case 6: $runtime= '24';break;
            case 7: $runtime= '6+1';break;
            case 8: $runtime= '6+2';break;
            case 9: $runtime= '12+1';break;
            case 10: $runtime= '12+2';break;
            case 11: $runtime= '24+1';break;
            case 12: $runtime= '24+2';break;
            case 13: $runtime= 'VÜ';break;
        }
        return $runtime;
    
    }

    public function getEntry() {
        return $this->entry;
    }

    public function getProfession() {
        return $this->profession;
    }
    
    public function getCompany() {
        return $this->company;
    }
    public function getActive() {
        return $this->active;
    }

    public function getSauna_week() {
        return $this->sauna_week;
    }

    public function getSolarabo() {
        return $this->solarabo;
    }
    
    public function getStart() {
        return $this->start;
    }

    public function getKontoInh() {
        return $this->kontoInh;
    }

    public function getKontoNr() {
        return $this->kontoNr;
    }

    public function getIban() {
        return $this->iban;
    }

    public function getZahlweise() {
        return $this->zahlweise;
    }
    
    public function getRemark() {
        return $this->remark;
    }
    
    public function getOrigin() {
        return $this->origin;
    }
       
    function getVisits() {
        if(count($this->visits)>0)
            return $this->visits;
        else
            return false;
    }
    
    function getLastVisitDay($amount = 1){
        $visitdays = array();
        //If Multible Visits Existing
        if(count($this->visits)>0){
            //loop through them to String Datearray
            foreach ($this->getVisits() as $visit){
                $visitdays[]=$visit->getCheckin()->format("d.m.Y");
            }
            //extingluish double dates
            $visitdays = array_unique($visitdays);
            //sort by old dates first
            usort($visitdays, function($a1, $a2) {
                $v1 = strtotime($a1);
                $v2 = strtotime($a2);
                return $v1 - $v2;
            });
        }
        //return 1 Date as standart
        if($amount===1){
            return $visitdays[count($visitdays)-1];
        }else{
            //Select maximal possible output wanted without giving Errors
            if(count($visitdays)<1){
                return false;
            }else if(count($visitdays)<=$amount){
                for($x = 1; $x<=count($visitdays); $x++){
                    $return[]=$visitdays[count($visitdays)-$x];
                }
                return $return;
            }else if(count($visitdays)>$amount){
                for($x = 1; $x<=$amount; $x++){
                    $return[]=$visitdays[count($visitdays)-$x];
                }
                return $return;
            }
        }
    }
    
    function getLastVisit2(){
        $visits = $this->getLastVisitDay(2);
        return !empty($visits[1])?$visits[1]:"";
    }
    
    function getLastVisit3(){
        $visits = $this->getLastVisitDay(3);
        return !empty($visits[2])?$visits[2]:"";
    }
    
    function getLastVisit4(){
        $visits = $this->getLastVisitDay(4);
        return !empty($visits[3])?$visits[3]:"";

    }
    
    public function setMember_id($member_id) {
        $this->member_id = $member_id;
    }

    public function setMitgl_nr($mitgl_nr) {
        $this->mitgl_nr = $mitgl_nr;
    }
    
    public function setName($name) {
        $this->name = $name;
    }

    public function setForename($forename) {
        $this->forename = $forename;
    }
    
    public function setGender($gender) {
        $this->gender = $gender;
    }
    
    public function setBorn(\DateTime $born) {
        $this->born = $born;
    }

    public function setStreet($street) {
        $this->street = $street;
    }

    public function setZipCode($zipCode) {
        $this->zipCode = $zipCode;
    }

    public function setCity($city) {
        $this->city = $city;
    }

    public function setTelephone($telephone) {
        $this->telephone = $telephone;
    }

    public function setMobilNr($mobilNr) {
        $this->mobilNr = $mobilNr;
    }

    public function setFaxNr($faxNr) {
        $this->faxNr = $faxNr;
    }

    public function setEmail($email) {
        $this->email = $email;
    }
    
    public function setContract($contract) {
        $this->contract = $contract;
    }
    
    public function setRuntime($runtime) {
        $this->runtime = $runtime;
    }
    
    public function setEntry(\DateTime $entry) {
        $this->entry = $entry;
    }

    public function setProfession($profession) {
        $this->profession = $profession;
    }
    
    public function setCompany($company) {
        $this->company = $company;
    }
    public function setActive($active) {
        $this->active = $active;
    }

    public function setSauna_week($sauna_week) {
        $this->sauna_week = $sauna_week;
    }

    public function setSolarabo($solarabo) {
        $this->solarabo = $solarabo;
    }

    public function setStart($start) {
        $this->start = $start;
    }

    public function setKontoInh($kontoInh) {
        $this->kontoInh = $kontoInh;
    }

    public function setKontoNr($kontoNr) {
        $this->kontoNr = $kontoNr;
    }

    public function setIban($iban) {
        $this->iban = $iban;
    }

    public function setZahlweise($zahlweise) {
        $this->zahlweise = $zahlweise;
    }
    
    public function setRemark($remark) {
        $this->remark = $remark;
        
    }
    
    public function setOrigin($origin) {
        $this->origin = $origin;
    }
       
    function setVisits($visits) {
        $this->visits = $visits;
    }
    
    function getRfid() {
        return $this->rfid;
    }

    function setRfid($rfid) {
        $this->rfid = $rfid;
    }

    /** void __construct(array $options) constructor, if array is given, it initializes the data */
    public function __construct(array $options = null) {
        $this->visits = new ArrayCollection();
        if (is_array($options)) {
            $this->setOptions($options);
        }
    }

    /** getArrayCopy()
     * This method is needed for Hydrator, when binding forms 
     * @return void 
     */
    public function getArrayCopy(){
        $arraycopy=get_object_vars($this);
        return $arraycopy;
    }
    /** getArrayCopy()
     * This method is needed for Hydrator, when binding forms 
     * @return void 
     */
    public function getPrintArrayCopy(){
        $arraycopy=get_object_vars($this);
        $arraycopy["last_visit"]=$this->getLastVisit2();
        $arraycopy["last_visit2"]=$this->getLastVisit2();
        $arraycopy["last_visit3"]=$this->getLastVisit3();
        $arraycopy["last_visit4"]=$this->getLastVisit4();
        $arraycopy["contract"]=$this->printContract();
        $arraycopy["runtime"]=$this->printRuntime();
        return $arraycopy;
    }
    
    /** 
     * ___set(string $name, type $value)
     * Checks if set-method exists and returns it
     * @retrun vo_id
     */
    public function __set($name, $value) {
        $method = 'set' . $name;
        if (!method_exists($this, $method)) {
            throw new \Exception('Invalid Method: ' . $method);
        }
        return $this->$method($value);
    }
    
    /** __get(string $name)
     * Checks if get-method exists and returns it 
     * @return vo_id
     */
    public function __get($name) {
        $method = 'get' . $name;
        if (!method_exists($this, $method)) {
            throw new \Exception('Invalid Method: ' . $method);
        }
        return $this->$method;
    }
    /** 
     * exchangeArray(array $data)
     * This method is needed for data exchange and hydrator (when binding forms) 
     * @return vo_id 
     */
    public function exchangeArray($data)
    {
        foreach ($data as $key=>$value){
            $method = 'set' . ucfirst($key);
            if(method_exists($this, $method)){
                $this->$method($value);
            }
        }
    }

    /**
     * setInputFilter(InputFilterInterface $inputFiler)
     * Right now no use, but needs to be defined for InputfilterAwareInterface 
     * @return Exception
     */
    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Filter ".$inputFilter." Not used");
    }

    /** getInputFilter()
     *  Defines filter for form inputs (e. g.  length), needed for form val_idation 
     *  @return InputFilter
     */
    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory     = new InputFactory();
            
            //Filter member_id
            $inputFilter->add($factory->createInput(array(
                'name'     => 'member_id',
                'required' => true,
                'filters'  => array(
                    array('name' => 'Int'),
                ),
            )));
            
            //Filter name
            $inputFilter->add($factory->createInput(array(
                'name'     => 'name',
                'required' => true,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name'    => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min'      => 1,
                            'max'      => 100,
                        ),
                    ),
                ),
            )));
            
            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    } 
}
