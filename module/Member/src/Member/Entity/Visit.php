<?php

namespace Member\Entity;
use Doctrine\ORM\Mapping as ORM;
use Zend\InputFilter\Factory as InputFactory;     
use Zend\InputFilter\InputFilter;                 
use Zend\InputFilter\InputFilterAwareInterface;  
use Zend\InputFilter\InputFilterInterface;  

/**
 *  @ORM\Entity;
 *  @ORM\Table(name="visit");
 */

class Visit implements InputFilterAwareInterface{
    /**
     * @var integer
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    protected  $visit_id;
    
    /**
     * @var integer
     * @ORM\ManyToOne(targetEntity="Member", inversedBy="Visit", cascade={"persist"})
     * @ORM\JoinColumn(name="member_id", referencedColumnName="member_id")
     */
    protected $member_id;  
    
    /**
     * @var integer
     * @ORM\ManyToOne(targetEntity="LockerKey", cascade={"persist"})
     * @ORM\JoinColumn(name="key_id", referencedColumnName="key_id")
     */
    protected $key_id;  
    
    /**
     * @var datetime
     * @ORM\Column(type="datetime", nullable=false)
     */
    protected $checkin;
    
    /**
     * @var datetime
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $checkout;
    
    //protected $inputFilter;
    
    //Getter und Setter
    public function getVisit_id() {
        return $this->visit_id;
    }

    public function getMember_id() {
        return $this->member_id;
    }

    public function getKey_id() {
        return $this->key_id;
    }

    public function getCheckin() {
        return $this->checkin;
    }

    public function getCheckout() {
        return $this->checkout;
    }

    public function setVisit_id($visit_id) {
        $this->visit_id = $visit_id;
    }

    public function setMember_id($member_id) {
        $this->member_id = $member_id;
    }

    public function setKey_id($key_id) {
        $this->key_id = $key_id;
    }

    public function setCheckin($checkin) {
        $this->checkin = $checkin;
    }

    public function setCheckout($checkout) {
        $this->checkout = $checkout;
    }

         //Functions
     /** void __construct(array $options) constructor, if array is given, it initializes the data */
    public function __construct(array $options = null) {
        if (is_array($options)) {
            $this->setOptions($options);
        }
    }

    /** getArrayCopy()
     * This method is needed for Hydrator, when binding forms 
     * @return void 
     */
    public function getArrayCopy()
    {
        return get_object_vars($this);
    }
      /** 
     * ___set(string $name, type $value)
     * Checks if set-method exists and returns it
     * @retrun vo_id
     */
    public function __set($name, $value) {
        $method = 'set' . $name;
        if (!method_exists($this, $method)) {
            throw new \Exception('Invalid Method: ' . $method);
        }
        return $this->$method($value);
    }
    
    /** __get(string $name)
     * Checks if get-method exists and returns it 
     * @return vo_id
     */
    public function __get($name) {
        $method = 'get' . $name;
        if (!method_exists($this, $method)) {
            throw new \Exception('Invalid Method: ' . $method);
        }
        return $this->$method;
    }
    /** 
     * exchangeArray(array $data)
     * This method is needed for data exchange and hydrator (when binding forms) 
     * @return vo_id 
     */
    public function exchangeArray($data)
    {
        foreach ($data as $key=>$value){
            $method = 'set' . ucfirst($key);
            if(method_exists($this, $method)){
                $this->$method($value);
            }
        }
    }

    /**
     * setInputFilter(InputFilterInterface $inputFiler)
     * Right now no use, but needs to be defined for InputfilterAwareInterface 
     * @return Exception
     */
    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Filter ".$inputFilter." Not used");
    }

    /** getInputFilter()
     *  Defines filter for form inputs (e. g.  length), needed for form val_idation 
     *  @return InputFilter
     */
    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory     = new InputFactory();
            
            //Filter mitgl_nr
            //$this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }
}
