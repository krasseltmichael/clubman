<?php

namespace Member\Entity;
use Doctrine\ORM\Mapping as ORM;
use Zend\InputFilter\Factory as InputFactory;     
use Zend\InputFilter\InputFilter;                 
use Zend\InputFilter\InputFilterAwareInterface;  
use Zend\InputFilter\InputFilterInterface;  

/**
 *  @ORM\Entity;
 *  @ORM\Table(name="contactinfo");
 */

class ContactInfo implements InputFilterAwareInterface{
    /**
     * @var integer
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    protected  $contactinfo_id;
    
    /**
     * @var integer
     * @ORM\ManyToOne(targetEntity="Member", inversedBy="Contactinfo", cascade={"persist"})
     * @ORM\JoinColumn(name="member_id", referencedColumnName="member_id")
     */
    protected $member_id;  
    
    /**
     * @var integer
     * @ORM\ManyToOne(targetEntity="ContactInfoType", cascade={"persist"})
     * @ORM\JoinColumn(name="contactinfotype", referencedColumnName="contactinfotype_id")
     */
    protected $contactinfotype;  
    
    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    protected $info;
    
    protected $inputFilter;
    
    //Getter und Setter

    function getContactinfo_id() {
        return $this->contactinfo_id;
    }

    function getMember_id() {
        return $this->member_id;
    }

    function getContactinfotype() {
        return $this->contactinfotype;
    }

    function getInfo() {
        return $this->info;
    }

    function setContactinfo_id($contactinfo_id) {
        $this->contactinfo_id = $contactinfo_id;
    }

    function setMember_id($member_id) {
        $this->member_id = $member_id;
    }

    function setContactinfotype($contactinfotype) {
        $this->contactinfotype = $contactinfotype;
    }

    function setInfo($info) {
        $this->info = $info;
    }

    //Functions
    /** void __construct(array $options) constructor, if array is given, it initializes the data */
    public function __construct(array $options = null) {
        if (is_array($options)) {
            $this->setOptions($options);
        }
    }

    /** getArrayCopy()
     * This method is needed for Hydrator, when binding forms 
     * @return void 
     */
    public function getArrayCopy(){
        return get_object_vars($this);
    }
    /** 
     * ___set(string $name, type $value)
     * Checks if set-method exists and returns it
     * @retrun vo_id
     */
    public function __set($name, $value) {
        $method = 'set' . $name;
        if (!method_exists($this, $method)) {
            throw new \Exception('Invalid Method: ' . $method);
        }
        return $this->$method($value);
    }
    
    /** __get(string $name)
     * Checks if get-method exists and returns it 
     * @return vo_id
     */
    public function __get($name) {
        $method = 'get' . $name;
        if (!method_exists($this, $method)) {
            throw new \Exception('Invalid Method: ' . $method);
        }
        return $this->$method;
    }
    /** 
     * exchangeArray(array $data)
     * This method is needed for data exchange and hydrator (when binding forms) 
     * @return vo_id 
     */
    public function exchangeArray($data)
    {
        foreach ($data as $key=>$value){
            $method = 'set' . ucfirst($key);
            if(method_exists($this, $method)){
                $this->$method($value);
            }
        }
    }

    /**
     * setInputFilter(InputFilterInterface $inputFiler)
     * Right now no use, but needs to be defined for InputfilterAwareInterface 
     * @return Exception
     */
    public function setInputFilter(InputFilterInterface $inputFilter){
        throw new \Exception("Filter ".$inputFilter." Not used");
    }

    /** getInputFilter()
     *  Defines filter for form inputs (e. g.  length), needed for form val_idation 
     *  @return InputFilter
     */
    public function getInputFilter(){
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory     = new InputFactory();
            
            //Filter plz
            $inputFilter->add($factory->createInput(array(
                'name'     => 'info',
                'required' => true,
                'validators' => array(
                    array(
                        'name' => 'Regex',
                        'options' => array(
                                            'pattern' => '('
                            . '([\+][0-9]{1,3}[ \.\-])?([\(]{1}[0-9]{1,6}[\)])?([0-9 \.\-\/]{3,20})((x|ext|extension)[ ]?[0-9]{1,4})?'
                            . '|'
                            . '(((((((00|\+)49[ \-/]?)|0)[1-9][0-9]{1,4})[ \-/]?)|((((00|\+)49\()|\(0)[1-9][0-9]{1,4}\)[ \-/]?))[0-9]{1,7}([ \-/]?[0-9]{1,5})?)'
                            . '|'
                            . '[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*)',
                                            'messages' => array(
                                                                \Zend\Validator\Regex::INVALID => 'Ungültige Kontaktinfo',
                                                                ),
                                            ),

                    )
                )
            )));
        }

        return $this->inputFilter;
    }
}

