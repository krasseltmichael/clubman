<?php

namespace Member\Entity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\EntityRepository;
use Zend\InputFilter\Factory as InputFactory;     
use Zend\InputFilter\InputFilter;                 
use Zend\InputFilter\InputFilterAwareInterface;  
use Zend\InputFilter\InputFilterInterface;       
use Doctrine\Common\Collections\ArrayCollection;

/** @ORM\Entity;
 *  #@#ORM\Entity(repositoryClass="Member\Entity\MemberRepository")
 *  @ORM\Table(name="memberadress");
 *  */

class MemberAddress implements InputFilterAwareInterface{
    /**
     * @var integer
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    protected $memberaddress_id; 
    
    /**
     * @var string
     * @ORM\Column(type="string", nullable=false) 
     */
    protected $street;
    
    /**
     * @var string
     * @ORM\Column(type="string", nullable=true) 
     */
    protected $zipCode;
    
    /** 
     * @var string
     * @ORM\Column(type="string", nullable=false) 
     */
    protected $city;
    
    protected $inputFilter;
    
    //getters and setters
    
    function getprintAddress() {
        return $this->getStreet()."\n".$this->getZipCode()." ".$this->getCity();
    }
    
    function getMemberaddress_id() {
        return $this->memberaddress_id;
    }

    function getStreet() {
        return $this->street;
    }

    function getZipCode() {
        return $this->zipCode;
    }

    function getCity() {
        return $this->city;
    }

    function setMemberaddress_id($memberaddress_id) {
        $this->memberaddress_id = $memberaddress_id;
    }

    function setStreet($street) {
        $this->street = $street;
    }

    function setZipCode($zipCode) {
        $this->zipCode = $zipCode;
    }

    function setCity($city) {
        $this->city = $city;
    }

    
    /** void __construct(array $options) constructor, if array is given, it initializes the data */
    public function __construct(array $options = null) {
        if (is_array($options)) {
            $this->setOptions($options);
        }
    }

    /** getArrayCopy()
     * This method is needed for Hydrator, when binding forms 
     * @return void 
     */
    public function getArrayCopy()
    {
        return get_object_vars($this);
    }
      /** 
     * ___set(string $name, type $value)
     * Checks if set-method exists and returns it
     * @retrun vo_id
     */
    public function __set($name, $value){
        $method = 'set' . $name;
        if (!method_exists($this, $method)) {
            throw new \Exception('Invalid Method: ' . $method);
        }
        return $this->$method($value);
    }
    
    /** __get(string $name)
     * Checks if get-method exists and returns it 
     * @return vo_id
     */
    public function __get($name) {
        $method = 'get' . $name;
        if (!method_exists($this, $method)) {
            throw new \Exception('Invalid Method: ' . $method);
        }
        return $this->$method;
    }
    /** 
     * exchangeArray(array $data)
     * This method is needed for data exchange and hydrator (when binding forms) 
     * @return vo_id 
     */
    public function exchangeArray($data){
        foreach ($data as $key=>$value){
            $method = 'set' . ucfirst($key);
            if(method_exists($this, $method)){
                $this->$method($value);
            }
        }
    }

    /**
     * setInputFilter(InputFilterInterface $inputFiler)
     * Right now no use, but needs to be defined for InputfilterAwareInterface 
     * @return Exception
     */
    public function setInputFilter(InputFilterInterface $inputFilter){
        throw new \Exception("Filter ".$inputFilter." Not used");
    }

    /** getInputFilter()
     *  Defines filter for form inputs (e. g.  length), needed for form val_idation 
     *  @return InputFilter
     */
    public function getInputFilter(){
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory     = new InputFactory();
            
            //Filter memberaddress_id
            $inputFilter->add($factory->createInput(array(
                'name'     => 'memberaddress_id',
                'required' => true,
                'filters'  => array(
                    array('name' => 'Int'),
                ),
            )));
            //Filter plz
            $inputFilter->add($factory->createInput(array(
                'name'     => 'zipcode',
                'required' => true,
                'filters'  => array(
                    array('name' => 'Int'),
                ),
                'validators' => array(
                    array(
                        'name' => 'Regex',
                        'options' => array(
                                            'pattern' => '^((0(1\d\d[1-9])|([2-9]\d\d\d))|(?(?=^(^9{5}))|[1-9]\d{4}))$ ',
                                            'messages' => array(
                                                                \Zend\Validator\Regex::INVALID => 'Ungültige Postleitzahl',
                                                                ),
                                            ),

                    )
                )
            )));
            
            //Filter Street
            $inputFilter->add($factory->createInput(array(
                'name'     => 'street',
                'required' => true,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name' => 'NotEmpty',
                        'options' => array(
                            'messages' => array(
                                \Zend\Validator\NotEmpty::IS_EMPTY => 'Straße darf nicht leer sein.',
                            ),
                        ),
                    ),
                    array (
                        'name' => 'StringLength', 
                        'options' => array( 
                            'encoding' => 'UTF-8', 
                            'min' => '4', 
                            'max' => '25', 
                            'messages' => array(
                                \Zend\Validator\StringLength::TOO_SHORT => 'Straße darf nicht kürzer als 4 Zeichen sein',
                                \Zend\Validator\StringLength::TOO_LONG => 'Straßenname darf nicht mehr als 25 Zeichen haben.',
                            ),
                        ), 
                    ), 
                    array(
                        'name' => 'Regex',
                        'options' => array(
                                            'pattern' => '^(([a-zA-ZäöüÄÖÜß]\D*)\s+\d+?\s*.*)$',
                                            'messages' => array(
                                                                \Zend\Validator\Regex::INVALID => 'Ungültige Zeichen eingegeben',
                                                                ),
                                            ),

                    )
                )
            )));
            //Filter City
            $inputFilter->add($factory->createInput(array(
                'name'     => 'city',
                'required' => true,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name' => 'NotEmpty',
                        'options' => array(
                            'messages' => array(
                                \Zend\Validator\NotEmpty::IS_EMPTY => 'Stadt darf nicht leer sein.',
                            ),
                        ),
                    ),
                    array (
                        'name' => 'StringLength', 
                        'options' => array( 
                            'encoding' => 'UTF-8', 
                            'min' => '2', 
                            'max' => '25', 
                            'messages' => array(
                                \Zend\Validator\StringLength::TOO_SHORT => 'Stadt darf nicht kürzer als 2 Zeichen sein',
                                \Zend\Validator\StringLength::TOO_LONG => 'Stadt darf nicht mehr als 25 Zeichen haben.',
                            ),
                        ), 
                    ), 
                    array(
                        'name' => 'Regex',
                        'options' => array(
                                            'pattern' => '/^[A-zäöüßÄÖÜ0-9 -\.]{3,25}$/i',
                                            'messages' => array(
                                                                \Zend\Validator\Regex::INVALID => 'Ungültige Zeichen eingegeben',
                                                                ),
                                            ),

                    )
                )
            )));

            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    } 
}
