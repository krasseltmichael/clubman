<?php

namespace Member\Entity;
use Doctrine\ORM\Mapping as ORM;
use Zend\InputFilter\Factory as InputFactory;     
use Zend\InputFilter\InputFilter;                 
use Zend\InputFilter\InputFilterAwareInterface;  
use Zend\InputFilter\InputFilterInterface;  

/**
 *  @ORM\Entity;
 *  @ORM\Table(name="contactinfotype");
 */

class ContactInfoType implements InputFilterAwareInterface {
    /**
     * @var integer
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    protected  $contactinfotype_id;
    
    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    protected $name;
    
    //Getter und Setter

    function getContactinfotype_id() {
        return $this->contactinfotype_id;
    }

    function getName() {
        return $this->name;
    }

    function setContactinfotype_id($contactinfotype_id) {
        $this->contactinfotype_id = $contactinfotype_id;
    }

    function setName($name) {
        $this->name = $name;
    }

    //Functions
    
    /** void __construct(array $options) constructor, if array is given, it initializes the data */
    public function __construct(array $options = null) {
        if (is_array($options)) {
            $this->setOptions($options);
        }
    }

    /** getArrayCopy()
     * This method is needed for Hydrator, when binding forms 
     * @return void 
     */
    public function getArrayCopy(){
        return get_object_vars($this);
    }
    /** 
     * ___set(string $name, type $value)
     * Checks if set-method exists and returns it
     * @retrun vo_id
     */
    public function __set($name, $value) {
        $method = 'set' . $name;
        if (!method_exists($this, $method)) {
            throw new \Exception('Invalid Method: ' . $method);
        }
        return $this->$method($value);
    }
    
    /** __get(string $name)
     * Checks if get-method exists and returns it 
     * @return vo_id
     */
    public function __get($name) {
        $method = 'get' . $name;
        if (!method_exists($this, $method)) {
            throw new \Exception('Invalid Method: ' . $method);
        }
        return $this->$method;
    }
    /** 
     * exchangeArray(array $data)
     * This method is needed for data exchange and hydrator (when binding forms) 
     * @return vo_id 
     */
    public function exchangeArray($data)
    {
        foreach ($data as $key=>$value){
            $method = 'set' . ucfirst($key);
            if(method_exists($this, $method)){
                $this->$method($value);
            }
        }
    }
    public function getInputFilter() {
        
    }

    public function setInputFilter(InputFilterInterface $inputFilter) {
        
    }

}

