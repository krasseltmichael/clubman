<?php

namespace Member\Entity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\EntityRepository;
use Zend\InputFilter\Factory as InputFactory;     
use Zend\InputFilter\InputFilter;                 
use Zend\InputFilter\InputFilterAwareInterface;  
use Zend\InputFilter\InputFilterInterface;       
use Doctrine\Common\Collections\ArrayCollection;

/** @ORM\Entity;
 *  @ORM\Table(name="contracttype");
 *  */

class ContractType extends EntityRepository{
    /**
     * @var integer
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    protected $contracttype_id; 
    
    /**
     * @var String
     * @ORM\Column(type="string")
     */
    protected $name;     
    
    /**
     * @var String
     * @ORM\Column(type="string")
     */
    protected $description;     

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    protected $turnus;     

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    protected $price;     

    /**
     * @var integer
     * @ORM\Column(type="integer")
     */
    protected $visitlimitperweek;     

    /**
     * @var integer
     * @ORM\Column(type="integer")
     */
    protected $solar;     

    /**
     * @var integer
     * @ORM\Column(type="integer")
     */
    protected $vibro;     
    
    /**
     * @var integer
     * @ORM\Column(type="integer")
     */
    protected $aqua;     

    /**
     * @var integer
     * @ORM\Column(type="integer")
     */
    protected $course;     

}