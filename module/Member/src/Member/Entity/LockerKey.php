<?php

namespace Member\Entity;
use Doctrine\ORM\Mapping as ORM;
use Zend\InputFilter\Factory as InputFactory;     
use Zend\InputFilter\InputFilter;   
use Zend\InputFilter\InputFilterInterface;  

/**
 *  @ORM\Entity;
 *  @ORM\Table(name="lockerkey");
 */

class LockerKey{
    /**
     * @var integer
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    protected  $key_id;
    
    /**
     * @var integer
     * @ORM\Column(type="integer")
     */
    protected $key_nr;
    
    /**
     * @var integer
     * @ORM\Column(type="integer")
     */
    protected $key_type;
    
    /**
     * @var integer
     * @ORM\Column(type="integer")
     */
    protected $key_active;
    
    /** 
     * @var string
     * @ORM\Column(type="string", nullable=false) 
     */
    protected $rfid;
    
    //Getter und Setter
    public function getKey_id() {
        return $this->key_id;
    }

    public function getKey_nr() {
        return $this->key_nr;
    }

    public function getKey_type() {
        return $this->key_type;
    }

    public function setKey_id($key_id) {
        $this->key_id = $key_id;
    }

    public function setKey_nr($key_nr) {
        $this->key_nr = $key_nr;
    }
    
    public function setKey_type($key_type) {
        $this->key_type = $key_type;
    }
    
    function getKey_active() {
        return $this->key_active;
    }

    function setKey_active($key_active) {
        $this->key_active = $key_active;
    }

    function getrfid() {
        return $this->rfid;
    }

    function setrfid($rfid) {
        $this->rfid = $rfid;
    }

       //Functions
     /** void __construct(array $options) constructor, if array is given, it initializes the data */
    public function __construct(array $options = null) {
        if (is_array($options)) {
            $this->setOptions($options);
        }
    }

    /** getArrayCopy()
     * This method is needed for Hydrator, when binding forms 
     * @return void 
     */
    public function getArrayCopy()
    {
        return get_object_vars($this);
    }
      /** 
     * ___set(string $name, type $value)
     * Checks if set-method exists and returns it
     * @retrun vo_id
     */
    public function __set($name, $value) {
        $method = 'set' . $name;
        if (!method_exists($this, $method)) {
            throw new \Exception('Invalid Method: ' . $method);
        }
        return $this->$method($value);
    }
    
    /** __get(string $name)
     * Checks if get-method exists and returns it 
     * @return vo_id
     */
    public function __get($name) {
        $method = 'get' . $name;
        if (!method_exists($this, $method)) {
            throw new \Exception('Invalid Method: ' . $method);
        }
        return $this->$method;
    }
    /** 
     * exchangeArray(array $data)
     * This method is needed for data exchange and hydrator (when binding forms) 
     * @return vo_id 
     */
    public function exchangeArray($data)
    {
        foreach ($data as $key=>$value){
            $method = 'set' . ucfirst($key);
            if(method_exists($this, $method)){
                $this->$method($value);
            }
        }
    }
}