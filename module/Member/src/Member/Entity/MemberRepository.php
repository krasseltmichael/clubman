<?php

namespace Member\Entity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\EntityRepository;
use Zend\InputFilter\Factory as InputFactory;     
use Zend\InputFilter\InputFilter;                 
use Zend\InputFilter\InputFilterAwareInterface;  
use Zend\InputFilter\InputFilterInterface;       
use Doctrine\Common\Collections\ArrayCollection;

class MemberRepository extends EntityRepository{
    public function findByBorn($day){
        $querybuilder = $this->_em->createQueryBuilder('m')
                ->select("m")
                ->from("Member\Entity\Member", "m")
                ->Where("m.born LIKE '%-".date("m",strtotime(" - 1 day",$day ))."-".date("d",strtotime(" - 1 day",$day ))." %'" . 'AND m.active=1');
        $yesterday=$querybuilder->getQuery()->getResult();
        
        $querybuilder = $this->_em->createQueryBuilder('m')
                ->select("m")
                ->from("Member\Entity\Member", "m")
                ->Where("m.born LIKE '%-".date("m",$day)."-".date("d",$day)." %'" . 'AND m.active=1');
        $today=$querybuilder->getQuery()->getResult();
        
        $querybuilder = $this->_em->createQueryBuilder('m')
                ->select("m")
                ->from("Member\Entity\Member", "m")
                ->Where("m.born LIKE '%-".date("m",strtotime(" + 1 day",$day ))."-".date("d",strtotime(" + 1 day",$day ))." %'" . 'AND m.active=1');
        $tomorrow=$querybuilder->getQuery()->getResult();
        
        return array("yesterday"=>$yesterday,"today"=>$today,"tomorrow"=>$tomorrow);
        
    }
    
    public function findByBornMonth($month){
        $querybuilder = $this->_em->createQueryBuilder('m')
                ->select("m")
                ->from("Member\Entity\Member", "m")
                ->Where("m.born LIKE '%-".str_pad($month, 2, "0",STR_PAD_LEFT)."-% %'" . 'AND m.active=1');
        //var_dump($querybuilder->getQuery()->getDQL());
        $birthdays=$querybuilder->getQuery()->getResult();
        return $birthdays;
        
    }
    
    /**
     * Find corresponding Visitsdays for Member
     */
    public function getMemberVisitDays($member=null){
        //Query for all Visits of Memberid
        $querybuilder = $this->_em->createQuerybuilder('v')
                ->select("v")
                ->from("Member\Entity\Visit", "v")
                ->where("v.member_id = '$member'")
                ->orderBy("v.checkin", "DESC");
        //retrieving matching Visits
        $entrys = $querybuilder->getQuery()->getresult();
        
        //Convert Objectlist to Dateonly list
        $visitdays = array();
        foreach ($entrys as $visit){
            $visitdays[]=$visit->getCheckin()->format("d.m.Y");
        }
        return array_unique($visitdays);
    }
    
}