<?php 
/*Die datei brauchen wir für jeden controller, gibt noch ne bessere lösung aber die funktioniert bei mir noch nicht*/
namespace Member\Service\Factory;

use Zend\ServiceManager\Factory\FactoryInterface;
use Interop\Container\ContainerInterface;
use Doctrine\ORM\EntityManager;
use Member\Controller\MemberController;

/**
 * This is the factory for Membercontroller. Its purpose is to instantiate the
 * service.
 */
class MemberFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = NULL)
    {
        $entityManager = $container->get('doctrine.entitymanager.orm_default');
        return new MemberController($entityManager);
    }
}