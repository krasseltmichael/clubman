<?php

namespace Member\Form;

use Zend\Form\Form;

class KeyForm extends Form {
    
    /** __construct()
     * Constructor that defines elements of form 
     * @return void 
    */
    function __construct() {
        parent::__construct('key');
        $this->setAttribute('method', 'post');
        $this->add(array(
            'name' => 'key_id',
            'attributes' => array(
                'type'  => 'hidden',
            ),
        ));
        $this->add(array(
            'name' => 'key_nr',
            'attributes' => array(
                'type'  => 'text',
            ),
            'options' => array(
                'label' => 'Schlüsselnummer',
            ),
        ));
        
         $this->add(array(
            'type' => 'Zend\Form\Element\Radio',
            'name' => 'key_type',
            'options' => array(
                'label' => 'Geschlecht wählen',
                'value_options' => array(
                    '0' => ' W',
                    '1' => ' M',
                ),
            ),
            'attributes' => array(
                'value' => '0' //set checked to '0'
            )
        ));
        
        $this->add(array(
            'name' => 'rfid',
            'options' => array(
                'label' => 'RFID-Karte',
            ),
        ));
        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type'  => 'submit',
                'value' => 'Absenden',
                '_id' => 'submitbutton',
            ),
        ));
              
        $this->add(array(
            'name' => 'learn',
            'attributes' => array(
                'type'  => 'button',
                'value' => 'RFID-Lernen',
                'id' => 'learn_button'
                ),
        ));
    }
}
