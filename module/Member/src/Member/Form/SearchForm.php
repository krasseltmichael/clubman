<?php

namespace Member\Form;

use Zend\Form\Form;

/**
 * Defines a Form for person entity
 *
 * @author Nadja
 */
class SearchForm extends Form {
    
    /** __construct()
     * Constructor that defines elements of form 
     * @return void 
    */
    function __construct() {
        parent::__construct('member');       
        
        $this->add(array(
            'name' => 'mitgl_nr',
            'attributes' => array(
                'type'  => 'text',
            ),
            'options' => array(
                'label' => 'Mitgliedsnummer',
            ),
        ));
        
        $this->add(array(
            'name' => 'forename',
            'attributes' => array(
                'type'  => 'text',
            ),
            'options' => array(
                'label' => 'Vorname',
            ),
        ));
        
        $this->add(array(
            'name' => 'name',
            'attributes' => array(
                'type'  => 'text',
            ),
            'options' => array(
                'label' => 'Nachname',
            ),
        ));
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Radio',
            'name' => 'gender',
            'options' => array(
                //'label' => 'Geschlecht wählen',
                'value_options' => array(
                    '-1' => ' unbekannt',
                    '0' => ' weiblich',
                    '1' => ' männlich',
                ),
            ),
            'attributes' => array(
                'value' => '-1' //set checked to '0'
            )
        ));
        
        $this->add(array(
            'name' => 'born',
            'attributes' => array(
                'type'  => 'date',
            ),
            'options' => array(
                'label' => 'geb.',
            ),
        ));
        
        $this->add(array(
            'name' => 'street',
            'attributes' => array(
                'type'  => 'text',
            ),
            'options' => array(
                'label' => 'Straße',
            ),
        ));
        
        $this->add(array(
            'name' => 'zipCode',
            'attributes' => array(
                'type'  => 'text',
            ),
            'options' => array(
                'label' => 'Postleitzahl',
            ),
        ));
        
        $this->add(array(
            'name' => 'city',
            'attributes' => array(
                'type'  => 'text',
            ),
            'options' => array(
                'label' => 'Stadt',
            ),
        ));

        $this->add(array(
            'name' => 'telephone',
            'attributes' => array(
                'type'  => 'text',
            ),
            'options' => array(
                'label' => 'Festnetz Nummer',
            ),
        ));
        $this->add(array(
            'name' => 'mobilNr',
            'attributes' => array(
                'type'  => 'text',
            ),
            'options' => array(
                'label' => 'Mobil Nummer',
            ),
        ));
        $this->add(array(
            'name' => 'faxNr',
            'attributes' => array(
                'type'  => 'text',
            ),
            'options' => array(
                'label' => 'Fax Nummer',
            ),
        ));
        $this->add(array(
            'name' => 'email',
            'attributes' => array(
                'type'  => 'text',
            ),
            'options' => array(
                'label' => 'E-Mail',
            ),
        ));
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'contract',
            'options' => array(
                'label' => 'Vertrag',
                'value_options' => array(
                    '1' => '',
                    '2' => 'Sparabo',
                    '3' => 'Tageskarte',
                    '4' => 'Zehnerkarte',
                    '5' => 'Gold',
                    '6' => 'Platin',
                    '7' => 'Möbius',
                    '8' => 'Richter',
                    '9' => 'P+S',
                    '10' => 'Reha',
                    '11' => 'Partner',
                    '12' => 'Schüler',
                    '13' => 'Go/Akt.',
                    '14' => 'Pla/Akt.'                    
                ),
            ),
            'attributes' => array(
                'value' => '1' //set selected to '1'
            )
        ));
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'runtime',
            'options' => array(
                'label' => 'Laufzeit',
                'value_options' => array(
                    '1' => '',
                    '2' => '6',
                    '3' => '12',
                    '4' => '12e',
                    '5' => '18',
                    '6' => '24',
                    '7' => '6+1',
                    '8' => '6+2',
                    '9' => '12+1',
                    '10' => '12+2',
                    '11' => '24+1',
                    '12' => '24+2',
                    '13' => 'VÜ'                    
                ),
            ),
            'attributes' => array(
                'value' => '1' //set selected to '1'
            )
        ));
        
        $this->add(array(
            'name' => 'entry',
            'attributes' => array(
                'type'  => 'date',
            ),
            'options' => array(
                'label' => 'Beitritt',
            ),
        ));
        
        $this->add(array(
            'name' => 'profession',
            'attributes' => array(
                'type'  => 'text',
            ),
            'options' => array(
                'label' => 'Beruf',
            ),
        ));
        
        $this->add(array(
            'name' => 'company',
            'attributes' => array(
                'type'  => 'text',
            ),
            'options' => array(
                'label' => 'Firma',
            ),
        ));
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'sauna_week',
            'options' => array(
                'use_hidden_element' => true,
                'checked_value' => '1',
                'unchecked_value' => '0',
                'label' => 'Sauna wöchentlich',
                'value_options' => array(
                    '0' =>'nein',
                    '1' =>'ja',
                ),
            ),
        ));
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'solarabo',
            'options' => array(
                'use_hidden_element' => true,
                'checked_value' => '1',
                'unchecked_value' => '0',
                'label' => 'Solarabo',
                'value_options' => array(
                    '0' =>'nein',
                    '1' =>'ja',
                ),
            ),
        ));
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'start',
            'options' => array(
                'use_hidden_element' => true,
                'checked_value' => '1',
                'unchecked_value' => '0',
                'label' => 'Startpaket',
                'value_options' => array(
                    '0' =>'nein',
                    '1' =>'ja',
                ),
            ),
        ));
        
        $this->add(array(
            'name' => 'kontoNr',
            'attributes' => array(
                'type'  => 'text',
            ),
            'options' => array(
                'label' => 'Kontonummer',
            ),
        ));
        
        $this->add(array(
            'name' => 'kontoInh',
            'attributes' => array(
                'type'  => 'text',
            ),
            'options' => array(
                'label' => 'Kontoinhaber',
            ),
        ));
        
        $this->add(array(
            'name' => 'iban',
            'attributes' => array(
                'type'  => 'text',
            ),
            'options' => array(
                'label' => 'IBAN',
            ),
        ));
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'zahlweise',
            'options' => array(
                'label' => 'Zahlweise',
                'value_options' => array(
                    '1' => '',
                    '2' => 'EV',
                    '3' => 'EM',
                    '4' => 'J',
                    '5' => 'B',
                    '6' => 'Ü'                    
                ),
            ),
            'attributes' => array(
                'value' => '1' //set selected to '1'
            )
        ));
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'active',
            'value' => '1',
            'options' => array(
                'use_hidden_element' => true,
                'checked_value' => '1',
                'unchecked_value' => '0',
                'label' => 'Aktiv',
                'value_options' => array(
                    '0' =>'nein',
                    '1' =>'ja',
                ),
            ),
            'attributes' => array(
                'value' => '1' //set checked to '1'
            )
        ));
        
        $this->add(array(
            'name' => 'origin',
            'attributes' => array(
                'type'  => 'text',
            ),
            'options' => array(
                'label' => 'Quelle',
            ),
        ));
        
        $this->add(array(
            'name' => 'remark',
            'attributes' => array(
                'type'  => 'text',
            ),
            'options' => array(
                'label' => 'Bemerkungen',
            ),
        ));
                      
        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type'  => 'submit',
                'value' => 'Suchen',
                '_id' => 'submitbutton',
            ),
        ));
    }
}
