<?php

namespace Member\Form;

use Zend\Form\Form;

class AdressForm extends Form {
    
    /** __construct()
     * Constructor that defines elements of form 
     * @return void 
    */
    function __construct() {
        parent::__construct('member');
        $this->setAttribute('method', 'post');
        $this->add(array(
            'name' => 'member_id',
            'attributes' => array(
                'type'  => 'hidden',
            ),
        ));
        $this->add(array(
            'name' => 'street',
            'attributes' => array(
                'type'  => 'text',
            ),
            'options' => array(
                'label' => 'Straße:',
            ),
        ));
        
        $this->add(array(
            'name' => 'zipCode',
            'attributes' => array(
                'type'  => 'text',
            ),
            'options' => array(
                'label' => 'Postleitzahl',
            ),
        ));
        
        $this->add(array(
            'name' => 'city',
            'attributes' => array(
                'type'  => 'text',
            ),
            'options' => array(
                'label' => 'Stadt',
            ),
        ));
    }
}