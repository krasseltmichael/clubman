<?php

namespace Member\Form;

use Zend\Form\Form;

/**
 * Defines a Form for person entity
 *
 * @author Nadja
 */
class VisitForm extends Form {
    
    /** __construct()
     * Constructor that defines elements of form 
     * @return void 
    */
    function __construct() {
        parent::__construct('visit');
        $this->setAttribute('method', 'post');
        $this->add(array(
            'name' => 'visit_id',
            'attributes' => array(
                'type'  => 'hidden',
                'readonly' => 'true',
            ),
            'option' => array(
                'label' => 'Mitgliedsnummer',                
            )
        ));
        
       $this->add(array(
            'name' => 'key_name',
            'attributes' => array(
                'type'  => 'text',
            ),
            'options' => array(
                'label' => 'Schlüsselnummer',
            ),
        ));
        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type'  => 'submit',
                'value' => 'Absenden',
                '_id' => 'submitbutton',
            ),
        ));
        
        
    }
}
