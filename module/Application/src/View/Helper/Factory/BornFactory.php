<?php
namespace Application\View\Helper\Factory;

use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;
use Application\View\Helper\Born;
use Application\Service\BornManager;

/**
 * This is the factory for Menu view helper. Its purpose is to instantiate the
 * helper and init menu items.
 */
class BornFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $bornManager = $container->get(BornManager::class);       
        
        $items = $bornManager->getBirthdays();
        
        // Instantiate the helper.
        return new Born($items);
    }
}

