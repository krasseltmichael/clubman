<?php
namespace Application\View\Helper;

use Zend\View\Helper\AbstractHelper;

/**
 * This view helper class displays a menu bar.
 */
class Born extends AbstractHelper {
    /**
     * Menu items array.
     * @var array 
     */
    protected $items = [];
    
    /**
     * Constructor.
     * @param array $items Menu items.
     */
    public function __construct($items=[]) {
        $this->items = $items;
    }
    public function show($what) {   
        foreach($this->items[$what] as $id=>$member) {            
            echo $member->getForename()." ".$member->getName();
            if($id!=count($this->items[$what])-1) echo ", ";
        }
    }
}