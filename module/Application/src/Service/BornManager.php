<?php
namespace Application\Service;

/**
 * This service is responsible for determining which items should be in the main menu.
 * The items may be different depending on whether the user is authenticated or not.
 */
class BornManager {
    
    /**
     * Doctrine EntityManager
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;
    
    /**
     * Constructs the service.
     */
    public function __construct($entityManager) {
        $this->entityManager = $entityManager;
    }
    
    /**
     * This method returns menu items depending on whether user has logged in or not.
     */
    public function getBirthdays() {
        $members = $this->entityManager->getRepository(\Member\Entity\Member::class)->findbyBorn(time());
        return $members;
    }
}
