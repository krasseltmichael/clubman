<?php
use Zend\Router\Http\Literal;
use Zend\Router\Http\Segment;
use Zend\Router\Http\Regex;
use Zend\ServiceManager\Factory\InvokableFactory;
use Application\Route\StaticRoute;
use Doctrine\ORM\Mapping\Driver\AnnotationDriver;

return array(
    'router' => array(
        'routes' => array(
            'calendar' => array(
                'type'    => Segment::class,
                'options' => array(
                    'route'    => '/calendar[/][:action][/:calendar_id]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'calendar_id'     => '[a-zA-Z0-9;:@&%=+\/\$_.-]*',
                    ),
                    'defaults' => array(
                        'controller' => CalendarController::class,
                        'action'     => 'index',
                        ),
                ),
            ),
        ),
     ),
    'controllers' => array(
        'factories' => array(
            CalendarController::class => \Calendar\Service\Factory\CalendarFactory::class,
        ),
    ),

     'doctrine' => array(
        'driver' => array(
            'application_entities' => array(
                'class' =>'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                //'cache' => 'filesystem',
                'paths' => array(__DIR__ . '/../src/Calendar/Entity')
            ),

            'orm_default' => array(
                'drivers' => array(
                    'Calendar\Entity'  => 'application_entities',
                )
            ),
        ),
    ),
    'view_manager' => array(
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        //'not_found_template'       => 'error/404',
        //'exception_template'       => 'error/index',
        'template_map' => array(
            //'layout/layout'           => __DIR__ . '/../view/layout/layout.phtml',
            'calendar/calendar/index' => __DIR__ . '/../view/calendar/calendar/index.phtml',
            //'error/404'               => __DIR__ . '/../view/error/404.phtml',
            //'error/index'             => __DIR__ . '/../view/error/index.phtml',
        ),
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
    ),

);
