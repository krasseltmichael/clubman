<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Calendar\Controller;

use Doctrine\ORM\EntityManager;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Zend_Date;

class CalendarController extends AbstractActionController
{
    /**
     * Constructor is used for injecting dependencies into the controller.
     */
    public function __construct(EntityManager $entityManager) 
    {
        $this->entityManager = $entityManager;
        //date and time functions
        $emConfig = $this->entityManager->getConfiguration();
        $emConfig->addCustomDatetimeFunction('YEAR', 'DoctrineExtensions\Query\Mysql\Year');
        $emConfig->addCustomDatetimeFunction('MONTH', 'DoctrineExtensions\Query\Mysql\Month');
        $emConfig->addCustomDatetimeFunction('DAY', 'DoctrineExtensions\Query\Mysql\Day');
    }  

    /**
     * Entity manager.
     * @var Doctrine\ORM\EntityManager 
     */
    public $entityManager;
    
    /** indexAcion()
     * executes action for IndexRoute, lists all members from database
     * @return ViewModel
     */
    public function indexAction(){
        //extract member_id from URL
        $months=array ('Januar', 'Februar', 'März', 'April', 'Mai', 'Juni', 'Juli', 'August', 'September', 'Oktober', 'November', 'Dezember');
        $month_param = $this->params()->fromRoute('calendar_id', $months[(date('m')-1)]);
        $month=array_search($month_param, $months);

        //extract all born from Database
        $calendars = $this->entityManager->getRepository(\Member\Entity\Member::class)->findByBornMonth(($month+1));
        return new ViewModel(array('calendars' => $calendars, 'month' => $month, 'months' => $months));

    }
    public function monthAction() {
        //extract member_id from URL
        $months=array ('Januar', 'Februar', 'März', 'April', 'Mai', 'Juni', 'Juli', 'August', 'September', 'Oktober', 'November', 'Dezember');
        $month_param = $this->params()->fromRoute('calendar_id', $months[(date('m')-1)]);
        $month=array_search($month_param, $months);
        
        //extract all born from Database
        $calendars = $this->entityManager->getRepository(\Member\Entity\Member::class)->findByBornMonth(($month+1));
        return new ViewModel(array('calendars' => $calendars, 'month' => $month, 'months' => $months));
        
    }
}
