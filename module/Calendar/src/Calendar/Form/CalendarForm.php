<?php

namespace Calendar\Form;

use Zend\Form\Form;

/**
 * Defines a Form for person entity
 *
 * @author Nadja
 */
class CalendarForm extends Form {
    
    /** __construct()
     * Constructor that defines elements of form 
     * @return void 
    */
    function __construct() {
        parent::__construct('calendar');       
        
        $this->add(array(
            'name' => 'mitgl_nr',
            'attributes' => array(
                'type'  => 'hidden',
            ),
        ));
        
        $this->add(array(
            'name' => 'forename',
            'attributes' => array(
                'type'  => 'hidden',
            ),
        ));
        
        $this->add(array(
            'name' => 'name',
            'attributes' => array(
                'type'  => 'hidden',
            ),
        ));
        
        $this->add(array(
            'name' => 'gender',
            'attributes' => array(
                'type' => 'hidden',
            )
        ));
        
        $this->add(array(
            'name' => 'born',
            'attributes' => array(
                'type'  => 'hidden',
            ),
        ));
        
        $this->add(array(
            'name' => 'street',
            'attributes' => array(
                'type'  => 'hidden',
            ),
        ));
        
        $this->add(array(
            'name' => 'zipCode',
            'attributes' => array(
                'type'  => 'hidden',
            ),
        ));
        
        $this->add(array(
            'name' => 'city',
            'attributes' => array(
                'type'  => 'hidden',
            ),
        ));

        $this->add(array(
            'name' => 'telephone',
            'attributes' => array(
                'type' => 'hidden',
            ),
        ));
        $this->add(array(
            'name' => 'mobilNr',
            'attributes' => array(    
                'type' => 'hidden',
            ),
        ));
        $this->add(array(
            'name' => 'faxNr',
            'attributes' => array(
                'type' => 'hidden',
            ),
        ));
        $this->add(array(
            'name' => 'email',
            'attributes' => array(
                'type' => 'hidden',
            ),
        ));
        
        $this->add(array(
            'name' => 'contract',
            'attributes' => array(
                'type' => 'hidden',
            )
        ));
        
        $this->add(array(
            'name' => 'runtime',
            'attributes' => array(
                'type' => 'hidden',
            )
        ));
        
        $this->add(array(
            'name' => 'entry',
            'attributes' => array(
                'type'  => 'hidden',
            ),
        ));
        
        $this->add(array(
            'name' => 'profession',
            'attributes' => array(
                'type' => 'hidden',
            ),
        ));
        
        $this->add(array(
            'name' => 'company',
            'attributes' => array(    
                'type' => 'hidden',
            ),
        ));
        
        $this->add(array(
            'name' => 'sauna_week',
            'attributes' => array(
                'type' => 'hidden',
            ),
        ));
        
        $this->add(array(
            'name' => 'solarabo',
            'attributes' => array(
                'type' => 'hidden',
            ),
        ));
        
        $this->add(array(
            'name' => 'start',
            'attributes' => array(
                'type' => 'hidden',
            ),
        ));
        
        $this->add(array(
            'name' => 'kontoNr',
            'attributes' => array(
                'type' => 'hidden',
            ),
        ));
        
        $this->add(array(
            'name' => 'kontoInh',
            'attributes' => array(
                'type' => 'hidden',
            ),
        ));
        
        $this->add(array(
            'name' => 'iban',
            'attributes' => array(
                'type' => 'hidden',
            ),
        ));
        
        $this->add(array(
            'name' => 'zahlweise',
            'attributes' => array(
                'type' => 'hidden',
            ),
        ));
        
        $this->add(array(
            'name' => 'origin',
            'attributes' => array(
                'type'  => 'text',
            ),
            'options' => array(
                'label' => 'Quelle',
            ),
        ));
        
        $this->add(array(
            'name' => 'remark',
            'attributes' => array(
                'type'  => 'text',
            ),
            'options' => array(
                'label' => 'Bemerkungen',
            ),
        ));
        
        $this->add(array(
            'name' => 'active',
            'attributes' => array(
                'type' => 'hidden',
            ),
        ));
                      
        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type'  => 'submit',
                'value' => 'Exportieren',
                '_id' => 'submitbutton',
            ),
        ));
    }
}
