<?php 
namespace Calendar\Service\Factory;

use Zend\ServiceManager\Factory\FactoryInterface;
use Interop\Container\ContainerInterface;
use Calendar\Controller\CalendarController;

/**
 * This is the factory for Membercontroller. Its purpose is to instantiate the
 * service.
 */
class CalendarFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = NULL)
    {
        $entityManager = $container->get('doctrine.entitymanager.orm_default');
        return new CalendarController($entityManager);
    }
}