<?php
use Zend\Router\Http\Literal;
use Zend\Router\Http\Segment;
use Zend\Router\Http\Regex;
use Zend\ServiceManager\Factory\InvokableFactory;
use Application\Route\StaticRoute;
use Doctrine\ORM\Mapping\Driver\AnnotationDriver;
use Ajaxs\Service\Factory\AjaxsFactory;

return array(
    'router' => array(
        'routes' => array(
            'ajaxs' => array(
                'type'    => Segment::class,
                'options' => array(
                    'route'    => '/ajaxs[/][:action][/:first_id][/:second_id]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        //person_id can be part of Url
                        'first_id'     => '[0-9a-z]+',
                        'second_id'    => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => AjaxsController::class,
                        'action'     => 'index',
                        ),
                ),
            ),
        ),
     ),   
    'controllers' => array(
        'factories' => array(
            AjaxsController::class => AjaxsFactory::class,
        ),
    ),
    'doctrine' => array(
        'driver' => array(
            'application_entities' => array(
                'class' =>'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                //'cache' => 'filesystem',
                'paths' => array(__DIR__ . '/../src/Ajaxs/Entity')
            ),
            'orm_default' => array(
                'drivers' => array(                    
                    'Ajaxs\Entity'  => 'application_entities',                    
                )
            ),
        ),
    ),
    'view_manager' => array(
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        //'not_found_template'       => 'error/404',
        //'exception_template'       => 'error/index',
        'template_map' => array(
            //'layout/layout'           => __DIR__ . '/../view/layout/layout.phtml',
            'ajaxs/ajaxs/index' => __DIR__ . '/../view/ajaxs/ajaxs/index.phtml',
            //'error/404'               => __DIR__ . '/../view/error/404.phtml',
            //'error/index'             => __DIR__ . '/../view/error/index.phtml',
        ),
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
    ),

);
