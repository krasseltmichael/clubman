<?php 
namespace Ajaxs\Service\Factory;

use Zend\ServiceManager\Factory\FactoryInterface;
use Interop\Container\ContainerInterface;
use Doctrine\ORM\EntityManager;
use Ajaxs\Controller\AjaxsController;

/**
 * This is the factory for Membercontroller. Its purpose is to instantiate the
 * service.
 */
class AjaxsFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = NULL)
    {
        $entityManager = $container->get('doctrine.entitymanager.orm_default');
        return new AjaxsController($entityManager);
    }
}