<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Ajaxs\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Doctrine\ORM\EntityManager;

class AjaxsController extends AbstractActionController
{
    /**
     * Constructor is used for injecting dependencies into the controller.
     */
    public function __construct(EntityManager $entityManager) {
        $this->entityManager = $entityManager;
        $emConfig = $this->entityManager->getConfiguration();
        $emConfig->addCustomDatetimeFunction('YEAR', 'DoctrineExtensions\Query\Mysql\Year');
        $emConfig->addCustomDatetimeFunction('MONTH', 'DoctrineExtensions\Query\Mysql\Month');
        $emConfig->addCustomDatetimeFunction('DAY', 'DoctrineExtensions\Query\Mysql\Day');

    }  

    /**
     * Entity manager.
     * @var Doctrine\ORM\EntityManager 
     */
    public $entityManager;
    
    /** indexAction()
     * executes action for IndexRoute
     * @return ViewModel
     */
    public function indexAction(){
        $view = new ViewModel();
        $view->setTerminal(true);
        
        
        $output = array("Hans","Wurst");
        $view->setVariable("output", $output);
        return $view;
    }

    
    /** getMemberAction()
     * executes action for IndexRoute
     * @return ViewModel
     */
    public function getMemberAction(){
        $view = new ViewModel();
        $view->setTerminal(true);
                
        //extract member_id from URL
        $member_id = (int) $this->params()->fromRoute('first_id', 0);
    
        $view = new ViewModel();
        $view->setTerminal(true);
        
        //find member in databse
        $member = $this->entityManager->find('Member\Entity\Member', $member_id);
        $view->setVariable("json", $member->getPrintArrayCopy());
        return $view;
    }

    /** getByRfidAction()
     * executes action for IndexRoute
     * @return ViewModel
     */
    public function getByRfidAction(){
        $view = new ViewModel();
        $view->setTerminal(true);
                
        //extract member_id from URL
        $rfid = $this->params()->fromRoute('first_id', 0);
    
        //find member in databse
        $member = $this->entityManager->getRepository('Member\Entity\Member')->findBy(array("rfid" => $rfid, "active" => 1));
        if(count($member)){
            $view->setVariable("json", $member[0]->getArrayCopy());
            return $view;
        }
        //find key in databse
        $key = $this->entityManager->getRepository('Member\Entity\LockerKey')->findBy(array("rfid" => $rfid));
        if(count($key)){
            $view->setVariable("json", $key[0]->getPrintArrayCopy());
            //CheckoutifUsed
            if($this->isUsedKey($key[0]->getKey_id())){
                //find visit in database
                $queryBuilder = $this->entityManager->getRepository('Member\Entity\Visit')->createQueryBuilder('v')
                    ->AddSelect("DAY(v.checkin),MONTH(v.checkin),YEAR(v.checkin)")
                    ->LeftJoin('Member\Entity\LockerKey', 'k', \Doctrine\ORM\Query\Expr\Join::WITH, 'v.key_id=k.key_id')
                    ->where('v.checkout is NULL AND DAY(v.checkin)= :day AND MONTH(v.checkin)= :month AND YEAR(v.checkin)= :year AND k.key_id = :key_id')->setParameters(array('key_id'=> $key[0]->getKey_id(), 'day' => date("d"), 'month' => date("m"), 'year' => date("Y")))
                    ->setMaxResults(1);
                $visit=$queryBuilder->getQuery()->getresult()[0][0];
                $visit->setCheckout(new \DateTime());
                $this->entityManager->persist($visit);
                $this->entityManager->flush();
            }
            return $view;
        }
        
    }

    /** getMemberByMitglNrAction()
     * executes action for IndexRoute
     * @return ViewModel
     */
    public function getMemberByMitglNrAction(){
        $view = new ViewModel();
        $view->setTerminal(true);
                
        //extract member_id from URL
        $mitgl_nr = $this->params()->fromRoute('first_id', 0);
    
        $view = new ViewModel();
        $view->setTerminal(true);
        
        //find member in databse
        $member = $this->entityManager->getRepository('Member\Entity\Member')->findBy(array("mitgl_nr" => $mitgl_nr, "active"=>1));
        
        $view->setVariable("json", $member[0]->getPrintArrayCopy());
        return $view;
    }
    
    /** getKeysAction()
     * executes action for IndexRoute
     * @return ViewModel
     */
    public function getKeysAction(){
        $view = new ViewModel();
        $view->setTerminal(true);
        
        //extract keys_id from URL
        $member_id = (int) $this->params()->fromRoute('first_id', 0);

        if($this->isCheckedin($member_id)){
            $view->setVariable("error", '<font color="red"><h4>Bereits eingeckeckt!</h4></font>');
            return $view;
        }

        //find member in databse
        $member = $this->entityManager->find('Member\Entity\Member', $member_id);
        $gender=$member->getGender();
        if(!empty($gender)){
            $gender=$member->getGender();
        }else{
            $gender=' ';
        }
        if(is_object($member) && is_numeric($member->getMember_id()) && !empty($gender)){
            //Sparabo Aussortieren
            if($member->getContract()=='2'){
                /*
                $queryBuilder = $this->entityManager->getRepository('Member\Entity\Visit')->createQueryBuilder('v');
                $queryBuilder->andWhere('v.member_id= :member')->setParameter('member', $member->getMember_id());
                $queryBuilder->andWhere('DAY(v.checkin)>= :day')->setParameter('day', 1);
                $queryBuilder->andWhere('MONTH(v.checkin)= :month')->setParameter('month', date("m"));
                $queryBuilder->andWhere('YEAR(v.checkin)= :year')->setParameter('year', date("Y"));
            
                $visitsofmonth=$queryBuilder->getQuery()->getresult();
                if(count($visitsofmonth)>=4){
                    $view->setVariable("keys", array());
                    $view->setVariable("error", "Sparabo im Monat abgelaufen, Letzter Besuch: ".$member->getLastVisit());
                    $view->setVariable("member", $member);
                    return $view;
                }
            */
            }
            $queryBuilder = $this->entityManager->getRepository('Member\Entity\LockerKey')->createQueryBuilder('k')
                                                ->where('k.key_type = :gender')->setParameter('gender', $gender)
                                                ->GroupBy('k.key_nr')
                                                ->orderBy('k.key_nr', 'ASC');
            $allkeys=$queryBuilder->getQuery()->getresult();
            $output=array();
            foreach ($allkeys as $key){
                if(!$this->isUsedKey($key->getKey_id()))
                    $output[$key->getKey_id()]=array($key->getKey_nr(),$key->getKey_type(),$key->getrfid());
            }
            $view->setVariable("keys", $output);
        }else if(empty($gender)){
            $view->setVariable("error", "Kein Geschlecht festgelegt!");
        }
        $view->setVariable("member", $member);
        return $view;
    }
    
    /** checkoutAction()
     * Action for finishing a visit 
     * @return ViewModel;
     */
    public function checkoutAction() {
        
        //extract visit_id from URL
        $visit_id = (int) $this->params()->fromRoute('first_id',0);
        
        //find visit in database
        $visit = $this->entityManager->find('Member\Entity\Visit', $visit_id);
        
        $visit->setCheckout(new \DateTime());
        $this->entityManager->persist($visit);
        $this->entityManager->flush();
    }         

    /** isCheckedin()
     * Checks if Member is allready logged in
     * @return bool
     */
    public function isCheckedin($member_id){
        
        $queryBuilder = $this->entityManager->getRepository('Member\Entity\Visit')->createQueryBuilder('v');
        $queryBuilder->AddSelect("DAY(v.checkin),MONTH(v.checkin),YEAR(v.checkin)");
        $queryBuilder->LeftJoin('Member\Entity\Member', 'm', \Doctrine\ORM\Query\Expr\Join::WITH, 'v.member_id=m.member_id');
        $queryBuilder->where('v.checkout is NULL AND DAY(v.checkin)= :day AND MONTH(v.checkin)= :month AND YEAR(v.checkin)= :year AND m.member_id = :member_id')->setParameters(array('member_id'=> $member_id, 'day' => date("d"), 'month' => date("m"), 'year' => date("Y")));
        $member_checked_in=$queryBuilder->getQuery()->getresult();
        if(count($member_checked_in)>=1){
            return true;
        }else{
            return false;
        }
    }    

    /** isUsedKey()
     * Checks if Key is allready used in
     * @return bool
     */
    public function isUsedKey($key_id){
        
        $queryBuilder = $this->entityManager->getRepository('Member\Entity\Visit')->createQueryBuilder('v');
        $queryBuilder->AddSelect("DAY(v.checkin),MONTH(v.checkin),YEAR(v.checkin)");
        $queryBuilder->LeftJoin('Member\Entity\LockerKey', 'k', \Doctrine\ORM\Query\Expr\Join::WITH, 'v.key_id=k.key_id');
        $queryBuilder->where('v.checkout is NULL AND DAY(v.checkin)= :day AND MONTH(v.checkin)= :month AND YEAR(v.checkin)= :year AND k.key_id = :key_id')->setParameters(array('key_id'=> $key_id, 'day' => date("d"), 'month' => date("m"), 'year' => date("Y")));
        $key_checked_in=$queryBuilder->getQuery()->getresult();
        if(count($key_checked_in)>=1){
            return true;
        }else{
            return false;
        }
    }    

    
    
 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    /* -- Old - Check if keept --*/
    /** getMembersAction()
     * executes action for IndexRoute
     * @return ViewModel
     */
    public function getMembersAction(){
        $view = new ViewModel();
        $view->setTerminal(true);
        
        //extract member_id from URL
        $mitgl_nr = (int) $this->params()->fromRoute('first_id', 0);
        //find member in databse
        $queryBuilder = $this->entityManager->getRepository('Member\Entity\Member')->createQueryBuilder('m');
        $queryBuilder->where("m.mitgl_nr LIKE '".$mitgl_nr."%' AND m.active !='0' ");
        $queryBuilder->groupBy('m.mitgl_nr');
        $queryBuilder->orderBy('m.mitgl_nr', 'ASC');
        $members=$queryBuilder->getQuery()->getresult();
        if(count($members)>=1){
            //Mitgliederlists
            $view->setVariable("members", $members);
            return $view;
        }else{
            //Nichts
            return $view;
        }
    }
    /** CheckoutAllAction()
     * excutes action for CheckoutAllAction()
     * @return ViewModel
     */
    public function CheckoutAllAction(){
        $view = new ViewModel();
        $view->setTerminal(true);

        $queryBuilder = $this->entityManager->getRepository('Member\Entity\Visit')->createQueryBuilder('v');
            $queryBuilder->where('v.checkout IS NULL');   
            $allvisits=$queryBuilder->getQuery()->getresult();
            foreach ($allvisits as $visit){
                $data["checkout"]=new \DateTime();
                $visit->exchangeArray($data);
                $this->entityManager->persist($visit);
                $this->entityManager->flush();                        
            }
        return $view;    
    }
    
     /** checkinAction()
     * executes action for IndexRoute
     * @return ViewModel
     */
    public function checkinAction(){
        $view = new ViewModel();
        $view->setTerminal(true);
        //extract member_id from URL
        $member_id = (int) $this->params()->fromRoute('first_id', 0);
        $member = $this->entityManager->find('Member\Entity\Member', $member_id);
        //extract key_id from URL
        $key_id = (int) $this->params()->fromRoute('second_id', 0);
        $key = $this->entityManager->find('Member\Entity\LockerKey', $key_id);

        $queryBuilder = $this->entityManager->getRepository('Member\Entity\Visit')->createQueryBuilder('v');
        $queryBuilder->AddSelect("DAY(v.checkin), MONTH(v.checkin), YEAR(v.checkin)");
        $queryBuilder->LeftJoin('Member\Entity\LockerKey', 'k', \Doctrine\ORM\Query\Expr\Join::WITH, 'v.key_id=k.key_id');
        $queryBuilder->where('v.checkout IS NULL');
        $queryBuilder->andWhere('k.key_id = :key')->setParameter('key', $key->getKey_id());
        $queryBuilder->andWhere('k.key_type = :gender')->setParameter('gender', $member->getgender());
        $queryBuilder->andWhere('DAY(v.checkin)= :day')->setParameter('day', date("d"));
        $queryBuilder->andWhere('MONTH(v.checkin)= :month')->setParameter('month', date("m"));
        $queryBuilder->andWhere('YEAR(v.checkin)= :year')->setParameter('year', date("Y"));
        $queryBuilder->orderBy('k.key_nr', 'ASC');
        $key_used=$queryBuilder->getQuery()->getresult();
        if(count($key_used)<=0){
            $visit = new \Member\Entity\Visit();
            $data["member_id"]=$member;
            $data["key_id"]=$key;
            $data["checkin"]=new \DateTime();
            $visit->exchangeArray($data);

            //save visit in database
            $this->entityManager->persist($visit);
            //$this->entityManager->persist($key);
            $this->entityManager->flush();                        
        }
        
        return $view;
    }
    /** visitorsAction()
     * executes action for visitorsRoute
     * @return ViewModel
     */
    public function visitorsAction(){
        $view = new ViewModel();
        $view->setTerminal(true);
        
        $queryBuilder = $this->entityManager->getRepository('Member\Entity\Visit')->createQueryBuilder('v');
        $queryBuilder->where('v.checkout is NULL AND DAY(v.checkin)= :day AND '
                            . 'MONTH(v.checkin)= :month AND '
                            . 'YEAR(v.checkin)= :year ')
                     ->leftJoin('v.member_id', 'm', \Doctrine\ORM\Query\Expr\Join::WITH, 'm.member_id = v.member_id')
                     ->addOrderBy("m.gender", "desc")
                     ->addOrderBy("v.checkin", "asc")
                     ->setParameters(array('day' => date("d"), 'month' => date("m"), 'year' => date("Y"))); 
            $visitors=$queryBuilder->getQuery()->getresult();
            $view->setVariable("visitors", $visitors);
            return $view;
    }    
}
