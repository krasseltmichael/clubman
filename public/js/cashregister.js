function position(id,price,name,tax,amount){
    this.id = id;
    this.price = price;
    this.name = name;
    this.tax = tax;
    this.amount = amount;
}
var kasse = {
    Url:"ws://192.168.2.18",
    positions:[],
    summe:0.00, 
    changeposition:-1,
    selectedarticle:-1,
    showmessage: function (type,message){
        switch(type){
            case "offline": 
                $("#reader_status").removeClass("green");
                $("#reader_status").addClass("red");
                break;
            case "online": 
                $("#reader_status").removeClass("red");
                $("#reader_status").addClass("green");
                break;
            case "send": document.getElementById("input").innerHTML = "<p>" + message + "</p>";
                break;
        }
    },
    renderbill: function(){
        this.summup();
        var newHTML = ["<div><div>Posten</div><div>Bezeichung</div><div>Menge</div><div>Steuer</div><div>Preis</div></div>"];
        parent=this;
        $.each(this.positions, function(index, value) {
            newHTML.push('<div id="position_'+index+'" class="'+((value.id==parent.selectedarticle)?"selected":"")+'">'+
                '<div>'+(index+1)+'</div>'+
                '<div>'+value.name+'</div>'+
                '<div>'+value.amount+'</div>'+
                '<div>'+Math.floor(value.tax*100)+'%</div>'+
                
                '<div>'+value.price+' €</div>'+
            '</div>');
        });
        newHTML.push('<div id="position_summe"><div></div><div></div><div></div><div>Summe:</div><div>'+this.summe+' €</div></div>');
        $("#positions").html(newHTML.join("\n"));
        parent=this;
        $.each(this.positions, function(index, value) {
            $('#position_'+index).click(function(element){
                $("#amount_select div:last-child").html(value.amount);
                parent.changeposition=index;
                parent.selectarticle(value.id);
                parent.renderbill();
           });
        });
    },
    buttons: function(){
        parent=this;
        $("#amount_select").on("click", "div", function(event){
                parent.changeamount($(this).html());
        });
        $("#article_controll button:first-child").on("click", function(event){
                parent.removeselected();
        });
        $("#article_controll button:last-child").on("click", function(event){
                parent.addselected();
        });

    },
    changeamount: function(code){
        var amount=Number($("#amount_select div:last-child").html());
        switch(code){
            case "-":
                if(amount>0)amount=amount-1;
                break;
            case "+":
                amount=amount+1;
                break;
            case "1":
                amount=1;
                break;
            case "2":
                amount=2;
                break;
            case "3":
                amount=3;
                break;
            case "4":
                amount=4;
                break;
            case "5":
                amount=5;
                break;
        }
        if(this.changeposition>=0){
            if(amount<=0){
                this.positions.splice(this.changeposition,1);
                this.changeposition=-1;
                $("#article_"+this.selectedarticle).removeClass("selected");
                this.selectedarticle=-1;
            }else{
                this.positions[this.changeposition].amount=amount;
            }
            this.renderbill();
        }
        $("#amount_select div:last-child").html(amount);
    },
    summup: function(){
        var summe=0.00;
        $.each(this.positions, function(index, value) {
            summe+=value.price*value.amount;
        });  
        this.summe=Math.round(summe*100)/100;
    },
    selectarticle: function(articleid){
        $("#article_"+this.selectedarticle).removeClass("selected");
        if(this.getpositionbyid(articleid)>=0)
            this.changeposition=this.getpositionbyid(articleid);
        else
            this.changeposition=-1;
        this.selectedarticle=articleid;
        $("#article_"+articleid).addClass("selected");
        this.renderbill();
    },
    addselected: function(){
        if(this.selectedarticle>0){
            if(this.getpositionbyid(this.selectedarticle)>=0){
                position=this.getpositionbyid(this.selectedarticle);
                this.positions[position].amount=Number(this.positions[position].amount)+Number($("#amount_select div:last-child").html());
            }else{
                ae=$("#article_"+this.selectedarticle);
                //alert(ae.attr("data-id")+ae.attr("data-price")+ae.attr("data-name")+ae.attr("data-tax")+$("#amount_select div:last-child").html());
                var newarticle = new position(ae.attr("data-id"),ae.attr("data-price"),ae.attr("data-name"),ae.attr("data-tax"),$("#amount_select div:last-child").html());
                this.positions.push(newarticle);
                $("#amount_select div:last-child").html(1);
            }
            $("#article_"+this.selectedarticle).removeClass("selected");
            this.selectedarticle=-1;
            this.renderbill();
        }
    },
    removeselected: function(){
        if(this.selectedarticle>0 && this.getpositionbyid(this.selectedarticle)>=0){
            this.positions.splice(this.getpositionbyid(this.selectedarticle),1);
            $("#article_"+this.selectedarticle).removeClass("selected");
            this.selectedarticle=-1;
            this.renderbill();
        }
    },
    getpositionbyid: function(id){
        for (var i = 0; i < this.positions.length; i++) {
            if (this.positions[i].id == id) {
                return i;
            }
        }
        return -1;
    },
    clear: function(){
        this.positions=[];
        this.renderbill();
    },
    loadarticles: function(){
        categorie=[1,2,3,4];
        $.each(categorie, function(index, value) {
            $('#categorie'+value+' div').html("");
            $.getJSON('/cataloge/getByCategory/'+index,function(data){
                newhtml=[];
                $.each(data, function(position, value) {
                    newhtml.push("<div onclick=\"kasse.selectarticle("+value.article_id+");\" id=\"article_"+value.article_id+"\" data-id=\""+value.article_id+"\" data-name=\""+value.article_name+"\" data-price=\""+value.article_price+"\" data-tax=\""+value.article_tax+"\">"+value.article_name+"<br>"+value.article_price+"€</div>");
                });  
                $('#categorie'+value+' div').html(newhtml.join("\n"));
            });
        });
    },
    book: function(){
        //Buchen nur bei Mindestens einem Eintrag möglich
        if(this.positions.length>=1){
            parent=this;
            //Ändere die Punkte in Kommas
            $.each(this.positions, function(index, value) {
                parent.positions[index].price.replace(".", ",");
            });
            //Rufe den Controller Cashbook Methode Add zum Speichern auf mit den Artikeln als POST
            $.post( "/cashbook/add", { articles: this.positions })
                .done(function( data ) {
                    //Bei Erfolg Weiterleiten zum Drucken
                    info = JSON.parse(data);
                    window.location.replace('/cashbook/print/'+info.cashbook_nr);
                }, "json");
        }
        //Bereinige die Anzeige
        this.clear();
    },
    start: function(){ 
        if($('#cash_register').length){ 
            this.buttons();
            this.renderbill();
            this.loadarticles();
            window.location= "#categorie1";
            /*$("input[name='rfid']").val(rfid);
            $.getJSON('/ajaxs/getByRfid/'+rfid,function(data){MemberInfo('rfidload',data.member_id)});
            $("#"+rfid).trigger('click');*/
        }
    }
};

