rfidReader = {
    Url: "ws://192.168.2.18",
    myTimeout: null,
    socket: null,
    runMode: null,
    showMessage: function(type,message){
        switch(type){
            case "offline": 
                $("#reader_status").removeClass("green");
                $("#reader_status").addClass("red");
                break;
            case "online": 
                $("#reader_status").removeClass("red");
                $("#reader_status").addClass("green");
                break;
            case "send": document.getElementById("input").innerHTML = "<p>" + message + "</p>";
                break;
        }
    },
    deadServer: function(){
        this.showMessage("offline","sd");
        rfidReader.start();
    },
    start: function() { 
        var parent = this;
        this.socket = new WebSocket(parent.Url);
        this.socket.onopen = function() {
            parent.showMessage('online');
        };
        this.socket.onmessage = function(evt) {
            if(evt.data!=="Heartbeat"){
                //Filter Chars
                var rfid=evt.data.replace(/[^A-Za-z 0-9 \.,\?""!@#\$%\^&\*\(\)-_=\+;:<>\/\\\|\}\{\[\]`~]*/g, '');
                if(rfidReader.runMode==="learn"){
                    $("input[name='rfid']").val(rfid);
                    rfidReader.setrunmode("identify")
                }else{
                    $.getJSON('/ajaxs/getByRfid/'+rfid,function(data){MemberInfo('rfidload',data.member_id)});
                    $("#rfid_"+rfid).trigger('click');
                }
            }else{
                clearTimeout(parent.myTimeout);
                parent.showMessage('online');
                parent.myTimeout = setTimeout(parent.deadServer.bind(parent), 1500);
            }
        };
        this.socket.onerror = function(evt) {
            parent.showMessage('offline');
            setTimeout('rfidReader.start()',1500);
        };
        this.socket.onclose = function(evt) {
            parent.showMessage('offline');
            setTimeout('rfidReader.start()',1500);
        };
        this.setrunmode("identify");
        $('#learn_button').click(function(){parent.setrunmode('learn')});
        return this;
    },
    setrunmode: function(newmode){
        rfidReader.runMode=newmode;
    },
    sendMessage: function(message){
        this.showMessage("send",message);
        this.socket.send(message);
    }
};

