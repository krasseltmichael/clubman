function read_cookie(key){
    var result;
    return (result = new RegExp('(?:^|; )' + encodeURIComponent(key) + '=([^;]*)').exec(document.cookie)) ? (result[1]) : null;
}
$(document).ready(function(){
    //Fügt Datepicker ein
    if (!Modernizr.inputtypes.date) {
        $('input[type=date]').datepicker({ dateFormat: 'dd.mm.yy' });
    }
    if (!Modernizr.inputtypes.time) {
        $('input[type=time]').timepicker({
            showPeriod: false,            // Define whether or not to show AM/PM with selected time. (default: false)
            showPeriodLabels: false,       // Define if the AM/PM labels on the left are displayed. (default: true)
            periodSeparator: ' ',         // The character to use to separate the time from the time period.
            //altField: '#alternate_input', // Define an alternate input to parse selected time to
            showOn: 'focus',              // Define when the timepicker is shown.
            // Localization
            hourText: 'Stunde',             // Define the locale text for "Hours"
            minuteText: 'Minute',         // Define the locale text for "Minute"
            amPmText: ['AM', 'PM'],       // Define the locale text for periods
            // Position
            myPosition: 'left top',       // Corner of the dialog to position, used with the jQuery UI Position utility if present.
            atPosition: 'left bottom',    // Corner of the input to position

            minutes: {
                starts: 0,                // First displayed minute
                ends: 55,                 // Last displayed minute
                interval: 5,              // Interval of displayed minutes
                manual: []                // Optional extra entries for minutes
            },
            rows: 4,                      // Number of rows for the input tables, minimum 2, makes more sense if you use multiple of 2
            showHours: true,              // Define if the hours section is displayed or not. Set to false to get a minute only dialog
            showMinutes: true,            // Define if the minutes section is displayed or not. Set to false to get an hour only dialog
            // Min and Max time
            
            minTime: {                    // Set the minimum time selectable by the user, disable hours and minutes
                hour: 00,            // previous to min time
                minute: 00
            },
            maxTime: {                    // Set the minimum time selectable by the user, disable hours and minutes
                hour: 23,            // after max time
                minute: 59
            },
            // buttons
            showCloseButton: true,       // shows an OK button to confirm the edit
            closeButtonText: 'Done',      // Text for the confirmation button (ok button)
            showNowButton: true,         // Shows the 'now' button
            nowButtonText: 'jetzt',         // Text for the now button
            showDeselectButton: true,    // Shows the deselect time button
            deselectButtonText: 'Löschen' // Text for the deselect button
            });
    }
    
    /*
    if(Math.round(Math.random() * (10)) || read_cookie("UpdateAvailable")){
        $("#system_version").load("/update.php?task=check");
    }else{
        $("#system_version").load("/update.php?task=update");
    }
    */
}); 
$(function() {
    $('#mitgl_nr').val('');
    if ( $('#table_members').length ) {
        var MemberInfoTimeout;
        $('.member .divtablerow').hover(
                                function() {
                                        var memberobject = this;
                                        MemberInfoTimeout=setTimeout(
                                            function(){
                                                MemberInfo('mouseover',$(memberobject).attr('id').substring(17,$(memberobject).attr('id').length));
                                            },
                                            //alert($(memberobject).attr('id').substring(17,$(memberobject).attr('id').length)),
                                            1200);
                                        },
                                function(){
                                        clearTimeout(MemberInfoTimeout);
                                        MemberInfo('mouseout');
                                }
                            );
    }
     setInterval(function() {
        show_visitors();
    }, 5000);
    show_visitors();
    
    //$('#header').hover(function(){$('#middle_content').addClass("expandheader");},function(){$('#middle_content').removeClass("expandheader");})
    kasse.start();
    rfidReader.start();
});
