var selectMembertimeout;
var showMembertimeout;
var MemberData=[];
var MemberReader=0;
var KeyReader=0;
function MemberInfo(mode,memberid){
    if (typeof memberid === 'undefined') { memberid = 0; }
    switch(mode){
        case 'mouseover':
            $('#key_list').html('');
            //$('#middle_content').addClass("expandheader");
            $('#key_list').load('/ajaxs/getKeys/' + memberid);
            if(MemberData.hasOwnProperty(memberid)){
                MemberShow(MemberData[memberid],memberid);
            }else{
                MemberLoad(memberid);
            }
            break;
        case 'mouseout':
            setTimeout("$('#key_list').html('')",1000);
            //$('#middle_content').removeClass("expandheader");
            var fields=["member_id","mitgl_nr","name","forename","born","street","zipCode","city","telephone","mobilNr","faxNr","email","company","profession","contract","runtime","entry","active","sauna_week","solarabo","start","kontoInh","kontoNr","iban","zahlweise","remark","origin","visits","last_visit","last_visit2","last_visit3","last_visit4"];
            for (var prop in fields) {
                $('#'+prop).html('');
            }
            if(MemberData.hasOwnProperty(MemberReader)){
                MemberShow(MemberData[MemberReader],MemberReader);
            }else{
                MemberLoad(MemberReader);
            }
            break;
        case 'rfidload':
            MemberReader=memberid;
            MemberLoad(memberid);
            $('#key_list').load('/ajaxs/getKeys/' + memberid);
            break;

    }
}
function MemberLoad(memberid){
    if (typeof memberid === 'undefined') { memberid = 0; }
    if(memberid>0){
        var memberinfodata=null;
        $.getJSON('/ajaxs/getMember/'+memberid,function(data){MemberShow(data,memberid);}
        );
        return memberinfodata;
    }
}
var MemberTranslateTimeout;
function MemberTranslate(modus){
    if (typeof modus === 'undefined') { modus = "wait"; }
    if (modus==="run"){
        var mitgl_nr=$('#mitgl_nr').val();
        $.getJSON('/ajaxs/getMemberByMitglNr/'+mitgl_nr,function(data){MemberInfo('mouseover',data.member_id);});
    }else{
        clearTimeout(MemberTranslateTimeout);
        MemberTranslateTimeout = setTimeout('MemberTranslate("run")', 800);
    }
}
function MemberShow(data,memberid){
    MemberData[memberid]=data;
    for (var prop in data) {
        if (data.hasOwnProperty(prop)) { 
            if(prop=="member_id"){
                link=$('#'+prop ).attr("href").split("/");
                link[(link.length-1)]=data[prop];
                $('#'+prop ).attr("href",link.join("/"));
            }else if(prop=="mitgl_nr"){
                $('#'+prop ).val(data[prop]);
            }else if(prop=="born" || prop=="entry"){
                var date = new Date(Date.parse(data[prop].date));
                $('#'+prop ).html(date.getDate()+"."+(date.getMonth()+1)+"."+date.getFullYear());
            }else if(prop=="gender"){
                $("#member_gender").removeClass("male");
                $("#member_gender").removeClass("female");
                if(data[prop]=="1"){
                    $("#member_gender").addClass("male");
                }else{
                    $("#member_gender").addClass("female");
                }
            }else{
                $('#'+prop ).html(data[prop]);    
            }
        }
    }
}

function checkin(member_id,key_id){
    $('#key_list').html('/ajaxs/checkin/' + member_id + '/' + key_id);
    $.get('/ajaxs/checkin/' + member_id + '/' + key_id, function( data ) {
        $('#mitgl_nr').focus();
        $('#key_list').html('');
        show_visitors();
    });
    
    if ("Notification" in window) {
        if (Notification.permission !== 'denied') {
            if (Notification.permission === "granted") {
                data=MemberData[member_id];
                var options = {
                    body: 'Eingecheckt:' + data.forename + ' ' + data.name + ' auf SchlüsselID:' + key_id,
                    icon: "/img/user-"+(data.gender==0?"female":"male")+".png"
                }
                var n = new Notification('Checkin erfolgreich',options);
                //n.click()
                //n.close()
                //n.error();
                //n.show();
                setTimeout(n.close.bind(n), 5000); 
            } else {
                Notification.requestPermission(function (permission) {
                    if (permission === "granted") {
                        var notification = new Notification("Hi there!");
                    }
                });
            }
        }else{
            alert("Geht nicht");
        }
    }
}
function show_visitors(){
    $('#visitor_list').load('/ajaxs/visitors');
}

function checkout(rfid,who){
    if(who=='all'){
        if (confirm('Sollen alle Besucher ausgecheckt werden ?')){
            $.get('/ajaxs/CheckoutAll', function(){checkin('refresh')});
        }else{
        }
    }else{
        $.get('/ajaxs/checkout/' + who, function(){$("#rfid_"+rfid).remove()});
    }
}
function checkinset(where,what){
    $('#'+where).val(what);
    $('#checkin_key_nr').focus();
}
