<?php
function getLocalrev(){
    $output = array();
    if(PHP_OS!='WINNT'){
        exec('svn info ../ |grep Revision: |cut -c11-', $output);
        $localrev=$output[0];
    }else{
        exec('svn info ../ ', $output);
        foreach($output as $line){
            if(preg_match("/(Revision)(:)/", $line)) {
                preg_match("/.*?(\\d+)/is", $line, $match);
                $localrev=$match[1];
            }
        }
    }    
    return $localrev;
}


if(isset($_GET["task"])){
    if(!isset($_GET["revision"]) && isset($_COOKIE["UpdateAvailable"]) and $_COOKIE["UpdateAvailable"]) $_GET["task"]="check";
    switch ($_GET["task"]){
        case "check":
            //Getting SVN Revison out of Internet
            $file = file("http://nmail.us.to/svn/clubman/");
           if(is_array($file) && count($file)>=2){
                $webrev=substr($file[2],24,strlen($file[2])-33);
            }else{
                $webrev=1;
            }

            //Reading Local Revision from Shell
            $localrev=getLocalrev();

            //Show Message
            if($webrev>$localrev){
                echo '<a style="color:white;" href="/update.php?task=update&revision='.$webrev.'">Neue Version '.$webrev.' verf&uuml;gbar</a><br>';
                setcookie("UpdateAvailable", 'true', time()+3600);
            }else{
                echo "Version:".$localrev."\n";
            }
            break;
        case "update":
            //Reading Local Revision from Shell
            $localrev=getLocalrev();
            if (isset($_GET['revision']) && is_numeric($_GET['revision'])){
                if($_GET['revision']>$localrev){
                    setcookie("UpdateAvailable","false",time() - 3600);
                    echo "Updating Files<br>\n";
                    $output=array();
                    exec('svn update ../', $output);
                    foreach($output as $line){echo $line."<br>\n";}
                    $output=array();
                    echo "Updating Database<br>\n";
                    passthru('../vendor/bin/doctrine-module orm:schema-tool:update --force');
                    foreach($output as $line){echo $line."<br>\n";}
                    echo '<a href="../../">Zur&uuml;ck zum System</a>';
                }
            }else{
                echo "Version ".$localrev;
            }
    }
}

